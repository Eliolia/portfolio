﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinsBehavior : MonoBehaviour
{

    public int value = 1;
    public Animator animator;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            int coinUI = int.Parse(CanvasManager._instance.coinPanelTxt.GetComponent<Text>().text) + value;
            CanvasManager._instance.coinPanelTxt.GetComponent<Text>().text = coinUI + "";
            animator.SetBool("IsCollect", true);
        }
    }
}
