﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CanvasManager : MonoBehaviour

{
    // Canvas Manager instance
    public static CanvasManager _instance;

    public GameObject pauseBtn;
    public GameObject pausePanel;
    public GameObject gameOverPanel;
    public GameObject coinPanelTxt;
    public GameObject gameOverCoinTxt;

    private bool isPause = false;

    private void Awake()
    {
        // DontDestroyOnLoad won't destroy the object at Scene.reload
        DontDestroyOnLoad(this);

        // instance is null -> First is the good one
        if (_instance == null)
        {
            _instance = this;
        }
        else // Destroy the loaded one
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        gameOverPanel.SetActive(false);
    }

    public void GameOver()
    {
        Time.timeScale = 0;
        coinPanelTxt.GetComponent<coinAmount>().SaveCoin();
        gameOverCoinTxt.GetComponent<Text>().text = PlayerPrefs.GetInt("CoinAmount") + "";
        gameOverPanel.SetActive(true);
        pauseBtn.SetActive(false);
    }

    public void PausePlayer()
    {
        if (isPause)
        {
            isPause = false;
            Time.timeScale = 1;
            pauseBtn.SetActive(true);
            pausePanel.SetActive(false);
        }
        else
        {
            isPause = true;
            Time.timeScale = 0;
            pauseBtn.SetActive(false);
            pausePanel.SetActive(true);
        }
    }

    public void Restart()
    {
        PlayerPrefs.SetInt("CoinAmount", 0);
        coinPanelTxt.GetComponent<Text>().text = "0";
        StartCoroutine(DelayedReload(0.05f));
    }

    public IEnumerator DelayedReload(float wait)
    {
        yield return new WaitForSecondsRealtime(wait);
        SceneManager.LoadScene("Run");
        yield return new WaitForSecondsRealtime(wait);

        // After Reload

        pauseBtn.SetActive(true);
        gameOverPanel.SetActive(false);
        pausePanel.SetActive(false);
        Time.timeScale = 1;
    }

}
