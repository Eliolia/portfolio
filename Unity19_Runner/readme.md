# Runner

## Résumé

Il s'agit d'une application issue d'un tutoriel de prise en main d'Unity 2019 sur Youtube.

## Comment jouer ?

Appuyez sur la touche "espace" du clavier pour sauter. 

## Implémentation

Le projet a été développé sous Unity 2019.x en C#.

## Les améliorations envisageables

Plusieurs améliorations du projet ont été envisagées :

* tomber dans les trous ;
* score final sur l'écran de fin ;
* levels ;
* intégration de sons ;
* des bonus ;
* collectibles divers ;
* faire sauter le méchant ;
* tableau des meilleurs scores ;
* un menu.

## Exécuter

Ouvrir l'exécutable "Roll a Ball.exe"

## Démonstration vidéo

Dans le dossier "Demonstration" se trouve la vidéo en question