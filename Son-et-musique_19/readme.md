# Sons et Musiques

## Résumé

C’est dans le cadre de l’exploitation et l’application de nos connaissances acquises dans l’UE Sons et Musiques que ces différents sons ont été réalisés.

## Notions abordées

* ajouts de musiques et de bruitages libres de droits sur une vidéo existante avec un logiciel de montage vidéo, une partie du trailer de Star Wars : The Old Republic ;

* reproduction d'une musique à l'oreille avec le logiciel LMMS, à savoir Tranz de Gorillaz ;

* compositions à partir d'inspirations diverses ;

* création d'événements sonores et manipulations de différentes fonctionnalités de Fmod Studio. 
