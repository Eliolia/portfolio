# Augmented Reality Trump

## Résumé

Ce projet est une prise en main de l'intégration du moteur de rendu AR Vuforia au sein d'un projet Unity.

On y voit l'animation d'un petit personnage aux allures de Trump sur un billet de 50 dollars américains.

## Comment utiliser ?

Brandir l'image d'un billet de 50 dollars américains imprimé sur une feuille ou sur un support numérique (billetNB_scaled.jpg) devant une webcam.

## Implémentation

Le projet a été développé sous Unity 2019.x. avec le moteur de rendu AR Vuforia.

## Exécuter

Ouvrir le projet avec la version 2019 LTS d'Unity, lancer le projet, autoriser la webcam.  

## Démonstration

Dans le dossier "Démonstration" se trouve une image du résultat du projet.
