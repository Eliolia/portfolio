# Application Rabin-Karp

## Résumé

Présenter l'algorithme Rabin-Karp de manière pédagogique en explicitant les différentes étapes et la comparaison avec d'autres algorithmes de recherche de sous-séquence, et l'implémenter.

## Installation

Installer la librairie SFML (ex: sur Ubuntu, tapez _sudo apt-get update && sudo apt-get install libsfml-dev_)
https://www.sfml-dev.org/download/sfml/2.5.0/

Exécutez la commande make dans un terminal situé dans le dossier du projet
ou 
Lancez la commande suivante:
_g++ -Wall -ansi -pedantic -std=c++11 -lsfml-graphics -lsfml-window -lsfml-system main.cpp Screen.cpp Button.cpp RB.cpp Menu.cpp Tutoriel.cpp Tester.cpp Credits.cpp App.cpp -o rabin_karp_

## Exécuter

Entrez la commande ./rabin_karp dans un terminal à partir de la racine du projet.

## Spécifications

* Langage:		C++
* Librairie:	SFML 2.3.2
* OS:			Linux

## Bug Report

* Dans la partie "Tester" de l'application, dès que l'utilisateur a saisi 20 caractères, on ne peut plus effacer. Il faut sortir de l'écran et y revenir.
