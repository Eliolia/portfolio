#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include "App.hpp"

int main(int argc, char const *argv[])
{
	std::cout 	<< std::endl
				<< "\t\033[1;33m#########################\033[0m\n"
				<< "\t\033[1;33m#\033[0m\tRabin-Karp\t\033[1;33m#\033[0m\n"
				<< "\t\033[1;33m#########################\033[0m"
				<< std::endl << std::endl;
	App* app = new App(0, 1280, 720, 32, 60, "Rabin-Karp C++/SFML");
	app->run();

	std::cout 	<< std::endl
				<< "\t\033[1;31m#########################\033[0m\n"
				<< "\t\033[1;31m#\033[0m\tFin du prog\t\033[1;31m#\033[0m\n"
				<< "\t\033[1;31m#########################\033[0m"
				<< std::endl << std::endl;

	return EXIT_SUCCESS;
}