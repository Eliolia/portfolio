#ifndef MENU_HPP
#define MENU_HPP


#include <string>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include "Screen.hpp"
#include "Button.hpp"

class Menu : public virtual Screen{

private:
	std::vector<Button> ListButton; // Liste des boutons présents sur le screen
public:

	//	#####################################
	//	#	Constructeurs et Destructeur	#
	//	#####################################

	// Constructeur par défaut
	Menu(void);

	// Constructeur paramétré
	Menu(std::string title, std::string fPath, std::string tPath);

	// Destructeur
	virtual ~Menu();

	//	#####################################
	//	#		Méthodes principales		#
	//	#####################################

	// Handler principal de la classe
	virtual int		run(sf::RenderWindow *&App);

	// Handler des évenements
	virtual int		processEvent(sf::RenderWindow *&App);

};

#endif