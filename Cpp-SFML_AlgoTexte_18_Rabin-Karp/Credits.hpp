#ifndef CREDITS_HPP
#define CREDIT_HPP


#include <string>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include "Screen.hpp"
#include "Button.hpp"

class Credits : public virtual Screen{

private:
	std::vector<Button> ListButton;	// Liste des boutons présents sur le screen
public:
	//	#####################################
	//	#	Constructeurs et Destructeur	#
	//	#####################################

	// Constructeur par défaut
	Credits(void);

	// Constructeur paramétré
	Credits(std::string title, std::string fPath, std::string tPath);
	
	// Destructeur
	virtual ~Credits();

	//	#####################################
	//	#		Méthodes principales		#
	//	#####################################

	// Handler principal de la classe
	virtual int		run(sf::RenderWindow *&App);

	// Handler des événements
	virtual int		processEvent(sf::RenderWindow *&App);

};

#endif