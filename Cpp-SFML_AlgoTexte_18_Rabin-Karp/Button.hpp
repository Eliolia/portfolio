#ifndef BUTTON_HPP
#define BUTTON_HPP

#include <string>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

class Button{
private:
	float 			x;
	float 			y;
	bool			isClicked;
	std::wstring	str;
	
	/* Note :
		Le type wstring est composé de wchar_t et permet de supporter les caractères Unicode.
		Il est à savoir que son utilisation est peu recommandée sur Linux (4 bits per wchar_t)
		et plutôt sur Windows (2 bits per wchar_t)		.
	*/


public:

	sf::Texture 	T_button;	// Texture du bouton
	sf::Sprite		S_button;	// Sprite du bouton
	sf::Text 		text;
	sf::Font 		Font;

	//	#####################################
	//	#	Constructeurs et Destructeur	#
	//	#####################################

	// Constructeur par défaut
	Button();

	// Constructeur paramétré
	Button(float posX, float posY, float scaleX, float scaleY, unsigned int txtSize, std::wstring _str, sf::Color bgColor, sf::Color txtColor, std::string fPath, std::string tPath);

	// Destructeur
	virtual ~Button();

	//	#####################################
	//	#		Méthodes principales		#
	//	#####################################

	// Charge une image et l'associe a une texture
	virtual bool	TextureLoad(sf::Texture &t, const std::string path);

	// Charge une police de caractère
	virtual	bool	FontLoad(sf::Font &f, const std::string path);

	// Associe une texture à un sprite donné en paramètre
	virtual void	TextureToSprite(sf::Sprite &s, sf::Texture &t, sf::Color c, std::string path, float _X, float _Y);

	// Configuration du texte associé au bouton
	virtual void	setText(sf::Text &t, std::wstring s, sf::Font f, sf::Color c, unsigned int size);

	// Permet de modifier la position absolue du bouton
	virtual void setTextPosition(float _X, float _Y);

	//	#####################################
	//	#		Getters et setters			#
	//	#####################################
	
	// Retourne la position en abscisse du bouton
	virtual float getX() const;

	// Permet de modifier la position en abscisse du bouton
	virtual void setX(const float _x);

	// Retourne la position en ordonnée du bouton
	virtual float getY() const;

	// Permet de modifier la position en abscisse du bouton
	virtual void setY(const float _y);

	// Retourne l'état actuel du bouton (cliqué ou pas)
	virtual bool getClicked() const;

	// Permet de modifier l'état du bouton
	virtual void setClicked(const bool b);

	// Retourne le string associé au bouton
	virtual std::wstring getStr() const;

	// Permet de modifier le texte associé au bouton
	virtual void setStr(const std::wstring _str);


};

#endif