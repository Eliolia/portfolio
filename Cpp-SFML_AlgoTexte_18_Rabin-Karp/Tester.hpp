#ifndef TESTER_HPP
#define TESTER_HPP

#include <string>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include "Screen.hpp"
#include "Button.hpp"
#include "RB.hpp"

class Tester:public virtual Screen{
private:
	int 	etape;							// Numéro du screen actuel
	std::string		UserSequence;			// Séquence entrée par l'User
	std::string		UserMotif;				// Motif entré par l'User
	int				sizeUserSequence;		// Taille de la séquence
	int				sizeUserMotif;			// Taille du motif
	int 			NbOcc;
	
	std::vector<Button> ListButton;			// Liste des boutons présents
	std::vector<std::wstring> contenu;		// Liste des informations textuelles présentes
											// à l'étape en cours

	std::vector<std::wstring> pressedkey;	// Liste des touches préssés par l'User

	std::vector<sf::Text> Text_Elements;	// Liste des éléments textuels sous format sf::Text
	std::vector<sf::Text> Key_Elements;		// Liste des éléments saisis par l'User sous format sf::Text

public:
	RB* rb;		// Pointeur sur une instance de la classe RaBin Karp

	//	#####################################
	//	#	Constructeurs et Destructeur	#
	//	#####################################

	// Constructeur paramétré
	Tester(std::string title, std::string fPath, std::string tPath);

	// Destructeur
	virtual ~Tester();

	//	#####################################
	//	#		Méthodes principales		#
	//	#####################################

	// Handler principal de la classe
	virtual int		run(sf::RenderWindow *&App);

	// Binding des touches 
	virtual void 	keyBind(sf::Event Event);

	// Handler de la partie saisie de l'écran
	virtual void 	Saisie(sf::RenderWindow *&App);

	// Handler des événements
	virtual int		processEvent(sf::RenderWindow *&App);

	// Handler de l'affichage des résultats de rb->run()
	virtual void 	afficheResultats(sf::RenderWindow *&App);

	//	#####################################
	//	#		Méthodes secondaires		#
	//	#####################################

	// Ajoute un wstring au vector contenu
	virtual void	addContenu(std::wstring c);

	// Modifie un éléments wstring à un index donné
	virtual void	setContenu(int index, std::wstring newContenu);

	// Supprime un élément wstring a un index donné
	virtual void	rmContenu(int index);

	// Retourne la taille totale du vector contenu
	virtual unsigned int	sizeContenu()const;

	// Supprime tous les éléments du vector contenu
	virtual void	resetContenu();

	// Ajoute un wstring au vector pressedkey
	virtual void	addKey(std::wstring c);

	// Supprime tous les éléments du vector pressedkey
	virtual void	resetKey();

	// Retourne la taille totale du vector pressedkey
	virtual unsigned int	sizeKey()const;

	// Supprime la dernière lettre du vector pressedkey
	virtual void 	delKey();

	// Ajoute un élément au vector Text_Elements
	virtual void	addTextElem(sf::Text t);

	// Modifie un éléments à un index donné dans le vector Text_Elements
	virtual void	setTextElem(int index, sf::Text t);

	// Supprime tous les éléments du vector Text_Elements
	virtual void	resetTextElem();

	// Retourne la taille du vector Text_Elements
	virtual unsigned int	sizeTextElem()const;

	// Ajoute une clé dans le vector Key_Elements
	virtual void	addKeyElem(sf::Text t);

	// Modifie une clé du vector Key_Elements à un index donné
	virtual void	setKeyElem(int index, sf::Text t);

	// Supprime tous les éléments du vector Key_Elements
	virtual void	resetKeyElem();

	// Retourne la taille du vector Key_Elements
	virtual unsigned int	sizeKeyElem()const;

	//	#####################################
	//	#		Getters et setters			#
	//	#####################################

	// Retourne la séquence entrée par l'User
	virtual std::string	getUserSequence() const;

	// Permet de modifier la séquence de l'User
	virtual void setUserSequence(std::string us);

	// Retourne le motif rentré par l'User
	virtual std::string	getUserMotif() const;

	// Permet de modifier le motif de l'User
	virtual void setUserMotif(std::string us);

	// Retourne la taille de la séquence
	virtual int getSizeSequence()const;

	// Permet de modifier la taille de la séquence
	virtual void setSizeSequence(const int size);

	// Retourne la taille du motif
	virtual int getSizeMotif()const;

	// Permet de modifier la taille du motif
	virtual void setSizeMotif(const int size);

};


#endif