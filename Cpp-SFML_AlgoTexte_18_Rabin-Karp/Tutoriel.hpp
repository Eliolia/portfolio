#ifndef TUTORIEL_HPP
#define TUTORIEL_HPP

#include <string>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include "Screen.hpp"
#include "Button.hpp"
#include "RB.hpp"

class Tutoriel:public virtual Screen{
private:
	int 	etape;		// Numéro du screen actuel
	std::vector<Button> ListButton;
	std::vector<std::wstring> contenu;
	std::vector<sf::Text> Text_Elements;
	sf::Texture		T_Image;
	sf::Sprite		S_Image;
	bool			isCharged;

public:
	RB* rb;
	int etape_algo_naif;
	bool found_algo_naif;

	Tutoriel();

	Tutoriel(std::string title, std::string fPath, std::string tPath);

	virtual ~Tutoriel();

	virtual int		run(sf::RenderWindow *&App);

	virtual int		processEvent(sf::RenderWindow *&App);

	virtual void	afficheIntro(sf::RenderWindow *&App);

	virtual void 	afficheResultats(sf::RenderWindow *&App);

	virtual void 	afficheCompare(sf::RenderWindow *&App);

	virtual void	addContenu(std::wstring c);

	virtual void	setContenu(int index, std::wstring newContenu);

	virtual void	rmContenu(int index);

	virtual unsigned int	sizeContenu()const;

	virtual void	resetContenu();

	virtual void	addTextElem(sf::Text t);

	virtual void	setTextElem(int index, sf::Text t);

	virtual void	resetTextElem();

	virtual unsigned int	sizeTextElem()const;

	virtual void AlgoNaif(sf::RenderWindow *&App);

};


#endif