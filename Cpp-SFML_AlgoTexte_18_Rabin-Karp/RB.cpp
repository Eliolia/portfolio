#include "RB.hpp"


RB::RB(){
	this->sequence = "ROUDOUDOU";
	this->motif    = "OU";
	this->premier  = 113;
	this->charset  = 256;
	this->occ      = 0;
	this->seq_len  = sequence.length();
	this->motif_len= motif.length();
	this->hashPat  = 0;
	this->hashSeq  = 0;
	this->hashVal  = 1;

}

RB::RB(std::string seq, std::string pat, int p){
	this->sequence = seq;
	this->motif    = pat;
	this->premier  = p;
	this->charset  = 256;
	this->occ      = 0;
	this->seq_len  = sequence.length();
	this->motif_len= motif.length();
	this->hashPat  = 0;
	this->hashSeq  = 0;
	this->hashVal  = 1;
}

RB::~RB(){

	tabSeq.clear();
	positions.clear();

}

int	RB::run(){
	int i, j;
	this->seq_len  	= sequence.length();
	this->motif_len	= motif.length();

	if(motif_len > 0 && seq_len > 0)
	{// Calcul de la vsaleur de hashage générale
		for(i = 0; i < this->getMotifLen() - 1; i++){
			this->hashVal = (this->hashVal * getCharset()) % getPremier();
		}
	
		// Calcul de la valeur de hashage du motif et de la première
		// sous-séquence
		for(i = 0; i < motif_len; i++){
			this->hashPat = (this->hashPat * getCharset() + motif[i]) % getPremier();
			this->hashSeq = (this->hashSeq * getCharset() + sequence[i]) % getPremier();
		}
	
		tabSeq.push_back(std::pair<std::string,std::string>(sequence.substr(0, motif_len), std::to_string(hashSeq)));
		std::cout << "Sous-Séquence et hash : \t" << tabSeq[0].first << "\t" <<  tabSeq[0].second << std::endl;
	
		// Chevauchement du motif sur la séquence
		for(i = 0; i <= getSeqLen() - getMotifLen(); i++){
			// Si hash(pat) == hash(seq)
			if(getHashPat() == getHashSeq()){
				// On vérifie char par char
				for(j = 0; j < getMotifLen(); j++){
					if(sequence[i+j] != this->motif[j])
						break; // char différent, on arrête
				}
	
				// Si motif[0..motif_len - 1] == seq[i.. i + motif_len -1]
				// donc si motif et sous-séquence font la même taille
				if(j == getMotifLen()){
					this->occ += 1;
					this->positions.push_back(std::to_string(i));
				}
			}
	
			// Calcul de la valeur de hashage de la prochaine
			// sous-séquence
			if(i < getSeqLen() - getMotifLen()){
				this->hashSeq = (getCharset() * (this->hashSeq - this->sequence[i] * getHashVal())+ this->sequence[i + getMotifLen()]) % getPremier();
				if(getHashSeq() < 0)
					this->hashSeq = this->hashSeq + getPremier();
			}
	
			tabSeq.push_back(std::pair<std::string,std::string>(sequence.substr(i+1, motif_len), std::to_string(hashSeq)));
			std::cout << "Sous-Séquence et hash : \t" << tabSeq[i+1].first << "\t" <<  tabSeq[i+1].second << std::endl;
	
		}
		std::cout << "Position des motifs dans la séquence : ";
		for(size_t k = 0; k < positions.size(); k++) 
			std::cout << " " << positions[k];
		std::cout << std::endl;}

	std::cout << "Occurences trouvées : " << getOcc() << std::endl;

	return getOcc();

}

void RB::reset(){
	this->setHashPat(0);
	this->setHashSeq(0);
	this->setHashVal(1);
	this->tabSeq.clear();
	this->positions.clear();
}

std::string RB::getSequence() const{
	return this->sequence;
}

void RB::setSequence(const std::string seq){
	this->sequence = seq;
}

int	RB::getSeqLen() const{
	return this->seq_len;
}

std::string RB::getMotif() const{
	return this->motif;
}

void RB::setMotif(const std::string pat){
	this->motif = pat;
}

int RB::getMotifLen()const{
	return this->motif_len;
}

int RB::getPremier() const{
	return this->premier;
}

void RB::setPremier(const int p){
	this->premier = p;
}

int RB::getCharset() const{
	return this->charset;
}

void RB::setCharset(const int c){
	this->charset = c;
}

int RB::getOcc() const{
	return this->occ;
}

void RB::setOcc(const int _occ){
	this->occ = _occ;
}

int RB::getHashPat()const{
	return this->hashPat;
}
void RB::setHashPat(const int p){
	this->hashPat = p;
}

int RB::getHashSeq()const{
	return this->hashSeq;
}
void RB::setHashSeq(const int s){
	this->hashSeq = s;
}
int RB::getHashVal()const{
	return this->hashVal;
}
void RB::setHashVal(const int h){
	this->hashVal = h;
}






