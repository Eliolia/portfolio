#include "Menu.hpp"

Menu::Menu(void): Screen(){}
Menu::Menu(std::string title, std::string fPath, std::string tPath):Screen(title,fPath, tPath){}

Menu::~Menu(){
}

int	Menu::run(sf::RenderWindow *&App){
	setRunning(true);

	unsigned int 		menu_font_size = App->getSize().y * 0.05;   // Taille des fonts Menu
	float 		X_pos_button = App->getSize().x * 0.8; // Position des bouttons en X
	float		Y_pos_button = App->getSize().y;

	Button Tutoriel((X_pos_button), (Y_pos_button*0.325f), 0.8f, 0.6f, menu_font_size, L"Tutoriel", sf::Color(255,255,255,this->getAlpha()), sf::Color(255,255,255,this->getAlpha()), "./Contents/BebasNeue-Regular.ttf", "./graphismes/bouton/tutoriel.png");

	Button Tester((X_pos_button), (Y_pos_button*0.475f), 0.8f, 0.6f, menu_font_size, L"Tester", sf::Color(255,255,255,this->getAlpha()), sf::Color(255,255,255,this->getAlpha()), "./Contents/BebasNeue-Regular.ttf", "./graphismes/bouton/tester.png");

	Button Credits((X_pos_button), (Y_pos_button*0.625f), 0.8f, 0.6f, menu_font_size, L"Crédits", sf::Color(255,255,255,this->getAlpha()), sf::Color(255,255,255,this->getAlpha()), "./Contents/BebasNeue-Regular.ttf", "./graphismes/bouton/credits.png");

	Button Quitter((X_pos_button), (Y_pos_button*0.775f), 0.8f, 0.6f, menu_font_size, L"Quitter", sf::Color(255,255,255,this->getAlpha()), sf::Color(255,255,255,this->getAlpha()), "./Contents/BebasNeue-Regular.ttf", "./graphismes/bouton/quitter.png");

	ListButton.push_back(Tutoriel);
	ListButton.push_back(Tester);
	ListButton.push_back(Credits);
	ListButton.push_back(Quitter);	


	while(this->getRunning()){
		int error = processEvent(App);
		switch(error){
			case -1:
				App->close();
		    	setRunning(false);
				return -1;
			break;
			case 1:
				setRunning(false);
				return 1;
			break;
			case 2:
				setRunning(false);
				return 2;
			break;
			case 3:
				setRunning(false);
				return 3;
			break;
		}

		App->clear();

		App->draw(S_bg);

		for(size_t i = 0; i < ListButton.size(); i++){
			App->draw(ListButton[i].S_button);
		}

		App->display();
	}
	return 0;
}

int		Menu::processEvent(sf::RenderWindow *&App){
	sf::Event Event;
	while(App->pollEvent(Event)){
		// L'user a fermé la fenêtre
		if(Event.type == sf::Event::Closed){
			App->close();
			this->setRunning(false);
			return -1;
		}

		// Si on a fait un clic de souris left
		if (Event.mouseButton.button == sf::Mouse::Left)
		{
			// Bouton Tester
			if(isButtonClicked(&ListButton[0].S_button, App)){
		    	std::cout << "\033[1;31mL'utilisateur va dans le tutoriel\033[0;m" << std::endl;
		    	return 1;
		    }

		    // Bouton Tester
			if(isButtonClicked(&ListButton[1].S_button, App)){
		    	std::cout << "\033[1;32mL'utilisateur va tester rabin-karp\033[0;m" << std::endl;

		    	return 2;
		    }

		    // Bouton Crédits
			if(isButtonClicked(&ListButton[2].S_button, App)){
		    	std::cout << "\033[1;33mL'utilisateur va dans les crédits\033[0;m" << std::endl;
		    	return 3;
		    }

		    // Bouton Quitter
		    if(isButtonClicked(&ListButton[3].S_button, App)){
		    	std::cout << "L'utilisateur a quitté l'application" << std::endl;
		    	App->close();
		    	setRunning(false);
		    	return -1;
		    }
		}

		// Si on a fait un clic de souris droit
		if (Event.mouseButton.button == sf::Mouse::Right)
		{
		    std::cout << "the right button was pressed" << std::endl;
		    std::cout << "mouse x: " << Event.mouseButton.x << std::endl;
		    std::cout << "mouse y: " << Event.mouseButton.y << std::endl;
		}

	}
	return 0;
}