package csp_etud;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Application {

	public static void main(String[] args) throws IOException {
		
		String fileName = "";
		// String fileName = "instance.txt";
		// String fileName = "5reines.txt";
		// String fileName = "zebre.txt";
		Network myNetwork = null;
		
		System.out.println("### Solveur CSP ###");
		
		System.out.print("Entrez le nom du fichier : ");
		Scanner sc = new Scanner(System.in);
		fileName = sc.nextLine();
		sc.close();
		
		System.out.println("Chargement du fichier : "+ new java.io.File(".").getCanonicalPath()+ "/"+ fileName);
		try {
			BufferedReader readFile = new BufferedReader(new FileReader(fileName));
			myNetwork = new Network(readFile);
			readFile.close();
			System.out.println("Chargement reussi!");
		}catch(Exception e) {
			System.err.println(e.getMessage());
		}
		
		CSP csp = new CSP(myNetwork);
		CSP cspAll = new CSP(myNetwork);

		long startTime = System.nanoTime();
		csp.searchSolution(); 			// Une solution du probleme
		long realTime = System.nanoTime() - startTime;
		System.out.println("Temps execution BT : "+realTime/1000000. + " ms");
		
		startTime = System.nanoTime();
		cspAll.searchAllSolutions();	// Toutes les solutions
		realTime = System.nanoTime() - startTime;
		System.out.println("Temps execution BTAll : "+realTime/1000000. + " ms");
		
		System.out.println("### Fin  ###");
	}

}
