# Backtracking with CSP

## En résumé

Proposition d'une implémentation d'un moteur de résolution de problèmes de satisfaction de contraintes (CSP).

Il va rechercher des solutions par l'établissement d'un réseau de contraintes et en y appliquant un algorithme de recherche par retour arrière (backtracking) en affectant une variable à fois par noeud de l'arbre de recherche.

Chaque assignation d'une valeur à une variable est testée afin de déterminer sa consistance (violation d'aucune contraintes).

## Implémentation

L'application a été réalisé en Java 1.8 en utilisant des librairies standards. 

On établit d'abord un ensemble de contraintes diverses (différence, égalité, évaluation d'expression booléenne, contraintes en extension). De là, on crée un réseau de contrainte (_Network_) en lisant des fichiers textes au format suivant :

```
Nombre de variables
Nom de la var1 ; val1 ; val2 ; ... ;
Nom de la var2 ; val1 ; val2 ; ... ;
Type de contrainte (ext, dif, eq, exp)

```

De ce réseau de contrainte, on applique un algorithme de backtracking qui va assigner pour chaque variable une valeur et vérifier la consistance (validité) de l'assignation.

Quand une assignation est complète (chaque variable est affectée sans violer aucune contrainte du problème), elle est alors considéré comme étant une solution.

## Exécuter

Assurez-vous d'avoir à disposition un terminal en ligne de commande et d'avoir un environnement d'exécution Java (_[Java Runtime Environment](https://www.oracle.com/java/technologies/javase-jre8-downloads.html)_)

Depuis un terminal, accédez au dossier "_CSP_" et exécutez le fichier _csp.jar_ avec la commande `java -jar csp.jar`.

Plusieurs fichiers textes sont disponibles dans le dossier "_CSP_" et vous serrez invité à saisir le nom de l'un deux afin d'obtenir leurs solutions si elles existent. Par exemple, le célèbre _[Zebra Puzzle](https://en.wikipedia.org/wiki/Zebra_Puzzle)_ est modélisé dans le fichier `zebreOpt.txt`. 

## Améliorations

* ajouter des types de contraintes plus génériques ;
* implémenter un forwardchecking pour prévenir des erreurs possibles et accélerer l'algorithme de backtracking ;
* mettre en place le choix de différentes heuristiques de recherche (min-conflict, order-domain-values, min/max-degrees, min-rest-values, ...)

