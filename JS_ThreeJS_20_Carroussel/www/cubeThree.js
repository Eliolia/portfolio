var renderer = null,
    scene = null,
    camera = null,
    cube = null,
    starGeometry = null,
    starMaterial = null,
    ground = null,
    cylinder = null,
    cone = null,
    carousel = null,
    cylinder2 = null,
    whiteCup = null,
    blackCup = null,
    R2D2 = null,
    Carousel_Horse = null;

main();
function main() {
    var canvas = document.getElementById("glcanvas");
    // Create the Three.js renderer and attach it to our canvas
    renderer = new THREE.WebGLRenderer( { canvas: canvas, antialias: true } );
    renderer.physicallyCorrectLights = true;
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    // Set the viewport size
    renderer.setSize(canvas.width, canvas.height);
    document.body.appendChild( renderer.domElement );
    // Create a new Three.js scene
    scene = new THREE.Scene();
    // Add  a camera so we can view the scene
    camera = new THREE.PerspectiveCamera( 45, canvas.width / canvas.height, 1, 400);
    camera.position.z = 20;
    camera.lookAt(scene.position);
    scene.add(camera);

    createContent();
    createLights(scene);
    ground.castShadow = false;
    ground.receiveShadow = true;
    cylinder.castShadow = true;
    cylinder.receiveShadow = true;
    cylinder2.castShadow = true;
    cylinder2.receiveShadow = true;
    cone.castShadow = true;
    cone.receiveShadow = true;

    // Finally, add the mesh to our scene
    scene.add( ground );
    scene.add( cylinder );
    scene.add( cylinder2 );
    scene.add( cone );

    var orbit = new THREE.OrbitControls( camera);
    var axesHelper = new THREE.AxesHelper( 20 );
    scene.add( axesHelper );

    carousel = new THREE.Group();
    carousel.add(ground);
    carousel.add(cylinder);
    carousel.add(cylinder2);
    carousel.add(cone);

    // Cup
    var mtlLoader = new THREE.MTLLoader();
    mtlLoader.setResourcePath('./models/Cups/');
    mtlLoader.setPath('./models/Cups/');
    mtlLoader.load('cup2.mtl', function (materials) {
        materials.preload();
        let objLoader = new THREE.OBJLoader();
        objLoader.setMaterials(materials);
        objLoader.setPath('./models/Cups/');
        objLoader.load('cup.obj', function (object) {
        object.scale.set(0.1,0.1,0.1);
        object.position.set(10,2,0);
        whiteCup = object;
        whiteCup.traverse( function( node ) { if ( node instanceof THREE.Mesh ) { node.castShadow = true; node.receiveShadow = true; } } );
        carousel.add(whiteCup);
        });
    });

    var mtlLoader2 = new THREE.MTLLoader();
    mtlLoader2.setResourcePath('./models/Cups/');
    mtlLoader2.setPath('./models/Cups/');
    mtlLoader2.load('cup.mtl', function (materials) {
        materials.preload();
        let objLoader2 = new THREE.OBJLoader();
        objLoader2.setMaterials(materials);
        objLoader2.setPath('./models/Cups/');
        objLoader2.load('cup.obj', function (object) {
        blackCup = object;
        object.scale.set(0.1,0.1,0.1);
        object.position.set(-10,2,0);
        blackCup.traverse( function( node ) { if ( node instanceof THREE.Mesh ) { node.castShadow = true; node.receiveShadow = true; } } );
        carousel.add(blackCup);
        });
    });

    var mtlLoader3 = new THREE.MTLLoader();
    mtlLoader3.setResourcePath('./models/R2D2/');
    mtlLoader3.setPath('./models/R2D2/');
    mtlLoader3.load('r2-d2.mtl', function (materials) {
    console.log(materials);
    materials.preload();
    let objLoader3 = new THREE.OBJLoader();
    objLoader3.setMaterials(materials);
    objLoader3.setPath('./models/R2D2/');
    objLoader3.load('r2-d2.obj', function (object) {
        R2D2 = object;
        object.position.set(0,0,-10);
        object.rotation.y = Math.PI / 2;
        object.scale.set(0.05,0.05,0.05);
        R2D2.traverse( function( node ) { if ( node instanceof THREE.Mesh ) { node.castShadow = true; node.receiveShadow = true; } } );
        carousel.add(R2D2);
    })});

    var mtlLoader4 = new THREE.MTLLoader();
    mtlLoader4.setResourcePath('./models/Carousel_Horse/');
    mtlLoader4.setPath('./models/Carousel_Horse/');
    mtlLoader4.load('Blank.mtl', function (materials) {
        materials.preload();
        let objLoader4 = new THREE.OBJLoader();
        objLoader4.setMaterials(materials);
        objLoader4.setPath('./models/Carousel_Horse/');
        objLoader4.load('Horse.obj', function (object) {
        object.scale.set(0.05,0.05,0.05);
        object.position.set(0,0,10);
        object.rotation.x = - Math.PI / 2;
        object.rotation.z =  Math.PI / 2;
        Carousel_Horse = object;
        Carousel_Horse.traverse( function( node ) { if ( node instanceof THREE.Mesh ) { node.castShadow = true; node.receiveShadow = true; } } );
        carousel.add(Carousel_Horse);
        });
    });


    initSound();
    scene.add(carousel);

    // Run the run loop
    renderer.setAnimationLoop( () => {
        // tells the browser we want to perform an animation
        update();
        render();
    });
};

function update () {
    var angle = .005;
    carousel.rotation.y += angle;
    if(whiteCup != null && blackCup != null){
        var angle2 = .01;
        whiteCup.rotation.y += angle2;
        blackCup.rotation.y += angle2;
    }
};

// render, or 'draw a still image', of the scene
function render() {
renderer.render( scene, camera );
};

function createLights( scene ) {
    // Add a directional light to show off the object
    var light = new THREE.DirectionalLight( 0xffffff, 1.5);
    // Position the light out from the scene, pointing at the origin
    light.position.set(0, 0, 10);
    light.rotation.x = - Math.PI / 4;
    light.castShadow = true;

    //Set up shadow properties for the light
    light.shadow.mapSize.width = 512; // default
    light.shadow.mapSize.height = 512; // default
    light.shadow.camera.near = 0.5; // default
    light.shadow.camera.far = 500; // default
    light.shadow.camera.left = -20;
    light.shadow.camera.right = 20;
    light.shadow.camera.top = 20;
    light.shadow.camera.bottom = -20;
    scene.add( light );

    //var starSkybox = new THREE.Mesh(starGeometry, starMaterial);
    textureURLs = [ // URLs of the six faces of the cube map
    "./textures/cube/posx.jpg",
    "./textures/cube/negx.jpg",
    "./textures/cube/posy.jpg",
    "./textures/cube/negy.jpg",
    "./textures/cube/posz.jpg",
    "./textures/cube/negz.jpg"
    ];
    // skybox
    var textures = loadTextures(textureURLs);
    var materials = [];
    for (var i = 0; i < 6; i++) {
    materials.push( new THREE.MeshBasicMaterial( {
    color: "white",
    // IMPORTANT: To see the inside of the cube, back faces must be rendered!
    side: THREE.BackSide,
    map: textures[i]
    } ) );
    }
    cube = new THREE.Mesh(new THREE.CubeGeometry(100,100,100), materials );
    scene.add(cube);
    function loadTextures(textureURLs) {
        var loaded = 0;
        function loadedOne() {
            loaded++;
            if (loaded == textureURLs.length) {
                for (var i = 0; i < textureURLs; i++)
                textures[i].needsUpdate = true;
                }
        }
        var textures = [];
        for (var i = 0; i < textureURLs.length; i++) {
        var tex = new THREE. TextureLoader().load( textureURLs[i], undefined, loadedOne );
        textures.push(tex);
        }
        return textures;
    }
    //scene.add(starSkybox);

    const ambientLight = new THREE.HemisphereLight(
        0xddeeff, // sky color
        0x202020, // ground color
        3, // intensity
    );
    scene.add(ambientLight);
};

function createContent() {
    // Stars’ geometry and material
    starGeometry = new THREE.SphereGeometry(200, 50, 50);
    starMaterial = new THREE.MeshPhongMaterial({
    map: new THREE. TextureLoader().load ("./textures/stars.png"),
    side: THREE.DoubleSide,
    shininess: 0
    });

    var groundGeometry = new THREE.CylinderBufferGeometry(15, 15, 1, 32);
    var groundMaterial = new THREE.MeshPhongMaterial({
    map: new THREE. TextureLoader().load ("./textures/spiral-retro-background.jpg"),
    side: THREE.FrontSide,
    shininess: 2
    });
    ground = new THREE.Mesh(groundGeometry, groundMaterial);

    var cylinderGeometry = new THREE.CylinderBufferGeometry(1, 1, 10, 32);
    var cylinderMaterial = new THREE.MeshStandardMaterial( {color: 0x3287a8, roughness: 0.3, metalness: 1});
    cylinder = new THREE.Mesh(cylinderGeometry, cylinderMaterial);
    cylinder.position.y = 5;

    var cylinderGeometry2 = new THREE.CylinderBufferGeometry(15, 15, 0.5, 32);
    var cylinderMaterial2 = new THREE.MeshPhongMaterial({
    map: new THREE. TextureLoader().load ("./textures/gold.jpg"),
    side: THREE.FrontSide,
    shininess: 2
    });
    cylinder2 = new THREE.Mesh(cylinderGeometry2, cylinderMaterial2);
    cylinder2.position.y = 9;

    var coneGeometry = new THREE.ConeBufferGeometry(14.5,3,32);
    var texture = new THREE.TextureLoader().load( "./textures/gold.jpg" );
    texture.wrapS = THREE.RepeatWrapping;
    texture.wrapT = THREE.RepeatWrapping;
    texture.repeat.set( 10, 10 );
    var hatMaterial = new THREE.MeshPhongMaterial({
    map: texture, side: THREE.FrontSide, shininess: 10
    });
    
    cone = new THREE.Mesh(coneGeometry, hatMaterial);
    cone.position.y = 10.5;


};

window.addEventListener( 'resize', function () {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}, 
false );

function initSound() {
    // instantiate a listener
    var audioListener = new THREE.AudioListener();
    // add the listener to the camera
    camera.add( audioListener );
    // instantiate audio object
    ambientSound = new THREE.Audio( audioListener );
    // add the audio object to the scene
    scene.add( ambientSound );
    // instantiate a loader
    var loader = new THREE.AudioLoader();
    // load a resource
    loader.load('./sounds/ambient.mp3',
    // Function when resource is loaded
    function ( audioBuffer ) {
            ambientSound.setBuffer( audioBuffer );
            ambientSound.setLoop( true );
            ambientSound.setVolume( 0.5 );
            ambientSound.play();
        });
    document.body.addEventListener('keydown', toggle);
}

function toggle () {
    if ( ambientSound.isPlaying === true ) {
        ambientSound.pause();
    }
    else {
        ambientSound.play();
    }
}
