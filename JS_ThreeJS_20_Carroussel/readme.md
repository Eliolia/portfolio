# Carroussel

## Résumé

Ce projet constitue une prise en main du framework three.js pour le rendu de scènes 3D dans un navigateur.

Il s'agit d'un carroussel dans lequel des modèles 3D sont chargés, texturés et tournent sur eux-même, le tout mis en place dans un graphe de scène.

Il y a également une gestion des ombres (génération, projection, captation).

## Implémentation

Le projet a été implémenté en javascript avec le framework three.js dont le rendu est fait dans un module WebGL.

## Exécuter

Le projet se trouve dans le dossier "www". Pour l'exécuter en local, il faut déposer le dossier sur un webserveur (par exemple [Servez](https://greggman.github.io/servez/)) puis lancer "[127.0.0.1:8080/index.html](127.0.0.1:8080/index.html)" sur un navigateur.

## Démonstration vidéo

Dans le dossier "Démonstration" se trouve la vidéo en question
