# Equilibrium_Rebirth

## Résumé

Le jeu a été créé initialement lors de la Climate Game Jam 2018. Une compétition que j'ai moi-même co-organisée avec les associations Hakatah et Hello World, en coopération avec de multiples acteurs (Ministère de la transition écologique et des solidarités, CNRS, Push Start, etc).

Il s'agit d'une adaptation en 3D et d'une amélioration du jeu notamment par l'implémentation d'un terrain généré procéduralement (bruit de Perlin) et d'une machine à états pour diriger les agents intelligents.

C'est un jeu sérieux de Stratégie-Survie vous permettant de contrôler la vie d'une colonie bactérienne et de son avenir. L'évolution et l'adaptation sont la clef de la survie.

Toutes les compositions musicales sont originales et ont été faites avec LMMS, Audacity et MuseScore3. Les sons sont libres de droits.

## Comment jouer ?

Vous devez, en incarnant une colonie d'organismes aléatoire, vous adapter à votre environnement en subvenant aux besoins de votre celle-ci par des migrations successives, en évitant les prédateurs (virus, autres colonies bactériennes, etc) et en vous renforçant pour vous défendre face à ces derniers.

Vous pouvez déplacer la colonie par un clic gauche sur le terrain à l'endroit désiré, bouger la caméra à l'aide des flèches directionnelles ou des touches ZQSD et pivoter la caméra avec le clic central de la souris.

Vous disposez d'un HUD vous indiquant l'énergie totale de votre colonie (en jaune), sa consommation énergétique (en rouge), ses ressources d'évolution (en violet/fushia), un indicateur de population et un score. De même, vous disposez de 2 panneaux supplémentaires, l'un sur la situation climatique de la zone où vous vous trouvez ainsi que vos scores d'adaptation, l'autre de contrôle pour gérer votre colonie par des actions.  

Vous perdez si votre colonie meurt.

## Implémentation

Le projet a été développé sous Unity 2019.1.x.

## Améliorations envisageables

* Plus de comportements des micro-organismes ;
* Rendre le terrain plus smooth ;
* Plus de variations de terrain (îles, etc) ;
* Système météo visuel ;
* Personnaliser les shaders ;
* Système jour/nuit avec des effets ;
* Modes de priorité de la colonie (défense, attaque, récolte, etc) ;

## Bug report

* Adaptation du Navmesh au terrain procédural fait que la colonie peut apparaître sous celui-ci.

## Exécuter

Le problème du Navmesh empêche d'avoir une version fonctionnelle du projet en build.

Ouvrez le projet sur Unity 19 et lancez-le.

## Démonstration vidéo

Dans le dossier "Démonstration" se trouve la vidéo en question
