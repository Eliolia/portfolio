﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine;
using UnityEngine.UI;

public class EffectVolumeScript : MonoBehaviour
{
    private Slider effectVolume;

    private void Start()
    {
        effectVolume = this.GetComponent<Slider>();
        effectVolume.value = GameSettings.EffectVolumeLevel;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameSettings.EffectVolumeLevel != effectVolume.value)
            GameSettings.EffectVolumeLevel = effectVolume.value;
    }
}
