﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicVolumeScript : MonoBehaviour
{
    private Slider musicVolume;

    private void Start()
    {
        musicVolume = this.GetComponent<Slider>();
        musicVolume.value = GameSettings.MusicVolumeLevel;
    }

    // Update is called once per frame
    void Update()
    {
        if(GameSettings.MusicVolumeLevel != musicVolume.value)
            GameSettings.MusicVolumeLevel = musicVolume.value;
    }
}
