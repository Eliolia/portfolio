﻿
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public float panSpeed = 10f;
    public float panBorderThickness = 10f;
    public float rotateSpeed = 50f;
    public float rotateAmount = 50f;

    public Quaternion rotation;
    public Vector2 panLimit;

    public float scrollSpeed = 2f;
    public float minY = 20f;
    public float maxY = 100f;

    public Transform CamPivot;
    public Transform Cam;

    private void Start()
    {
        rotation = transform.rotation;
    }

    private void Update()
    {
        MoveCamera();
        RotateCamera();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            transform.rotation = rotation;
        }
    }

    private void MoveCamera()
    {
        Vector3 camF = Cam.forward;
        Vector3 camR = Cam.right;
        camF.y = 0;
        camR.y = 0;
        camF = camF.normalized;
        camR = camR.normalized;


        Vector3 pos = transform.position;

        if (Input.GetKey("z") || Input.mousePosition.y >= Screen.height - panBorderThickness)
        {
            pos.z += camF.z * panSpeed * Time.deltaTime;
        }

        if (Input.GetKey("s") ||  Input.mousePosition.y <= panBorderThickness)
        {
            pos.z -= camF.z * panSpeed * Time.deltaTime;
        }

        if (Input.GetKey("d") ||  Input.mousePosition.x >= Screen.width - panBorderThickness)
        {
            pos.x += camR.x *panSpeed * Time.deltaTime;
        }

        if (Input.GetKey("q") || Input.mousePosition.x <= panBorderThickness)
        {
            pos.x -= camR.x * panSpeed * Time.deltaTime;
        }

        float scroll = Input.GetAxis("Mouse ScrollWheel");
        pos.y -= scroll * scrollSpeed * Time.deltaTime;

        pos.x = Mathf.Clamp(pos.x, -panLimit.x, panLimit.x);
        pos.y = Mathf.Clamp(pos.y, minY, maxY);
        pos.z = Mathf.Clamp(pos.z, -panLimit.y, panLimit.y);

        transform.position = pos;
    }

    private void RotateCamera()
    {
        Vector3 origin = transform.eulerAngles;
        Vector3 dest = origin;

        if (Input.GetMouseButton(2))
        {
            dest.x -= Input.GetAxis("Mouse Y") * rotateAmount;
            dest.y += Input.GetAxis("Mouse X") * rotateAmount;

        }

        if(dest != origin)
        {
            transform.eulerAngles = Vector3.MoveTowards(origin, dest, Time.deltaTime * rotateSpeed);
        }
    }
}
