﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefendState : BaseState
{
    private float _attackReadyTime;
    private Organism _organism;

    public DefendState(Organism organism) : base(organism.gameObject)
    {
        _organism = organism;
    }


    public override Type Tick()
    {
        if(_organism.Target == null)
        {
            if (_organism.Team == Team.Colonie)
                return typeof(WanderState);
            else return typeof(WanderStateOthers);
        }

        _attackReadyTime -= Time.deltaTime;

        if(_attackReadyTime <= 0f)
        {
            _organism.LetsFire();
        }
        return null;
    }

}
