﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderStateOthers : BaseState
{
    private Vector3? _dest;
    private float stopDist = 1f;
    private float turnSpeed = 1f;
    private readonly LayerMask _layerMask = LayerMask.NameToLayer("Walls");
    private float _rayDist = 4f;

    private Quaternion _desiredRotation;
    private Vector3 _direction;
    protected Organism _organism;

    public WanderStateOthers(Organism organism) : base(organism.gameObject)
    {
        _organism = organism;
    }

    public override Type Tick()
    {
        var chaseTarget = CheckForAggro();
        if (chaseTarget != null)
        {
            _organism.SetTypeOfTarget(chaseTarget.Item1);
            _organism.SetTarget(chaseTarget.Item2);
            if (_organism.TypeofTarget == "Organism")
            {
                return typeof(ChaseState);
            }

            if(_organism.Team != Team.Colonie && _organism.TypeofTarget == "Player")
            {
                return typeof(ChaseState);
            }
                
        }

        if (_dest.HasValue == false || Vector3.Distance(transform.position, _dest.Value) <= stopDist)
        {
            FindRandomDestination();
        }

        transform.rotation = Quaternion.Slerp(transform.rotation, _desiredRotation, Time.deltaTime * turnSpeed);

        if (IsForwardBlocked())
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, _desiredRotation, 0.2f);
        }
        else
        {
            transform.Translate(Vector3.forward * Time.deltaTime * GameSettings.OrganismMoveSpeed);
        }

        Debug.DrawRay(transform.position, _direction * _rayDist, Color.red);
        while (IsPathBlocked())
        {
            FindRandomDestination();
        }

        return null;
    }

    private bool IsForwardBlocked()
    {
        Ray ray = new Ray(transform.position, transform.forward);
        return Physics.SphereCast(ray, 0.5f, _rayDist, _layerMask);
    }

    private bool IsPathBlocked()
    {
        Ray ray = new Ray(transform.position, _direction);
        return Physics.SphereCast(ray, 0.5f, _rayDist, _layerMask);
    }

    private void FindRandomDestination()
    {
        Vector3 testPos = (transform.position + (transform.forward * 4f)) + new Vector3(UnityEngine.Random.Range(-4.5f, 4.5f), 0f, UnityEngine.Random.Range(-4.5f, 4.5f));

        _dest = new Vector3(testPos.x, 1f, testPos.z);
        _direction = Vector3.Normalize(_dest.Value - transform.position);
        _direction = new Vector3(_direction.x, 0f, _direction.z);
        _desiredRotation = Quaternion.LookRotation(_direction);
    }

    Quaternion startingAngle = Quaternion.AngleAxis(-120, Vector3.up);
    Quaternion stepAngle = Quaternion.AngleAxis(5, Vector3.up);

    protected virtual Tuple<string, Transform> CheckForAggro()
    {
        RaycastHit hit;
        var angle = transform.rotation * startingAngle;
        var direction = angle * Vector3.forward;
        var pos = transform.position;
        for (int i = 0; i < 30; i++)
        {
            if (Physics.Raycast(pos, direction, out hit, GameSettings.AggroRadius))
            {
                var organism = hit.collider.GetComponent<Organism>();
                var player = hit.collider.GetComponent<PlayerBehaviour>();

                if (organism != null && organism.Team != gameObject.GetComponent<Organism>().Team)
                {
                    Debug.DrawRay(pos, direction * hit.distance, Color.red);

                    return new Tuple<string, Transform>("Organism", organism.transform);
                }
                else if(player != null)
                {
                    Debug.DrawRay(pos, direction * hit.distance, Color.red);
                    return new Tuple<string, Transform>("Player", player.transform);
                }
                else
                {
                    Debug.DrawRay(pos, direction * hit.distance, Color.yellow);
                }
            }
            else
            {
                Debug.DrawRay(pos, direction * GameSettings.AggroRadius, Color.white);
            }
            direction = stepAngle * direction;
        }

        return null;
    }

}
