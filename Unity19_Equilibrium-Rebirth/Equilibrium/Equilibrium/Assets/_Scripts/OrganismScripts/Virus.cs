﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Virus : Organism
{
    
    private void Awake()
    {
        worldObject = GameObject.Find("World");
        InitializeStateMachine();
    }

    void Start()
    {
        _currentTick = 0;

        this._team = Team.Virus;
        this._level = Level.Normal;
        this._consoEnergie = 0;
        this._consoTime = 0;

        this.HP = GameSettings.VirusMaxHP;

        GameVariables.NbVirus++;

        StartCoroutine(HealthManager(this));
    }

    // Update is called once per frame
    void Update()
    {
        HPBarFollowCamera();
    }

    protected override void InitializeStateMachine()
    {
        var states = new Dictionary<Type, BaseState>
        {
            {typeof(WanderStateOthers), new WanderStateOthers(this) },
            {typeof(ChaseState), new ChaseState(this) },
            {typeof(DefendState), new DefendState(this) }
        };

        GetComponent<StateMachine>().SetStates(states);
    }

    protected IEnumerator HealthManager(Virus _virus)
    {
        while (true)
        {
            yield return new WaitForSeconds(GameSettings.OrganismTimeToCheckHealth);

            _virus._healthBar.fillAmount = _virus.HP / GameSettings.VirusMaxHP;

            if (_virus.HP <= 0)
            {
                Destroy(_virus.gameObject);
                GameVariables.NbVirus--;

            }
        }
    }
}
