﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Noise
{

    public enum NormalizeMode { Local, Global }

    public static float[,] GenerateNoiseMap(int mapWidth, int mapHeight, int seed, float scale, int octaves, float persistance, float lacunarity, Vector2 offset, NormalizeMode normalizeMode) 
        // seed va permettre de générer plein de noiseMap uniques, sorte de génération aléatoire beaucoup utilisée genre dans minecraft
        // en 2D, scale pour avoir des nombres à virgules pour la coloration de la map. 
        // octaves sert à ajouter des détails important qu'à l'octave précédente
        // la persistance en 0 et 1 sert à ajouter un niveau de détails de moins en moins important,
        // la lacunarity toujours supérieure à 1
        // offset si on veut influer manuellement sur le noise.
    {
        float[,] noiseMap = new float[mapWidth, mapHeight];

        System.Random prng = new System.Random(seed); // generation de nombres pseudo aléatoires
        Vector2[] octaveOffsets = new Vector2[octaves]; // pour un look plus naturel du terrain, décalage

        float maxPossibleHeight = 0;
        float amplitude = 1;
        float frequency = 1; // va augmenter à chaque octave 


        for (int i = 0; i < octaves; i++)
        {
            // si trop haut la valeur sera "ignorée"
            float offsetX = prng.Next(-100000, 100000) + offset.x;
            float offsetY = prng.Next(-100000, 100000) - offset.y;
            octaveOffsets[i] = new Vector2(offsetX, offsetY);

            maxPossibleHeight += amplitude;
            amplitude *= persistance;
        }

        if(scale <= 0)
        {
            scale = 0.0001f;
        }

        float maxLocalNoiseHeight = float.MinValue;
        float minLocalNoiseHeight = float.MaxValue;

        //générer depuis le milieu et non pas le coin en haut à droite
        float halfWidth = mapWidth / 2;
        float halfHeight = mapHeight / 2;

        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                amplitude = 1;
                frequency = 1;
                float noiseHeight = 0;

                for (int i = 0; i < octaves; i++)
                {
                    float sampleX = (x - halfWidth + octaveOffsets[i].x) / scale * frequency ; // plus la fréquence sera élevée plus la hauteur de la map va changer rapidement à chaque itération
                    float sampleY = (y - halfHeight + octaveOffsets[i].y) / scale * frequency;

                    //bruit de perlin
                    float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1; // possibilité d'avoir une valeur négative
                    noiseHeight += perlinValue * amplitude; // += pour avoir un effet de nuage, moins pixélisé

                    amplitude *= persistance;
                    frequency *= lacunarity;
                }


                //Pour permettre la normalisation
                if(noiseHeight > maxLocalNoiseHeight)
                {
                    maxLocalNoiseHeight = noiseHeight;
                }else if(noiseHeight < minLocalNoiseHeight)
                {
                    minLocalNoiseHeight = noiseHeight;
                }

                noiseMap[x, y] = noiseHeight;
            }
        }

        //Normalisation
        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                if(normalizeMode == NormalizeMode.Local)
                {
                    noiseMap[x, y] = Mathf.InverseLerp(minLocalNoiseHeight, maxLocalNoiseHeight, noiseMap[x, y]);
                }
                else
                {
                    float normalizedHeight = (noiseMap[x, y] + 1) / (maxPossibleHeight);
                    noiseMap[x, y] = Mathf.Clamp(normalizedHeight, 0, int.MaxValue);
                }
                
            }
        }
        return noiseMap;
    }
}
