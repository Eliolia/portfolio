﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GameSettings : MonoBehaviour
{
    [Header("Speed & Time")]

    [SerializeField] private int maxSeed = 10000;

    public static int MaxSeed => Instance.maxSeed;

    [SerializeField] private float organismMoveSpeed = 2f;

    public static float OrganismMoveSpeed
    {
        get
        {
            return Instance.organismMoveSpeed;
        }
        set
        {
            Instance.organismMoveSpeed = value;
        }
    }

    [SerializeField] private float organismAttackSpeed = 1f;

    public static float OrganismAttackSpeed => Instance.organismAttackSpeed;

    [SerializeField] private float organismTimeToCheckHealth = 1f;

    public static float OrganismTimeToCheckHealth => Instance.organismTimeToCheckHealth;

    [SerializeField] private float gatherSpeed = 1f;

    public static float GatherSpeed => Instance.gatherSpeed;

    [SerializeField] private float weatherChangeLaps = 15f;

    public static float WeatherChangeLaps
    {
        get
        {
            return Instance.weatherChangeLaps;
        }
        set
        {
            Instance.weatherChangeLaps = value;
        }
    }

    [Space(10)]

    [Header("Attack & Gather stats")]

    [SerializeField] private float aggroRadius = 4.5f;

    public static float AggroRadius => Instance.aggroRadius;

    [SerializeField] private float attackRange = 3f;

    public static float AttackRange => Instance.attackRange;

    [SerializeField] private int attackMax = 11;

    public static int AttackMax => Instance.attackMax;

    [SerializeField] private int attackMin = 1;

    public static int AttackMin => Instance.attackMin;

    [SerializeField] private GameObject organismProjectilePrefab;

    public static GameObject OrganismProjectilePrefab => Instance.organismProjectilePrefab;

    [SerializeField] private float gatherRange = 0.2f;

    public static float GatherRange => Instance.gatherRange;

    [Space(10)]

    [Header("Energy")]

    [SerializeField] private int beginEnergy = 200;

    public static int BeginEnergy
    {
        get
        {
            return Instance.beginEnergy;
        }
        set
        {
            Instance.beginEnergy = value;
        }
    }

    [SerializeField] private int minEnergieGain = 5;

    public static int MinEnergieGain => Instance.minEnergieGain;

    [SerializeField] private int maxEnergieGain = 15;

    public static int MaxEnergieGain => Instance.maxEnergieGain;

    [SerializeField] private int organismNormalCost = 10;

    public static int OrganismNormalCost => Instance.organismNormalCost;

    [SerializeField] private int organismAdvancedCost = 15;

    public static int OrganismAdvancedCost => Instance.organismAdvancedCost;

    [SerializeField] private int organismExpertCost = 25;

    public static int OrganismExpertCost => Instance.organismExpertCost;


    [Space(10)]

    [Header("Colony")]

    [SerializeField] private int maxColonieTaille = 10;

    public static int MaxColonieTaille => Instance.maxColonieTaille;

    [Header("HP")]

    [SerializeField] private float bacterieMaxHP = 20f;

    public static float BacterieMaxHP => Instance.bacterieMaxHP;

    [SerializeField] private float playerMaxHP = 1000f;

    public static float PlayerMaxHP => Instance.playerMaxHP;

    [SerializeField] private float organismMaxHP = 100f;

    public static float OrganismMaxHP => Instance.organismMaxHP;

    [SerializeField] private float virusMaxHP = 10f;

    public static float VirusMaxHP => Instance.virusMaxHP;

    [SerializeField] private int organismMinLossHP = 1;

    public static int OrganismMinLossHP => Instance.organismMinLossHP;

    [SerializeField] private int organismMaxLossHP = 5;

    public static int OrganismMaxLossHP => Instance.organismMaxLossHP;

    [SerializeField] private int organismMinGainHP = 1;

    public static int OrganismMinGainHP => Instance.organismMinGainHP;

    [SerializeField] private int organismMaxGainHP = 5;

    public static int OrganismMaxGainHP => Instance.organismMaxGainHP;

    [Space(10)]

    [Header("DNA")]

    [SerializeField] private float organismDNATime = 5f;

    public static float OrganismDNATime => Instance.organismDNATime;

    [SerializeField] private int beginDNA = 0;

    public static int BeginDNA
    {
        get
        {
            return Instance.beginDNA;
        }
        set
        {
            Instance.beginDNA = value;
        }
    }

    [SerializeField] private int minDNAGain = 1;

    public static int MinDNAGain => Instance.minDNAGain;


    [SerializeField] private int maxDNAGain = 5;

    public static int MaxDNAGain => Instance.maxDNAGain;

    [SerializeField] private int normalDNACost = 5;

    public static int NormalDNACost => Instance.normalDNACost;


    [SerializeField] private int advancedDNACost = 25;

    public static int AdvancedDNACost => Instance.advancedDNACost;

    [SerializeField] private int expertDNACost = 70;

    public static int ExpertDNACost => Instance.expertDNACost;

    [Space(10)]
    [Header("Consommation")]

    // Consommation des organismes en fonction niveau
    [SerializeField] private int normalOrganismConso = 1;
    public static int NormalOrganismConso => Instance.normalOrganismConso;

    [SerializeField] private int advancedOrganismConso = 2;
    public static int AdvancedOrganismConso => Instance.advancedOrganismConso;

    [SerializeField] private int expertOrganismConso = 3;
    public static int ExpertOrganismConso => Instance.expertOrganismConso;

    [SerializeField] private float maxDistToPlayer = 241f;
    public static float MaxDistToPlayer => Instance.maxDistToPlayer;

    [Space(10)]
    [Header("Sons et musiques")]

    [SerializeField] private float effectVolumeLevel = 0.5f;

    public static float EffectVolumeLevel
    {
        get
        {
            return Instance.effectVolumeLevel;
        }

        set
        {
            Instance.effectVolumeLevel = value;
        }
    }

    [SerializeField] private float musicVolumeLevel = 0.5f;

    public static float MusicVolumeLevel
    {
        get
        {
            return Instance.musicVolumeLevel;
        }
        set
        {
            Instance.musicVolumeLevel = value;
        }
    }

    public static GameSettings Instance { get; private set; }



    // Application du principe de singleton
    private void Awake()
    {
        if (Instance != null)
            Destroy(gameObject);
        else
            Instance = this;
        DontDestroyOnLoad(this.gameObject);
    }



}
