﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateNutriments : MonoBehaviour
{
    public float t = 0.5f; // Laps de temps entre chaque spawn

    [SerializeField]
    protected GameObject prefab;
    [SerializeField]
    protected int MaxNutriments = 100;
    [SerializeField]
    protected int minX = -100;
    [SerializeField]
    protected int maxX = 100;
    [SerializeField]
    protected int y = 15;
    [SerializeField]
    protected int minZ = -100;
    [SerializeField]
    protected int maxZ = 100;

    static private int _minX;
    static private int _maxX;
    static private int _minZ;
    static private int _maxZ;


    // Start is called before the first frame update
    void Start()
    {
        UpdateSpawn();
        StartCoroutine(nutrimentCreation());
    }

    private void UpdateSpawn()
    {
        _minX = (int)GameObject.FindGameObjectWithTag("Player").gameObject.transform.position.x + minX;
        _maxX = (int)GameObject.FindGameObjectWithTag("Player").gameObject.transform.position.x + maxX;
        _minZ = (int)GameObject.FindGameObjectWithTag("Player").gameObject.transform.position.x + minZ;
        _minZ = (int)GameObject.FindGameObjectWithTag("Player").gameObject.transform.position.x + minZ;
    }

    private void Update()
    {
        UpdateSpawn();
    }

    IEnumerator nutrimentCreation()
    {
        while(true)
        {
            // Crée une coroutine attendant t secondes avant d'exécuter la suite
            yield return new WaitForSeconds(t);
            UpdateSpawn();
            if (GameVariables.CurrentNbNutriments < MaxNutriments)
            {
                Vector3 randomPos = new Vector3(Random.Range(_minX, _maxX), y, Random.Range(_minZ, _maxZ));
                Instantiate(prefab, randomPos, Quaternion.identity);
                GameVariables.CurrentNbNutriments += 1;
            }
        }
    }
}
