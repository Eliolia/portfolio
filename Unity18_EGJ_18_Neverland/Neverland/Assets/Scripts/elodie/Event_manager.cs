﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Event_manager : MonoBehaviour {

    public bool isActive;
    public Text myText;
    [SerializeField]
    List<Event> myEvents;
    GameObject instance1;
    GameObject instance2;
    GameObject button1;
    GameObject button2;
    

    // Jauges
    public int notoriete;
    public int etat;
    public int fiabilite;
    public int lobby;

    Event CurrentEvent;
    int CurrentInfo;
    int CurrentScore;

    // Use this for initialization
    void Start () {
        // this.DebugEventManager();
        CurrentEvent = this.RandomEvent();
        this.SetNotoriete(0);
        this.SetEtat(0);
        this.SetFiabilite(0);
        this.SetLobby(0);
        this.CreatePopUp(CurrentEvent);
    }

    public void DebugEventManager()
    {
        for(int i = 0; i < myEvents.Count; i++)
        {
            Debug.Log(this.myEvents[i].SelectRandMsg());
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    virtual public int GetNotoriete()
    {
        return this.notoriete;
    }

    virtual public void SetNotoriete(int notoriete)
    {
        this.notoriete = notoriete;
    }

    virtual public int GetEtat()
    {
        return this.etat;
    }

    virtual public void SetEtat(int etat)
    {
        this.etat = etat;
    }

    virtual public int GetFiabilite()
    {
        return this.fiabilite;
    }

    virtual public void SetFiabilite(int fiabilite)
    {
        this.fiabilite = fiabilite;
    }

    virtual public int GetLobby()
    {
        return this.lobby;
    }

    virtual public void SetLobby(int lobby)
    {
        this.lobby = lobby;
    }

    public void CreatePopUp(Event myEvent)
    {
        if (!isActive)
        {
            int numEvent;
            do
            {
                numEvent = myEvent.SelectRandMsg();
                CurrentInfo = numEvent;
                if(myEvent.GetAlreadySeen(numEvent) == 0) {
                    myEvent.SetAlreadySeen(numEvent,1);
                    string alert = myEvent.message[numEvent];

                    this.SetIsActive(true);
                    instance1 = Instantiate(Resources.Load("PopUp", typeof(GameObject)), GameObject.Find("Canvas").transform) as GameObject;
                    instance2 = Instantiate(Resources.Load("Text", typeof(GameObject)), GameObject.Find("PopUp(Clone)").transform) as GameObject;
                    button1 = new GameObject();
                    button2 = new GameObject();
                    instance2.GetComponent<Text>().name = "Text";
                    instance2.GetComponent<Text>().text = alert;
                    instance2.GetComponent<Text>().transform.localPosition = new Vector3(0, 50, 0);
                    Debug.Log(myEvent);

                    if (myEvent is Event_lobby)
                    {
                        button1 = Instantiate(Resources.Load("Button", typeof(GameObject)), GameObject.Find("PopUp(Clone)").transform) as GameObject;
                        button2 = Instantiate(Resources.Load("Button", typeof(GameObject)), GameObject.Find("PopUp(Clone)").transform) as GameObject;
                        button1.GetComponent<Button>().name = "One";
                        button2.GetComponent<Button>().name = "Two";
                        button1.transform.GetChild(0).GetComponent<Text>().text = "Oui";
                        button2.transform.GetChild(0).GetComponent<Text>().text = "Non";
                        button1.GetComponent<Button>().transform.localPosition = new Vector3(-80, -70, 0);
                        button2.GetComponent<Button>().transform.localPosition = new Vector3(80, -70, 0);

                        button1.GetComponent<Button>().onClick.AddListener(TaskOnClick1);
                        button2.GetComponent<Button>().onClick.AddListener(TaskOnClick2);
                    }
                    else if (myEvent is Event_info)
                    {
                        button1 = Instantiate(Resources.Load("Button", typeof(GameObject)), GameObject.Find("PopUp(Clone)").transform) as GameObject;
                        button2 = Instantiate(Resources.Load("Button", typeof(GameObject)), GameObject.Find("PopUp(Clone)").transform) as GameObject;
                        button1.GetComponent<Button>().name = "One";
                        button2.GetComponent<Button>().name = "Two";
                        button1.transform.GetChild(0).GetComponent<Text>().text = "Oui";
                        button2.transform.GetChild(0).GetComponent<Text>().text = "Non";
                        button1.GetComponent<Button>().transform.localPosition = new Vector3(-80, -70, 0);
                        button2.GetComponent<Button>().transform.localPosition = new Vector3(80, -70, 0);

                        button1.GetComponent<Button>().onClick.AddListener(TaskOnClick1);
                        button2.GetComponent<Button>().onClick.AddListener(TaskOnClick2);

                    }
                    else if (myEvent is Event_gouv)
                    {
                        button1 = Instantiate(Resources.Load("Button", typeof(GameObject)), GameObject.Find("PopUp(Clone)").transform) as GameObject;
                        button2 = Instantiate(Resources.Load("Button", typeof(GameObject)), GameObject.Find("PopUp(Clone)").transform) as GameObject;
                        button1.GetComponent<Button>().name = "One";
                        button2.GetComponent<Button>().name = "Two";
                        button1.transform.GetChild(0).GetComponent<Text>().text = "Oui";
                        button2.transform.GetChild(0).GetComponent<Text>().text = "Non";
                        button1.GetComponent<Button>().transform.localPosition = new Vector3(-80, -70, 0);
                        button2.GetComponent<Button>().transform.localPosition = new Vector3(80, -70, 0);

                        button1.GetComponent<Button>().onClick.AddListener(TaskOnClick1);
                        button2.GetComponent<Button>().onClick.AddListener(TaskOnClick2);

                    }
                    else if (myEvent is Event_osef)
                    {
                        button1 = Instantiate(Resources.Load("Button", typeof(GameObject)), GameObject.Find("PopUp(Clone)").transform) as GameObject;
                        button1.GetComponent<Button>().name = "One";
                        button1.transform.GetChild(0).GetComponent<Text>().text = "Okay !";
                        button1.GetComponent<Button>().transform.localPosition = new Vector3(0, -70, 0);
                        button1.GetComponent<Button>().onClick.AddListener(TaskOnClick3);

                    }
                }
            } while (myEvent.GetAlreadySeen(numEvent) != 1);
        }
    }

    public void TaskOnClick1()
    {
        Debug.Log("Vous avez cliqué sur un bouton!");
        this.SetNotoriete(CurrentEvent.scoreJauge[0,0]);
        this.SetEtat(CurrentEvent.scoreJauge[0,1]);
        this.SetFiabilite(CurrentEvent.scoreJauge[0,2]);
        this.SetLobby(CurrentEvent.scoreJauge[0,3]);
        
        Destroy(instance1);
    }
    public void TaskOnClick2()
    {
        Debug.Log("Vous avez cliqué sur un bouton!");
        this.SetNotoriete(CurrentEvent.scoreJauge[1, 0]);
        this.SetEtat(CurrentEvent.scoreJauge[1, 1]);
        this.SetFiabilite(CurrentEvent.scoreJauge[1, 2]);
        this.SetLobby(CurrentEvent.scoreJauge[1, 3]);
        Destroy(instance1);
    }

    public void TaskOnClick3()
    {
        Debug.Log("Vous avez cliqué sur un bouton!");
        Destroy(instance1);
    }

    public Event RandomEvent()
    {
        return myEvents[Random.Range(0, myEvents.Count)];
    }

    public void SetIsActive(bool active)
    {
        this.isActive = active;
    }

    public bool GetIsActive()
    {
        return this.isActive;
    }



}
