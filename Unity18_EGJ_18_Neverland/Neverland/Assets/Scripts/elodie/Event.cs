﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Event : MonoBehaviour {

    public int nbEvents; // Nombre d'événement d'un événement donné
    public List<string> message; // Liste des messages possibles
    public int[,] scoreJauge;
    public List<int> alreadySeen;
    public GameObject myPicture; // Image qui va apparaître aléatoirement



    // Use this for initialization
    void Start () {

        this.InitAlreadySeen();
    }
	
	// Update is called once per frame
	void Update () {
		
	}


    // ACCESSEURS

    virtual public int GetNbEvents()
    {
        return this.nbEvents;
    }

    virtual public void SetNbEvents(int nbEvents)
    {
        this.nbEvents = nbEvents;
    }

    virtual public void AddMessage(string message)
    {
        this.message.Add(message);
    }

    virtual public string GetMessage(int index)
    {
        return this.message[index];
    }

    virtual public int GetAlreadySeen(int index)
    {
        return this.alreadySeen[index];
    }

    virtual public void SetAlreadySeen(int index, int value)
    {
        this.alreadySeen[index] = value;
    }

    virtual public void InitAlreadySeen()
    {
        for(int i = 0; i < this.nbEvents; i++)
        {
            this.alreadySeen.Add(0);
        }
    }

    // METHODES

    virtual public void InitMsg() { }

    // Selection un message aléatoire dans l'Event
    virtual public int SelectRandMsg()
    {
        int N = this.nbEvents;
        int myRandomEvent = Random.Range(0, N);
        return myRandomEvent;
    }

    virtual public void InitScore() { }

    // Cet effet va être donné de façon random en fonction si accepté
    virtual public int RandomEffectAccepted()
    {
        return Random.Range(0, 100);
    }

    virtual public int RandomEffectRefused()
    {
        return Random.Range(-100, 0);
    }

}
