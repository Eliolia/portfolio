﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NoguesLib;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UI_Article : MonoBehaviour
{

    private Vector3 m_PosArticleVisual;
    private Vector3 m_PosInScroll;
    private bool m_IsPublished;
    private bool m_IsBeingDragged;
    [SerializeField]
    private float m_Speed;
    public Sprite NormalSprite;
    public Sprite PopupSprite;
    public Sprite PublishedSprite;
    public Timer m_Timer;
    public bool m_MouseOver;
    public AudioClip m_SelectArticleSound;


    // Use this for initialization
    void Start()
    {
        GetComponent<Image>().sprite = NormalSprite;

        m_MouseOver = false;
        m_Timer = new Timer(1, false, false, Time.time);
        m_IsPublished = false;
        m_IsBeingDragged = false;
        m_PosInScroll = new Vector3(230, -200, 0);
    }

    // Update is called once per frame
    void Update()
    {

        m_PosInScroll += new Vector3(0, m_Speed, 0);

        if (!m_IsPublished && !m_IsBeingDragged)
        {
            transform.position = m_PosInScroll;
        }

        if (m_Timer.IsOver(Time.time) && m_MouseOver)
        {
            GetComponent<Image>().sprite = PopupSprite;
        }

        if (m_IsPublished)
        {
            GetComponent<Image>().sprite = PublishedSprite;
        }
    }

    public void BeginDrag()
    {
        if (!m_IsPublished)
        {
            m_IsBeingDragged = true;
            AudioSource.PlayClipAtPoint(m_SelectArticleSound, Camera.main.transform.position);
        }
    }

    public void PointerEnter()
    {
        if (!m_IsPublished)
        {
            m_Timer.Reset(Time.time);
            m_MouseOver = true;
        }
    }

    public void PointerExit()
    {
        if (!m_IsPublished)
        {
            GetComponent<Image>().sprite = NormalSprite;
            m_MouseOver = false;
        }
    }

    public void Dragging()
    {
        if (!m_IsPublished)
        {
            transform.position = Input.mousePosition;
            m_PosArticleVisual = Input.mousePosition;
        }
    }

    public void EndDrag()
    {
        if (!m_IsPublished)
        {
            m_IsBeingDragged = false;
            GameManager.UseArticleHolders(this);
            transform.position = m_PosInScroll;
        }
    }

    public Vector3 PosArticleVisual
    {
        get
        {
            return m_PosArticleVisual;
        }

        set
        {
            m_PosArticleVisual = value;
        }
    }

    public Vector3 PosInScroll
    {
        get
        {
            return m_PosInScroll;
        }

        set
        {
            m_PosInScroll = value;
        }
    }

    public Vector3 PosInScroll1
    {
        get
        {
            return m_PosInScroll;
        }
    }

    public bool IsPublished
    {
        set
        {
            m_IsPublished = value;
            GetComponent<Image>().sprite = PublishedSprite;
        }

        get
        {
            return m_IsPublished;
        }
    }
}
