﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArticleHolder : MonoBehaviour
{

    private bool m_IsBusy;
    private UI_Article m_Article;
    public AudioClip m_SendArticleSound;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (m_Article == null)
        {
            m_IsBusy = false;
        }

        else
        {
            m_IsBusy = true;
            m_Article.transform.position = transform.position;
        }

    }

    public void SetArticle(UI_Article _Article)
    {
        m_Article = _Article;
        _Article.IsPublished = true;
        _Article.GetComponent<Article>().isPublished = true;
        AudioSource.PlayClipAtPoint(m_SendArticleSound, Camera.main.transform.position);

    }

    public bool IsBusy
    {
        get
        {
            return m_IsBusy;
        }
    }
}
