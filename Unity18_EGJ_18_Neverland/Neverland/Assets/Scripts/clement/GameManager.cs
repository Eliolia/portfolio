﻿using System.Collections;
using NoguesLib;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    [SerializeField]
    private UI_Article m_ArticleToSpawn;  // Temporary
    [SerializeField]
    private Vector3 m_WhereToSpawnArticles;
    [SerializeField]
    private float m_WhatHeightAreTheyDestroyed;
    private List<UI_Article> m_ScrollingArticles;
    private Timer m_SpawnTimer;
    [SerializeField]
    private float m_TimeToSpawn;
    [SerializeField]
    private List<ArticleHolder> m_ArticleHoldersEditor;
    static private List<ArticleHolder> m_ArticleHolders;
    public Transform m_Parent;
    public AudioClip m_ArticleDeath;
    public List<UI_Article> m_ListOfPossibleArticles;


    // Use this for initialization
    void Start()
    {
        m_ScrollingArticles = new List<UI_Article>();
        m_SpawnTimer = new Timer(m_TimeToSpawn, true, false, Time.time);
        m_ArticleHolders = m_ArticleHoldersEditor;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_SpawnTimer.IsOver(Time.time))
        {
            SpawnArticles();
        }

        DestroyOutOfFrameArticles();
    }

    void SpawnArticles()
    {
        int possibleRand =  m_ListOfPossibleArticles.Count;
        int truerandwtf = Random.Range(0, possibleRand);
        Debug.Log(truerandwtf);
        UI_Article newArticle = Instantiate(m_ListOfPossibleArticles[truerandwtf], m_WhereToSpawnArticles, Quaternion.identity, m_Parent) as UI_Article;

        m_ScrollingArticles.Add(newArticle);
    }

    void DestroyOutOfFrameArticles()
    {
        for (int i = 0; i < m_ScrollingArticles.Count; i++)
        {
            if (m_ScrollingArticles[i].PosInScroll.y > m_WhatHeightAreTheyDestroyed && !m_ScrollingArticles[i].IsPublished)
            {
                UI_Article toDelete = m_ScrollingArticles[i];
                m_ScrollingArticles.Remove(m_ScrollingArticles[i]);
                Destroy(toDelete.gameObject);
                AudioSource.PlayClipAtPoint(m_ArticleDeath, Camera.main.transform.position);
            }

        }
    }

    static public void UseArticleHolders(UI_Article _Article)
    {
        foreach (ArticleHolder holder in m_ArticleHolders)
        {
            if (holder.GetComponent<BoxCollider2D>().bounds.Contains(Input.mousePosition))
            {
                if (!holder.IsBusy)
                {
                    holder.SetArticle(_Article);
                }
            }
        }
    }

}
