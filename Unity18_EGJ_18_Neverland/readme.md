# Neverland

## Résumé

Ce jeu a été réalisé en 30h lors de l'EduGameJam 2018, organisé par le Ministère de l'Education Nationale et le réseau Canopé, sur le thème de l'esprit critique.

Nous avions choisis les thèmes d'apprentissage suivant : 

* Sélectionner des informations ;
* Evaluer la fiabilité de l'information ;
* Hiérarchiser la diffusion des informations ;

En découle un jeu de gestion dans lequel le joueur incarne un rédacteur en chef dans une société dystopique totalitaire. Il devra opérer des choix quant aux articles à mettre dans sa ligne éditoriale, chacun ayant un impact variable sur la réputation du journal (état, confiance du peuple, lobby divers, révolutionnaires).

Le point principal du jeu va à l'établissement de critères d'évaluations de l'information selon deux catégories : 

* La source : identité de l'émetteur, son parcours et son statut ;
* Le contenu : objectivité et niveau de l'argumentation, qualité de la ponctuation expressive et utilisation de modalisateurs.

## Comment jouer ?

Vous devez cliquer sur les articles pour en voir la provenance, déterminer si l'article a sa place dans votre journal et s'il doit en faire la une.

Pour ajouter un article à votre journal, il suffit de faire un glisser-déposer de l'article en question.

## Implémentation

Le projet a été développé sous Unity 2018.x.

## Les améliorations envisageables

Plusieurs améliorations du projet ont été envisagées :

* Système de popularité du journal ;
* Les jauges statistiques concernant la fiabilité, l'"Etat", les lobbys et la notoriété auprès du peuple ;
* Toute la partie feedbacks et évaluation des étudiants.

## Bug report

* Le retour à l'écran d'accueil depuis les crédits n'est pas mis en place ;
* La proportionnalité en fonction de la taille de la fenêtre désirée.

## Exécuter

Ouvrir l'exécutable "buildalmostfinal.exe"

## Démonstration vidéo

Dans le dossier "Démonstration" se trouve la vidéo en question

## En savoir plus

Pour en savoir plus, ouvrez "EGJ2018_neverland.pdf".