# Manuel d'utilisation

_Les cartes éléctroniques (Arduino UNO R3) et les nano-ordinateurs (Raspberry Pi 1B+ et 2B) utilisées dans ce projet peuvent être remplacées par des équivalents compatibles_

## Installation et mise en marche de la solution

### Mise en place du robot humanoïde Nao

1. Téléchargez et installez [la version du logiciel Choregraphe pour Nao V4-V5](https://developer.softbankrobotics.com/us-en/downloads/nao-v5-v4) correspondante à votre système d'exploitation (_Windows, Linux ou Mac_) ; 
2. Lancez _Choregraphe_ et ouvrez le projet `./Code/Nao/ServeurNao/ServeurNao.pml` ;
<img src="./Ressources/ServeurNao.png" title="Flow Diagram du projet ServeurNao" style="margin-left:auto ; margin-right: auto; display: block">

3. Branchez **le Robot Nao** à votre routeur ou à votre ordinateur. Sa première connexion à votre réseau nécessite qu'elle soit filaire (_Ethernet RJ45_). Par la suite, vous pourrez connecter votre robot sans-fil (_Wi-Fi_) ;
4. Cliquez sur l'onglet _Connetion_ puis _Connect to..._ ou bien le raccourci _Ctrl+Shift+C_ pour établir la connexion avec **le Robot Nao** physique ;
5. Téléversez le projet dans Nao en cliquant sur le bouton _Play_ ou avec le raccourci _F5_.

### Mise en place du Serveur Central

1. Assurez vous de disposer d'une [Raspberry Pi](https://www.raspberrypi.org/products/) alimenté par un chargeur micro-USB 5V d'au moins 2A et qu'elle soit connectée à votre réseau local par Wi-Fi ou câble Ethernet. Elle doit également tourner sous un [système d'exploitation compatible architecture ARM](https://www.raspberrypi.org/downloads/) ;
2. Assurez vous également que les fichiers et dossiers suivants soient présents dans un même dossier sur cette Raspberry Pi :
    * Le dossier `./Code/Serveurs/BD` ;
    * Le dossier `./Code/Serveurs/historique_ordres` ;
    * Le fichier `./Code/Serveurs/InterpreteurBD.py` ;
    * Les fichiers `./Code/Serveurs/Serveur.py` et `./Code/Serveurs/Serveur_Central.py` ;
    * Le fichier `./Code/Serveurs/Domotarduino.py`.
3. Exécutez dans une console le programme du **Serveur Central** avec la commande suivante : `python ./Serveur_Central.py <PORT>` où _<PORT\>_ est un argument du programme pour déterminer le port de communication principal alloué au **Serveur Central** ;

> Le programme du **Serveur Central** vous informe à son démarrage de son adresse IPv4 ainsi que son numéro de port. Reportez les dans le module Python **ClientNao** du projet _Choregraphe_ **ServeurNao** aux lignes 122 et 123.

### Mise en place de la carte Arduino UNO R3

1. Téléchargez et installez [la dernière version du logiciel Arduino IDE](https://www.arduino.cc/en/main/software) correspondante à votre système d'exploitation (_Windows, Linux ou Mac_) ;
2. Munissez vous de 3 LEDs de couleurs différentes, 3 résistances de 100kΩ et des câbles courts et longs, puis réalisez le montage suivant :

| LED    | Pièce   | N° DIGITAL PIN |
| :-----:| :------:|:--------------:|
| Rouge  | Salon   | 2              |
| Jaune  | Cuisine | 3              |
| Bleue  | Chambre | 4              |

<img src="./Ressources/photo_montage_arduino.jpg" title="Photo du montage simulateur lumières connectés sur la carte Arduino" style="margin-left:auto ; margin-right: auto; display: block">
3. Branchez votre montage directement au **Serveur Central** à l'aide d'[un câble USB 2.0 (ou supérieur) Type AB (Mâle/Mâle)](http://site.allaboutadapters.com/webgraph/USBCable-AB-6F-Application.gif) ;
4. Ouvrez le projet `./Code/Arduino/Domotarduino_2` dans Arduino IDE, vérifiez que votre carte Arduino est bien reconnue par le **Serveur Central**
5. Compilez puis téléverser le programme dans la carte **Arduino Uno R3**

>À l'aide de _Arduino IDE_, vérifier dans l'onglet _Outils_ puis _Port Série_ que votre carte **Arduino Uno R3** est connecté au port _/dev/ttyACM0_. Si tel n'est pas le cas, veuillez ouvrir le fichier `./Code/Serveurs/BD/ip_port_obj_co.txt` et remplacer _arduino, NULL, /dev/ttyACM0_ par _arduino, NULL, /dev/<PORT\>_ où <PORT\> correspond au numéro de port série de votre carte constaté avec _Arduino IDE_.

### Mise en place du Serveur IoT

1. Assurez vous de disposer d'une [Raspberry Pi](https://www.raspberrypi.org/products/) alimenté par un chargeur micro-USB 5V d'au moins 2A et qu'elle soit connectée à votre réseau local par Wi-Fi ou câble Ethernet. Elle doit également tourner sous un [système d'exploitation compatible architecture ARM](https://www.raspberrypi.org/downloads/) ;
2. Branchez la à un clavier, une souris et un écran via le port micro-hdmi afin de suivre les simulations qui y seront executées;
3. Assurez vous également que les fichiers et dossiers suivants soient présents dans un même dossier sur chaque Raspberry Pi :
    * Les fichiers `./Code/Serveurs/Serveur.py`, `./Code/Serveurs/Serveur_Central.py` et `./Code/Serveurs/ServeurRPI.py` ;
    * Le fichier `./Code/Serveurs/InterpreteurRPI.py` ;
    * Les images PNG "fenetre_ferme", "fenetre_ouvert", "porte_ferme", "porte_ouvert", "volet_ferme" et "volet_ouvert" du dossier `./Code/Serveur`
4. Exécutez dans une console le programme du **Serveur IOT** avec la commande suivante : `python ./ServeurRPI.py <PORT>` où _<PORT>_ est un argument du programme pour déterminer le port de communication principal alloué au **Serveur IOT** ;

> Le programme du **Serveur IOT** vous informe à son démarrage de son adresse IPv4 ainsi que son numéro de port. Veuillez ouvrir le fichier `./Code/Serveurs/BD/ip_port_obj_co.txt` et remplacer _rpi, dex11, NULL_ par _rpi, <IP\>, NULL_ où <IP\> correspond à l'adresse IP ou le nom d'ĥote de votre carte Raspberry Pi

## Utilisation

Une fois l'ensemble de l'architecture installée et opérationnelle, l'interaction se fait au travers de **l'Interface Nao**. Des conversations vont permettre de créer une réaction avec le système ou avec le robot.

Il est également possible de simuler l'envoi d'ordre vers le **Serveur Central** en simulant **l'Interface** grâce au script Python `./Code/Serveurs/Iclient.py` prenant en argument l'<IP\> et le <PORT\> du **Serveur Central**.

Les ordres sont alors de la forme `Qui? Quoi?` suivit d'une commande spécifique (ici, A(_ctionner_), E(_teindre_) ou I(_nformation_)):
* -> arduino
    * salon
    * cuisine
    * chambre
* -> rpi
    * fenetre
    * porte
    * volet     

_Exemple : "arduino chambre A" aura pour effet d'allumer la LED bleue sur la carte Arduino, simulant la chambre de la personne âgée._

>L'ensemble des dialogues disponibles sont dans les Dialog Topics accessibles depuis l'interface Choregraphe dans les contenus du projet.

_Dernière date de modification : 2017/06/09_