#!/usr/bin/env python2
#-*- coding: utf-8 -*-

# Module d'écriture du port série
# Port série ttyACM0
# Envoie d'un nombre vers la Arduino

from serial import *

n = raw_input("Entrez un nombre entier : ")

with Serial(port="/dev/ttyACM0", baudrate=9600, timeout=1, writeTimeout=1) as port_serie:
	if port_serie.isOpen():
		port_serie.write([n])
		while True:
			ligne = port_serie.readline()
			print ligne
