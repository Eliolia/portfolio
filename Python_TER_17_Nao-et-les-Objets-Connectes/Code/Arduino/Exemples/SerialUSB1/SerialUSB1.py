#!/usr/bin/env python
#-*- coding: utf-8 -*-

# Module de lecture du port série
# Port série ttyACM0

import serial
mySerial = serial.Serial('/dev/ttyACM0', 9600)
while 1:
	if(mySerial != 0):
		print(mySerial.readline())