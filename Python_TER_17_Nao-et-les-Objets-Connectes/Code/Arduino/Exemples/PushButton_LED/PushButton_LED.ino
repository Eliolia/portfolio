// Allume la LED quand le bouton est pressé

const int LED = 13;   // num de pin pour la LED
const int BUTTON = 7; // Num de pin où le bouton est connecté
int val = 0;          // Stocke l'état de l'input pin du BUTTON

void setup() {
  pinMode(LED, OUTPUT);   // LED est un output
  pinMode(BUTTON, INPUT); // BUTTON est un input
}

void loop() {
  val = digitalRead(BUTTON); // Lit la valeur de l'input et save it
  
  // Vérifie si l'input est sur HIGH (bouton pressé)
  if (val == HIGH){
    digitalWrite(LED, HIGH);  // LED allumée
  }else{
    digitalWrite(LED, LOW);   // LED éteinte
  }
}
