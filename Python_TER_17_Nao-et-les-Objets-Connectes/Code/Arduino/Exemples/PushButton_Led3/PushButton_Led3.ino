// Allume la LED quand le bouton est pressé, éteint quand
// de nouveau pressé

const int LED = 13;   // num de pin pour la LED
const int BUTTON = 7; // Num de pin où le bouton est connecté
int val = 0;          // Stocke l'état de l'input pin du BUTTON
int old_val = 0;       // Conserve l'ancien état de la Led
int state = 0;        // 0 = LED off = LED on

void setup() {
  pinMode(LED, OUTPUT);   // LED est un output
  pinMode(BUTTON, INPUT); // BUTTON est un input
}

void loop() {
  val = digitalRead(BUTTON); // Lit la valeur de l'input et save it
  
  // Vérifie si l'input est sur HIGH (bouton pressé)
  // et change state
  if((val == HIGH) && (old_val == LOW)){
    state = 1 - state;
    delay(50);
  }

  if ((val == LOW) && (old_val == HIGH)) {
    delay(50);
  }

  old_val = val; // On conserve maintenant val
  
  if (state == 1){
    digitalWrite(LED, HIGH);  // LED allumée
  }else{
    digitalWrite(LED, LOW);   // LED éteinte
  }
}
