// Allume la LED, si bouton pressé alors On (state =1) sinon
// Off (state = 0)

const int LED = 13;   // num de pin pour la LED
const int BUTTON = 7; // Num de pin où le bouton est connecté
int val = 0;          // Stocke l'état de l'input pin du BUTTON
int state = 0;        // 0 = LED off = LED on

void setup() {
  pinMode(LED, OUTPUT);   // LED est un output
  pinMode(BUTTON, INPUT); // BUTTON est un input
}

void loop() {
  val = digitalRead(BUTTON); // Lit la valeur de l'input et save it
  
  // Vérifie si l'input est sur HIGH (bouton pressé)
  // et change state
  if(val == HIGH){
    state = 1 - state;
  }
  
  if (state == 1){
    digitalWrite(LED, HIGH);  // LED allumée
  }else{
    digitalWrite(LED, LOW);   // LED éteinte
  }
}
