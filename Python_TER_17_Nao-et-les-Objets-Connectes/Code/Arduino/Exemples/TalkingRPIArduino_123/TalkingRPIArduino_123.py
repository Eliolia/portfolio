#!/usr/bin/env python2
#-*- coding: utf-8 -*-

# Module de lecture du port série
# Port série ttyACM0

import serial 	# Communication série
import time		# Gestion du temps

ser = serial.Serial('/dev/ttyACM0', 9600)
cpt = 0

while 1:
	if cpt < 6:
		cpt = cpt + 1
	else:
		cpt = 0
	ser.write(str(cpt))
	time.sleep(2)