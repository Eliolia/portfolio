<?xml version="1.0" encoding="UTF-8" ?>
<Package name="alarm" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="." xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="prog_reveil" src="prog_reveil/prog_reveil.dlg" />
    </Dialogs>
    <Resources />
    <Topics>
        <Topic name="prog_reveil_frf" src="prog_reveil/prog_reveil_frf.top" topicName="prog_reveil" language="fr_FR" />
    </Topics>
    <IgnoredPaths />
</Package>
