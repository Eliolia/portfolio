#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import datetime
import time
import threading

class Alarme(threading.Thread):
	def __init__(self, annee, mois, jour, heure, minute, event):
		super(Alarme, self).__init__()
		self.time = [int(annee), int(mois), int(jour), int(heure), int(minute)]
		self.event = str(event)
		self.active = True

	def run(self):
		try:
			while self.active:
				same = True
				now = datetime.datetime.now()
				current = [now.year, now.month, now.day, now.hour, now.minute]

				print("Heure courante : ")
				print("Date  : " + str(current[2])+"/"+str(current[1])+ "/" + str(current[0]))
				print("Heure : " +str(current[3])+ " heure et " + str(current[4]) + " minutes")

				for i in range(5):
					if current[i] != self.time[i]:
						same = False

				if same:
					# TODO: Executer le réveil sur Nao
					print("Wake up ! Vous avez prévu l'évenement suivant : ")
					print(self.event)
					music = os.popen('vlc ./Reveil_en_douceur.mp3', 'w')
					time.sleep(60)
					music.close()
					self.active = False
				else:
					time.sleep(10)
			return
		except:
			return

	def alarm_kill(self):
		self.active = False




