<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Interfacage" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1/ExampleDialog" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="ExampleDialog" src="behavior_1/ExampleDialog/ExampleDialog.dlg" />
        <Dialog name="ExampleDialog" src="behavior_1/ExampleDialog/ExampleDialog/ExampleDialog.dlg" />
    </Dialogs>
    <Resources>
        <File name="client_0_1" src="client_0_1.py" />
    </Resources>
    <Topics>
        <Topic name="ExampleDialog_enu" src="behavior_1/ExampleDialog/ExampleDialog_enu.top" topicName="ExampleDialog" language="en_US" />
        <Topic name="ExampleDialog_enu" src="behavior_1/ExampleDialog/ExampleDialog/ExampleDialog_enu.top" topicName="ExampleDialog" language="en_US" />
    </Topics>
    <IgnoredPaths>
        <Path src="behavior_1/ExampleDialog_enu.top" />
        <Path src="behavior_1/behavior.xar" />
        <Path src="behavior_1/ExampleDialog.dlg" />
        <Path src="behavior_1/ExampleDialog/ExampleDialog.dlg" />
        <Path src="behavior_1/ExampleDialog/ExampleDialog_enu.top" />
        <Path src="behavior_1/ExampleDialog/ExampleDialog/ExampleDialog.dlg" />
        <Path src="behavior_1/ExampleDialog/ExampleDialog/ExampleDialog_enu.top" />
    </IgnoredPaths>
</Package>
