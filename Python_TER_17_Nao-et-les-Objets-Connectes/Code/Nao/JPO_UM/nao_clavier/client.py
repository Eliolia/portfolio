#!/usr/bin/python
#coding=utf-8
import tty
import sys
import termios
import getip
import client_0_1

def main():
    # connection
    host="nao.local"
    port=10000
    print("Connecting to : " + host + ", " + str(port))
    socket = client_0_1.connection(host, port)
    #socket = client_0_1.connection("192.168.0.20", "10000")
    
    rep = client_0_1.message(socket, "handshake".encode())
    print(rep)

    # definition relation touche / ordres
    order = {'z':'forward', 's':'back', 'd':'right', 'q':'left'}
    
    # detection appui touches
    orig_settings = termios.tcgetattr(sys.stdin)
    tty.setraw(sys.stdin)
    key = 0
    while key != chr(27): # ESC
        key=sys.stdin.read(1)[0]
        if key in order.keys():
            rep = client_0_1.message(socket, str(order[key]).encode())
            # print("\rYou pressed" + key)
    client_0_1.deconnection(socket)
    # # termios.tcsetattr(sys.stdin, termios.TCSADRAIN, orig_settings)

if __name__ == '__main__':
    main()
