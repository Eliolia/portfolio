#!/usr/bin/python
# coding=utf-8
import socket
import threading
import SocketServer
import logging

class NaoServer(object):
    def __init__(self, HOST,PORT):
        from serveurthread_0_1 import *
        self.server = ThreadedTCPServer((HOST, PORT), ThreadedTCPRequestHandlerNao)
    
    def main(self):
        from time import sleep 
        
        ip, port = self.server.server_address

        # Start a thread with the server -- that thread will then start one
        # more thread for each request
        server_thread = threading.Thread(target=self.server.serve_forever)
        # Exit the server thread when the main thread terminates
        server_thread.daemon = True
        server_thread.start()
        print "Server loop running in thread:", server_thread.name
        #server_thread.join()

        while 1:
            sleep(0.2)
        # print "dans thread",server_thread.name
        #sleep(1)
        #       line = raw_input('Saisir stop pour arreter serveur \n')
        self.close()
        
    def close(self):
        self.server.shutdown()
        self.server.server_close()
    
class ThreadedTCPRequestHandlerNao(SocketServer.BaseRequestHandler):
    msg = ""
    nb = 0
    def handle(self):
        data = ""
        while data!="EOT" and data!="fin":            
            data = self.request.recv(1024)
            ThreadedTCPRequestHandlerNao.msg = data
            ThreadedTCPRequestHandlerNao.nb += 1
            # print("received: " + data)
            cur_thread = threading.current_thread()
            response = "{} : {} bien recu ".format(cur_thread.name, data)
            self.request.sendall(response)
        #data = self.request.recv(1024)
        self.finish()
        
    def finish(self):
        return SocketServer.BaseRequestHandler.finish(self)
