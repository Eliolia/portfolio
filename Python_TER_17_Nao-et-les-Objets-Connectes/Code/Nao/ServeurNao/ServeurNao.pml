<?xml version="1.0" encoding="UTF-8" ?>
<Package name="ServeurNao" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="ServeurNao" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="Nobody" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="Scenario&#x0D;" src="Scenario/Scenario.dlg" />
        <Dialog name="Get_Date-Heure" src="Get_Date-Heure/Get_Date-Heure.dlg" />
        <Dialog name="Jpo" src="Jpo/Jpo.dlg" />
        <Dialog name="Dances" src="Dances/Dances.dlg" />
    </Dialogs>
    <Resources>
        <File name="randmusic" src="Musiques/randmusic.mp3" />
        <File name="got" src="Musiques/got.mp3" />
    </Resources>
    <Topics>
        <Topic name="Scenario_frf" src="Scenario/Scenario_frf.top" topicName="Scenario" language="fr_FR" />
        <Topic name="Get_Date-Heure_frf" src="Get_Date-Heure/Get_Date-Heure_frf.top" topicName="Get_Date-Heure" language="fr_FR" />
        <Topic name="Jpo_frf" src="Jpo/Jpo_frf.top" topicName="Jpo" language="fr_FR" />
        <Topic name="Dances_frf" src="Dances/Dances_frf.top" topicName="Dances" language="fr_FR" />
    </Topics>
    <IgnoredPaths />
</Package>
