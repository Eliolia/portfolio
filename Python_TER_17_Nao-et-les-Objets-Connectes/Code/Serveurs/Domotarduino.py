#!/usr/bin/env python2
#-*- coding: utf-8 -*-

# Module de lecture/écriture du port série
# Port série ttyACM0

# NOTE : Lors de la réception, il y a un deadlock. Le serveur bloque sur la réponse de Arduino

import serial
import time
import sys

lenght = len(sys.argv)
msg =""

if lenght != 4:
	print "Pas assez d'arguments"
	sys.exit()
else:
	port = sys.argv[2]
	code = sys.argv[3]

	if port != "NULL":
		if code == "NULL":
			sys.stdout.write("Erreur")
		elif code == '0':
			sys.stdout.write("off")
		elif code == '1':
			sys.stdout.write("on")
		else:
			try:
				# On envoie un code à Arduino
				myPort = serial.Serial(port, 9600)
				myPort.write(code)
				time.sleep(0.1)

				# On réceptionne sa réponse

				msg = myPort.readline()
				"""
				while True:
					msg = myPort.readline()
					break
				"""
				sys.stdout.write(msg)

			except Exception as e:
				raise e
			finally:
				myPort.close()
	else:
		sys.stdout.write("Erreur: port inconnu.")