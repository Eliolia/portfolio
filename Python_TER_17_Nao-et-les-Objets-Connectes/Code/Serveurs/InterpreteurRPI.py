#!/usr/bin/python
# coding=utf-8

""" 
NOTE : 	Privilégier le visionneur d'image ristretto
 		eog (Eye of Gnome) génère une ligne vide puis un 
		warning car il ne trouve pas sa miniature
"""


import sys, os, time
import subprocess

longueur = len(sys.argv)
if (longueur != 2):
	sys.stdout.write("Error: pas le bon nombre d'arguments")
	sys.exit()
else :

	if os.path.isfile(sys.argv[1]):
		p = subprocess.Popen(["ristretto", sys.argv[1]],stdout=subprocess.PIPE)
		time.sleep(4)
		p.kill()
		#print sys.argv[1]
		ordre = sys.argv[1]
		mots = []
		for mot in ordre.split('.'):
			mots.append(mot)
		#print mots
		msg = mots[0].split('_')
		#print msg
		repNao = msg[0] + " " + msg[1] + " ok"
		sys.stdout.write(repNao)
	else :
		sys.stdout.write("Error : fichier "+sys.argv[1]+" non existant")
		sys.exit()


