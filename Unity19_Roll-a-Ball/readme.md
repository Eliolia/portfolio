# Roll-a-ball

## Résumé

Il s'agit d'une application issue d'un tutoriel de prise en main d'Unity 2019.

## Comment jouer ?

Bougez la balle avec les flèches directionnelles pour ramasser tous les collectibles.

## Implémentation

Le projet a été développé sous Unity 2019.x en C#.

## Les améliorations envisageables

Plusieurs améliorations du projet ont été envisagées :

* HUD ;
* levels ;
* intégration de sons ;
* bouger le niveau et non pas la balle pour faire bouger la balle sur le plateau.

## Exécuter

Ouvrir l'exécutable "Roll a Ball.exe"

## Démonstration vidéo

Dans le dossier "Démonstration" se trouve la vidéo en question