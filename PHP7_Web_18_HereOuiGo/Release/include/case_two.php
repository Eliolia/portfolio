<section id="case_two">
	<?php  
		require('statistiques.php');
		if(!empty($_SESSION) && isset($_SESSION['auth']) && isset($_SESSION['admin'])){
			echo '<article id="case_two_admin">';
			echo "<h2>Statistiques générales du site</h2>";
			?>
		<table>
			<tr>
				<th>Nombre de membres</th>
				<th>Trajets proposés</th>
				<th>Avis déposés</th>
			</tr>
			<tr>
				<td><?php echo compter_membre(); ?></td>
				<td><?php echo compter_trajet(); ?></td>
				<td><?php echo compter_avis(); ?></td>
			</tr>

		</table>



	<?php

			echo '</article>';
		}elseif(!empty($_SESSION) && isset($_SESSION['auth'])){ 
			echo '<article id="case_two_admin">';
			echo "<h2>Votre activité</h2>";
			?>
		<table>
			<tr>
				<th>Mes trajets</th>
				<th>Avis à déposer</th>
				<th>Offres publiées</th>

			</tr>
			<tr>
				<td><a href="mes_trajets.php" class='big_button' title='Regardez quels sont vos trajets en cours !'><?php echo compter_mes_trajets(); ?></a></td>
				<td><a href="leave_avis.php" class='big_button' title='Auriez-vous des avis à déposer ?'><?php echo compter_avis_deposer(); ?></a></td>
				<td><a href="mes_trajets.php" class='big_button' title='Auriez-vous des avis à déposer ?'><?php echo compter_offres_membre() ?></a></td>
			</tr>

		</table>

		
		<?php echo "</article>";
		}else{
			echo '<h2>Envie de rejoindre la communauté ?</h2>
				<article class="case_two_article">
					<h3>Consulter les profils des membres !</h3>
					<p>Et créer le votre!</p>
				</article>
				<article class="case_two_article">
					<h3>Réservez votre place ou proposez un trajet !</h3>
					<p>Les inscrits peuvent réserver leur place en ligne et devenir conducteurs.</p>

				</article>
				<article class="case_two_article">
					<h3>Déposez des avis !</h3>
					<p>Conducteur comme passager, donnez votre ressentit sur votre voyage.</p>

				</article>
				<a href="./inscription.php" id="but_sign_in">Inscrivez-vous !</a>';
			}
		?>
	

</section>