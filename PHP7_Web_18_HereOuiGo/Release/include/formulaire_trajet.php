<?php
	function date_min(){
		$aujourdhui=date("Y-m-d");
		$demain=mktime(0,0,0,substr($aujourdhui,5,2),substr($aujourdhui,8,2)+1,substr($aujourdhui,0,4));

		return date("Y-m-d",$demain); 
	}
	echo "<h2>Proposer votre trajet</h2>";
	if(isset($_GET['erreur'])){
		if($_GET['erreur'] == true){
			print "<div class='error_box'><p>Une erreur s'est produite. Veuillez renouveler votre trajet.</p></div>";
			exit();
		}
	}

	echo "<form id='id_trajet' name='trajet' method='post' action="; echo $_SERVER['PHP_SELF'] . ">"; 
				
	echo "<fieldset>
		<table>
		<tr><td><label for='ville_dep'> Ville de départ : </label></td>
		<td><input type='text' name='ville_dep' pattern='^[a-zA-Z]*(-|\s)?([a-zA-Z]*)?' autofocus required/></td></tr>
		<br>
		<tr><td><label for='adr_rdv'> Adresse de rendez-vous : </label></td>
		<td><input type='text' name='adr_rdv' maxlength='64' autofocus required/></td></tr>
		</br>
		<tr><td><label for='ville_arr'> Ville d'arrivée : </label></td>
		<td><input type='text' name='ville_arr' maxlength='40' pattern='^[a-zA-Z]*(-|\s)?([a-zA-Z]*)?'  required/></td></tr>
		</br>
		<tr><td><label for='adr_depot'> Adresse de dépôt : </label></td>
		<td><input type='text' name='adr_depot' maxlength='64' required/></td></tr>
		</br>
		<tr><td><label for='date'> Date : </label></td>
		<td><input type='date' name='date' min=". date_min() . " required/></td></tr>
		</table>
		</fieldset>
		<br>
		<fieldset>
		<table>";

		$acces =new PDO('mysql:host=localhost;dbname=bdd_hereouigo;charset=utf8', $username, $password);

		?>

		<tr><td><label for="vehicule">Votre vehicule : </label></td>
			<td><select name="vehicule">
				<?php
				// Recherche des vehicules enregistré de USER
				$req = $acces->prepare("SELECT * 
										FROM membre_vehicule, vehicule 
										WHERE mail = :mail
										AND membre_vehicule.immatriculation = vehicule.immatriculation;");
				if($req->execute(array("mail"=>$_SESSION['mail']))){
					$data = $req->fetchAll();
					foreach($data as $vehicule){
						echo "<option value =" . $vehicule['immatriculation'] . ">";
						echo $vehicule['marque'] . ' ' . $vehicule['modele'] . ' ' ."({$vehicule['immatriculation']})";
						echo "</option>";
					}
				}else{
					print "";
					header("Location: add_trajet.php?erreur=true");
					exit();
				}

				?>
			</select></td></tr>
			<br>
			<tr><td><label for="prix">Votre prix</label></td>
			<td><input type="number" name="prix" min='1' max='200' />€</td></tr>
			<br>
			<tr><td><label for="nb_places">Nombre de places disponibles</label></td>
			<td><input type="number" name="nb_places" min="0" /></td></tr>
			

		<?php 
		echo "</table></fieldset>
		<input type='submit' name='submit' value='Ajouter'/>
		

	</form>
		";



?>