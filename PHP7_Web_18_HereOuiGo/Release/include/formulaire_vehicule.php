<?php

	echo "<h2>Ajouter votre vehicule</h2>";

	echo "<form class='generic_form' id='id_vehicule' name='vehicule' method='post' action="; echo $_SERVER['PHP_SELF'] . ">"; 
				 
				echo "
				<table>
					<tr><td><label for='immatriculation'> Immatriculation : </label></td>
					<td><input type='text' name='immatriculation' pattern='^[A-Z]{2}-[0-9]{3}-[A-Z]{2}' placeholder='XX-000-XX' autofocus required/></td></tr>
					</br>
					<tr><td><label for='marque'> Marque : </label></td>
					<td><input type='text' name='marque' required/></td></tr>
					</br>
					<tr><td><label for='modele'> Modele : </label></td>
					<td><input type='text' name='modele' required/></td></tr>
					</br>
					<tr><td><label for='type_carburant'> Type de carburant : </label></td>
					<td><select id='type_carburant' name='type_carburant' required>
					  <option value='essence' selected>Essence</option> 
					  <option value='diesel' >Diesel</option>
					  <option value='hybride'>Hybride</option>
					  <option value='gpl'>GPL</option>
					  <option value='electrique'>Electrique</option>
					</select></td></tr>
					</br>
					<tr><td><label for='nb_place'> Nombre de places total : </label></td>
					<td><input type='number' max-length='2' min ='1' max='99' name='nb_place' pattern='^[0-9]{2}' required/></td>
					</br></tr>
				</table>
				<input type='submit' name='submit' value='Ajouter'/>
				<input type='reset' value='Effacer'/>
				</form>"
?>