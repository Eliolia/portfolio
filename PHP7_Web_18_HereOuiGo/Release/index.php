<?php
	error_reporting(E_ALL);
	empty($_SESSION)? session_start() : print "";
?>
<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title> HereOuiGo - voyagez tranquille </title>
		<link rel="icon" href="./WebContents/icon.png" sizes="64x64" />
		<link rel="stylesheet" href="styles.css"/>
		
		

		<!--[if lt IE 9]>
			<script src="./Scripts/html5shiv.js"></script>
		<![endif] -->
	</head>
	<body>
		<div id="global"> 
			<?php
				require("./include/header.php");
				require("./include/config.php");
				require("./include/footer.php");
			?>
		</div>
	</body>
</html>