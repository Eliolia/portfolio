<?php
	error_reporting(E_ALL);
	empty($_SESSION)? session_start() : print "";
	include("./BD/info_bd.php");
?>

<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title> HereOuiGo - voyagez tranquille </title>
		<link rel="icon" href="./WebContents/icon.png" sizes="64x64" />
		<link rel="stylesheet" href="./styles.css"/>
		<script src="./Scripts/monscript.js"></script>

		<!--[if lt IE 9]>
			<script src="./Scripts/html5shiv.js"></script>
		<![endif] -->
	</head>
	<body>
		<?php
			include("./include/header.php");
		?>
		<div id="main">
			<?php
				if(isset($_POST['submit'])){
					$email = $_POST['email'];
					try{
						$bdd = new PDO('mysql:host=localhost;dbname=bdd_hereouigo;charset=utf8',$username,$password);
						$req = $bdd->prepare("SELECT * FROM membre WHERE mail=:mail;");
						if($req->execute(array("mail"=>$email))){
							$data = $req->fetch();
							if(count($data)>1){
								/*script de génération de mdp et envoie par mail*/
								$char = "abcdefghijklmnopqrstuvwxyz0123456789AZERTYUIOPQSDFGHJKLMWXCVBN";
								$mdp = str_shuffle($char);
								$mdp = mb_substr($mdp, 0, 5);
								$mdph = password_hash($mdp, PASSWORD_DEFAULT);
								$req = $bdd->prepare("UPDATE membre SET mot_de_passe=:mdph WHERE mail=:mail;");
								if($req->execute(array("mdph"=>$mdph, "mail"=>$email))){
									/*ne fonctionne pas en local. il faudrait un serveur smtp*/
									$sujet = "Changement de mot de passe";
									$message = "Bonjour, le mot de passe suivant vous a été attribué : ".$mdp."\r\n Vous pouvez dès à présent vous connecter avec celui-ci. Pensez à le changer dans votre profil. \r\n L'équipe HereOuiGo !";
									$de = "HereOuiGo";
									$entete = "De: ".$de."\r\n";
									$entete.= "Content-type: text/plain; charset=UTF-8" . "\r\n";
									if(@mail($email,$sujet,$message,$entete)){
										echo "<div class='valid_box'><p>Un nouveau mot de passe a été envoyé à l'adresse renseignée.</p>
										<p><a href='index.php'>Retourner à l'accueil</a></p></div>";
									}
									else{
										include("./include/formulaire_mdp_oublie.php");
										echo "<div class='error_box'><p>Une erreur s'est produite lors de l'envoi de votre mot de passe par e-mail.</p>
										<p>Débug: {$mdp}</p></div>";
									}
								
								}else{
									include("./include/formulaire_mdp_oublie.php");
									echo "<div class='error_box'><p>Une erreur s'est produite lors de la mise à jour de vos informations.</p></div>";
								}
							}else{
								include("./include/formulaire_mdp_oublie.php");
								echo "<div class='error_box'><p> Aucun utilisateur n'est enregistré avec cette adresse e-mail </p></div>";
							}
						}
						else{
							include("./include/formulaire_mdp_oublie.php");
							echo "<div class='error_box'><p>Une erreur s'est produite lors de la mise à jour de vos informations.</p></div>";						
						}
						// On oublie pas de  fermer le curseur d'analyse des résultats après l'utilisation 
						$req->closeCursor();
 
						// Déconnexion de la BDD
						unset( $bdd );
					}
					catch(PDOException $e){
						include("./include/formulaire_mdp_oublie.php");
						print "<p> Erreur : ".$e->getMessage()." </p>";
					}
				}else{
					include("./include/formulaire_mdp_oublie.php");
				}
			?>
		</div>
		<?php
			include("./include/footer.php");
		?>

	</body>
</html>