<?php
	error_reporting(E_ALL);
	empty($_SESSION)? session_start() : print "";
	include("./BD/info_bd.php");
?>

<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title> HereOuiGo - voyagez tranquille </title>
		<link rel="icon" href="./WebContents/icon.png" sizes="64x64" />
		<link rel="stylesheet" href="styles.css"/>
		<script src="./Scripts/monscript.js"></script>

		<!--[if lt IE 9]>
			<script src="./Scripts/html5shiv.js"></script>
		<![endif] -->
	</head>
	<body>

	<?php
		include("./include/header.php");
	?>
	<div id="main">
	<?php
		if(isset($_POST['submit']) && isset($_SESSION['admin'])){
				$email=$_POST['pseudo'];
				try{
					// Connexion à la BDD
					$bdd = new PDO('mysql:host=localhost;dbname=bdd_hereouigo;charset=utf8', $username, $password);
					// Vérifier que le membre existe déjà dans la BDD
					$req_verif =$bdd->prepare("SELECT * 
								FROM membre 
								WHERE mail = :email;");
					if($req_verif->execute(array('email' => $email))){
						$data = $req_verif->fetch();
						if(count($data) > 1){
							$req_verif = $bdd->prepare("DELETE FROM admin WHERE mail=:email;");
							if($req_verif->execute(array('email' => $email))){
								echo "
									<div class='valid_box'>
									<p>Cet utilisateur n'est plus un admin !</p>
									<p><a href='index.php?admin=TRUE'> Retourner à l'administration </a></p>
									</div>";
							}else{
								include("./include/formulaire_delete_admin.php");
								echo "
									<div class='error_box'>
									<p>Une erreur s'est produite lors de l'execution de votre demande, veuillez réessayer !</p>
									</div>";
							}
						}
						else{
							include("./include/formulaire_delete_admin.php");
							echo "
							<div class='error_box'>
							<p>Cet utilisateur n'est pas référencé dans la base de donnée.</p>
							</div>";
						}
					}
					else{
						include("./include/formulaire_delete_admin.php");
						echo "
						<div class='error_box'>
						<p>Une erreur s'est produite lors de l'execution de votre demande, veuillez réessayer !</p>
						</div>";						
					}
					// On oublie pas de  fermer le curseur d'analyse des résultats après l'utilisation 
					$req_verif->closeCursor();
 
					// Déconnexion de la BDD
					unset( $bdd );
				}
				catch(PDOException $e){
					print"Erreur ! : ".$e->getMessage()."</br>";
					die();
				}
		}
		elseif(isset($_SESSION['admin'])){
			include("./include/formulaire_delete_admin.php");
		}
		else{
				echo "
					<div class='error_box'>
					<p>Vous n'avez pas accès à cette demande.</p>
					<a href='index.php'> Retourner à l'accueil </a>
					</div>";
		}

	?>

		
	</div>
		<?php
			include("./include/footer.php");
		?>
	</body>
</html>