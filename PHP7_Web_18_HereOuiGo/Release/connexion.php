<?php
	error_reporting(E_ALL);
	empty($_SESSION)? session_start() : print "";
	include("./BD/info_bd.php");
?>

<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title> HereOuiGo - voyagez tranquille </title>
		<link rel="icon" href="./WebContents/icon.png" sizes="64x64" />
		<link rel="stylesheet" href="styles.css"/>
		<script src="./Scripts/monscript.js"></script>

		<!--[if lt IE 9]>
			<script src="./Scripts/html5shiv.js"></script>
		<![endif] -->
	</head>
	<body>
	<?php
		include("./include/header.php");
	?>
	<div id="main">
	<?php
		
		// Un invité a lancé une connexion
		if(isset($_POST['submit'])){
			if(isset($_POST['email']) && isset($_POST['pass-co'])){
					try{

						$bdd=new PDO('mysql:host=localhost;dbname=bdd_hereouigo;charset=utf8',$username,$password);

						// Vérification que compte non bloqué
						$req = $bdd->prepare("SELECT * FROM blacklist WHERE mail=:mail;");
						if($req->execute(array("mail" => $_POST['email']))){
							$data = $req->fetch();
							if(count($data) > 1){
								echo "
								<div class='error_box'>
								<p>Ce compte utilisateur a été bloqué.</p>
								<p>Veuillez <a href='contact.php'>contacter un administrateur</a></p>
								</div>";
							}else{

								// Vérification identifiants
							
								$req=$bdd->prepare("SELECT * FROM membre WHERE mail=:mail");
								if($req->execute(array("mail" => $_POST['email']))){
									$ligne=$req->fetch();
									if(count($ligne) > 1){
										if(password_verify($_POST['pass-co'], $ligne['mot_de_passe'])){
											$_SESSION['pseudo'] = $ligne['pseudo'];
											$_SESSION['mail'] = $ligne['mail'];
											$_SESSION['prenom'] = $ligne['prenom'];
											$_SESSION['nom'] = $ligne['nom'];
											$_SESSION['auth'] = TRUE;

											// On vérifie si le membre est dans le groupe des admins
											$req =$bdd->prepare("SELECT * FROM admin WHERE mail = ?;");
											$req->execute(array($ligne['mail']));
											$data = $req->fetch();
											if(count($data) > 1){
												$_SESSION['admin'] = TRUE;
											}
											sleep(2);
											header("Location: ./index.php"); // Retour page d'accueil
										}
										else{
											include("./include/formulaire_connexion.php");
											echo "
											<div class='error_box'>
											<p>Votre identifiant ou votre mot de passe est incorrect.</p>
											</div>";
										}
									}else{
										include("./include/formulaire_connexion.php");
											echo "
											<div class='error_box'>
											<p>Votre identifiant ou votre mot de passe est incorrect.</p>
											</div>";
									}
								}else{
									include("./include/formulaire_connexion.php");
									echo "
									<div class='error_box'>
									<p>Votre identifiant ou votre mot de passe est incorrect.</p>
									</div>";
								}

							}
						}else{
							include("./include/formulaire_connexion.php");
							echo "
								<div class='error_box'>
								<p>Erreur d'accès à la base de données.</p>
								<p>Veuillez <a href='contact.php'>contacter un administrateur</a></p>
								</div>";
						}
					// On oublie pas de  fermer le curseur d'analyse des résultats après l'utilisation 
					$req->closeCursor();
 
					// Déconnexion de la BDD
					unset( $bdd );
					}
					catch(PDOException $e){
						print "<div class='error_box'><p>Erreur ! : ".$e->getMessage()."</p></div>";
						die();
					}
			}
		}


		else{
			// Dans tous les autres cas on donne le formulaire de connexion
			include("./include/formulaire_connexion.php");
		}

	?>

		
	</div>
		<?php
			include("./include/footer.php");
		?>
	</body>
</html>