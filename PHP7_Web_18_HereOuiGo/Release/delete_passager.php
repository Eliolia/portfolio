<?php
    error_reporting(E_ALL);
    empty($_SESSION)? session_start() : print "";
    include("./BD/info_bd.php");
?>

<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title> HereOuiGo - voyagez tranquille </title>
        <link rel="icon" href="./WebContents/icon.png" sizes="64x64" />
        <link rel="stylesheet" href="styles.css"/>
        <script src="./Scripts/monscript.js"></script>

        <!--[if lt IE 9]>
            <script src="./Scripts/html5shiv.js"></script>
        <![endif] -->
    </head>
    <body>

    <?php
        include("./include/header.php");
    ?>
    <div id="main">
    <?php
        if(isset($_POST['id_offre']) && isset($_SESSION['auth'])){
                $email=$_SESSION['mail'];
                $offre=intval($_POST['id_offre']);
                try{
                    // Connexion à la BDD
                    $bdd = new PDO('mysql:host=localhost;dbname=bdd_hereouigo;charset=utf8', $username, $password);
                    $req_verif = $bdd->prepare("DELETE FROM passager WHERE mail=:email AND id_offre=:offre;");
                        if($req_verif->execute(array('email' => $email, 'offre'=>$offre))){
                            $req_verif= $bdd->prepare("UPDATE offre SET nombre_place = nombre_place + 1 WHERE id_offre = :id_offre");
                            if($req_verif->execute(array("id_offre" => $offre))){
                                echo "
                                <div class='valid_box'>
                                <p>Votre réservation est annulée!</p>
                                <p><a href='mes_trajets.php'>Retourner à mes trajets</a></p>
                                </div>";   
                            }else{
                                //Il y a eu une erreur mais ne concerne pas le passager
                                echo "
                                <div class='valid_box'>
                                <p>Votre réservation est annulée!</p>
                                <p><a href='mes_trajets.php'>Retourner à mes trajets</a></p>
                                </div>";
                            }
                            
                        }else{
                            echo "
                                <div class='error_box'>
                                <p>Une erreur s'est produite lors de l'execution de votre demande, veuillez réessayer !</p>
                                </div>";
                        }
                    // On oublie pas de  fermer le curseur d'analyse des résultats après l'utilisation 
                    $req_verif->closeCursor();
 
                    // Déconnexion de la BDD
                    unset( $bdd );
                }
                catch(PDOException $e){
                    print"Erreur ! : ".$e->getMessage()."</br>";
                    die();
                }
        }
        else{
                echo "
                    <div class='error_box'>
                    <p>Vous n'avez pas accès à cette demande.</p>
                    <a href='index.php'> Retourner à l'accueil </a>
                    </div>";
        }

    ?>

       
    </div>
        <?php
            include("./include/footer.php");
        ?>
    </body>
</html>