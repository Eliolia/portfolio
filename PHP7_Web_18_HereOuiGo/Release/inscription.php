<?php
	error_reporting(E_ALL);
	empty($_SESSION)? session_start() : print "";
	include("./BD/info_bd.php");
?>

<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title> HereOuiGo - voyagez tranquille </title>
		<link rel="icon" href="./WebContents/icon.png" sizes="64x64" />
		<link rel="stylesheet" href="styles.css"/>
		<script src="./Scripts/monscript.js"></script>

		<!--[if lt IE 9]>
			<script src="./Scripts/html5shiv.js"></script>
		<![endif] -->
	</head>
	<body>
		<?php include("./include/header.php"); ?>
		<div id="main">
			<?php

				if(isset($_SESSION['auth'])){
					echo "<div class='error_box'>
							<p>Vous êtes connecté en tant que membre. Si vous souhaitez créer une nouvelle inscription, veuillez vous déconnecter de ce compte.</p></div>";
							exit();
				}

				if(isset($_POST['submit']) && isset($_POST['password']) && isset($_POST['password2'])){
					if($_POST['password'] == $_POST['password2']){


					// Informations générales
					$pseudo = $_POST['pseudo'];
					$email = $_POST['email'];
					$telephone = $_POST['numero'];
					$nom = strtoupper($_POST['nom']);
					$prenom = strtoupper($_POST['prenom']);
					$age = $_POST['date'];
					// Ici c'est pour l'adresse
					$num_voie = intval($_POST['numvoie']);
					$nom_voie = strtoupper($_POST['nomrue']);
					$complement = strtoupper($_POST['compl']);
					$code_postal = $_POST['codep'];
					$ville = strtoupper($_POST['ville']);

					$pass_hash = password_hash($_POST['password'], PASSWORD_DEFAULT);
					try{
						// Connexion à la BDD
						$bdd = new PDO('mysql:host=localhost;dbname=bdd_hereouigo;charset=utf8', $username, $password);

						// Vérifier que le membre n'existe pas déjà dans la BDD

						$req_verif =$bdd->prepare("SELECT * 
										FROM membre 
										WHERE mail = :email;");
						$req_verif->execute(array('email' => $email));
						$data = $req_verif->fetch();
						if(count($data) > 1){
							echo "
							<div class='error_box'>
							<p>Cet utilisateur est déjà enregistré : ".$data['prenom']." ".$data['nom']." alias ".$data['pseudo']."</p></div>";
							include("./include/formulaire_inscription.php");
							exit();
						}

						// Vérification d'abord que l'adresse n'existe pas déjà sinon on l'ajoute

						$req_verif =$bdd->prepare("SELECT * 
										FROM adresse 
										WHERE numero_voie = :num_voie
										AND nom_voie = :nom_voie
										AND complement_adresse = :complement
										AND code_postal = :code_postal
										AND ville = :ville;");

						if($req_verif->execute(array('num_voie'=>$num_voie, 'nom_voie'=>$nom_voie, 'complement'=>$complement, 'code_postal'=>$code_postal, 'ville'=>$ville))){

							$data_adresse = $req_verif->fetch();
							if(count($data_adresse) > 1){
								// On récupère juste id_adresse et on poursuit l'inscription
								$id_adresse = $data_adresse['id_adresse'];

							}else{
								// On crée une nouvelle adresse et on récupère id_adresse
								$req_verif = $bdd->prepare("INSERT INTO adresse (numero_voie, nom_voie, complement_adresse, code_postal, ville) 
									VALUES(:num_voie, :nom_voie, :complement, :code_postal, :ville);");
								if($req_verif->execute(array('num_voie'=>$num_voie, 'nom_voie'=>$nom_voie, 'complement'=>$complement, 'code_postal'=>$code_postal, 'ville'=>$ville))){
									$req_verif =$bdd->prepare("SELECT id_adresse 
										FROM adresse 
										WHERE numero_voie = :num_voie
										AND nom_voie = :nom_voie
										AND complement_adresse = :complement
										AND code_postal = :code_postal
										AND ville = :ville;");
									$req_verif->execute(array('num_voie'=>$num_voie, 'nom_voie'=>$nom_voie, 'complement'=>$complement, 'code_postal'=>$code_postal, 'ville'=>$ville));
									$data_adresse = $req_verif->fetch();
									if(count($data_adresse) == 2){$id_adresse = $data_adresse['id_adresse'];}
									else{
										print "<div class='error_box'><p>Une erreur s'est produite. Veuillez renouveler votre inscription.</p></div>";
										include("./include/formulaire_inscription.php");
										exit();
									}
									

								}
								else{
									print "<div class='error_box'><p>Une erreur s'est produite. Veuillez renouveler votre inscription.</p></div>";
									include("./include/formulaire_inscription.php");
									exit();
								}
							}
							// Après avoir trouvé id_adresse, on insert un membre avec ses informations

							$req_verif = $bdd->prepare("INSERT INTO membre (mail, pseudo, numero, nom, prenom, date_naissance, fk_adresse, mot_de_passe)
										VALUES (:email, :pseudo, :telephone, :nom, :prenom, :date_naissance, :id_adresse, :pass_hash);");

							if($req_verif->execute(array('email'=>$email, 'pseudo'=>$pseudo, 'telephone'=>$telephone, 'nom'=>$nom, 'prenom'=>$prenom, 'date_naissance'=>$age, 'id_adresse'=>$id_adresse, 'pass_hash'=>$pass_hash))){
								echo "<div class='valid_box'><p>Inscription de $prenom $nom réussie.</p>
										<p>Vous pouvez désormais <a href='./connexion.php'>vous connecter</a> avec vos identifiants.</p></div>";
							}else{
								print "<div class='error_box'><p>Une erreur s'est produite. Veuillez renouveler votre inscription.</p></div>";
								include("./include/formulaire_inscription.php");
							}
						}else{
							print "<div class='error_box'><p>Une erreur s'est produite. Veuillez renouveler votre inscription.</p></div>";
							include("./include/formulaire_inscription.php");
						}	
						// On oublie pas de  fermer le curseur d'analyse des résultats après l'utilisation 
						$req_verif->closeCursor();
 
						// Déconnexion de la BDD
						unset( $bdd );					

					}catch(PDOException $e){
						print "<div class='error_box'><p>Erreur ! : ".$e->getMessage()."</p></div>";
						die();
					}
				}else{
					print "<div class='error_box'><p>Votre mot de passe et sa confirmation ne sont pas identiques, veuillez réessayer !</p></div>";
					include("./include/formulaire_inscription.php");
				}
			}else{
				include("./include/formulaire_inscription.php");
			} ?>
			
		</div>
			<?php include("./include/footer.php"); ?>
	</body>
</html>