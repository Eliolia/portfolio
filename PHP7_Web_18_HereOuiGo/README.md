# HereOuiGo : La plateforme de covoiturage communautaire

## Résumé

C’est dans le cadre de l’exploitation et l’application de nos connaissances acquises dans l’UE Architecture et programamtion du web et de l'UE Systèmes de gestion de bases de données 2 que la réalisation de ce projet, sur une plateforme de covoiturage, s'incrit.

## Fonctionnalités implémentées

Le site internet dispose de 3 vues chargées dynamiquement et associées à 3 profils utilisateurs différents : admin, membre, invité.

* L'utilisateur "invité" pourra rechercher des offres de covoiturage selon différents critères par dates et selon un tri spécifique. Les informations affichées sont limités du fait qu'il n'est pas enregistré. Il peut contacter les administrateurs et s'inscrire sur la plateforme.

* L'utilisateur "membre" hérite des fonctionnalitées de l'invité. Il dispose de son propre profil et peut consulter celui des autres, ajouter un véhicule et ainsi proposer des trajets qui seront alors visibles par tous. Il pourra déposer un avis à la fois en tant que passager et en tant que conducteur, ce qui influencera sa e-réputation sur la plateforme.

* L'utilisateur "admin" hérite des deux précédents. Il peut gérer les membres à travers une interface dédiée où il peut bloquer, débloquer, supprimer, ajouter des comptes membres, voire modifier certaines informations qui ne sont pas des informations générales. Il peut également accéder à des statistiques et créer manuellement des trajets types. 

Concernant la base de données, tout se trouve dans le rapport de ce projet.

## Utilisation de l'application

### Installation du Serveur Web

Veillez à disposer d'un serveur Web (Apache par exemple) lié à une base de donnée MySQL et un moteur d'interprétation PHP 7.1 et d'un accès à PHPmyAdmin.

Voici une liste de plateforme de développement en fonction de votre système d'exploitation :
* **WINDOWS** : [WAMP](http://www.wampserver.com/) 
* **LINUX** : [XAMPP](https://www.apachefriends.org/fr/index.html)
* **MAC OS** : [MAMP](https://www.mamp.info/en/)

Vous pouvez également installer manuellement chaque composante. Pensez à sécuriser vos accès FTP, MySQL et PHPMyAdmin.

### Installation de l'application

Rendez-vous sur votre interface PHPMyAdmin puis créez une nouvelle *BD bdd_hereouigo*;
Aller dans l'onglet "Importer" de votre nouvelle BD puis selectionner le fichier *./BD/bdd_hereouigo.sql*;
Dans le fichier ./BD/info_bd.php, changez éventuellement le $username et le $password si besoin est pour la connexion à la BD MySQL;

Placer le dossier HereOuiGo à la racine de votre dossier htdocs (Emplacement pouvant différent en fonction de votre configuration);

Une fois fait, vous pouvez naviguer sur l'Application à partir de la racine du projet.

## En savoir plus

Ouvrez le rapport qui porte le nom "rapport_HereOuiGo_20171217.pdf".