# Surfaces MLS

## Résumé

C’est dans le cadre de l’exploitation et l’application de nos connaissances acquises dans l’UE Informatique Graphique que la réalisation de ce mini-projet s'incrit.

Il s'agit d'appliquer une projection d'un nuage de points aléatoire sur des surfaces MLS calculées à partir de modèles 3D. 

L'objectif est d'obtenir un Point Set de taille réduite par rapport au modèle initial pour des raisons de temps de calcul au chargement et d'espace mémoire.

## Notions abordées

* Chargement de modèles 3D ;
* Calculs du poids (ou kernel) des points selon plusieurs formules (Gaussien, Wendland et Singular) ;
* Implémentation d'algorithmes MLS qui sont Hermit Point Set Surfaces et Algebric Point Set Surfaces.

## Résultats en images

Tous les résultats en images sont dans le dossier "images".