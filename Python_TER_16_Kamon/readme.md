# Kamon

## Règles

Le Kamon est un jeu de société abstrait créé par Bruno Cathala en 2007. 

Deux joueurs alternent la mise en place de pions sur un plateau hexagonal de 37 cases, où chaque côté est représenté par un animal. Chaque joueur dispose de 18 jetons, qu'ils jouent tour à tour. Chaque coup impose une contrainte au joueur suivant, de telle sorte qu'il est obligé de jouer sur une case comportant soit le même nombre soit la même couleur que le jeton joué précédemment. 

Pour remporter la partie, chaque joueur a trois possibilités :

* former une ligne de ses propres pions en reliant 2 côtés opposés ;
* créer une boucle en entourant au moins 1 case, vide ou non ;
* faire en sorte que l’adversaire soit bloqué en ne pouvant plus placer ses jetons.

Si tous les pions Kamons sont retirés du plateau et qu’aucune des conditions de victoire n’est remplie par l’un des deux joueurs, alors la partie est considérée comme nulle.

## Comment jouer ?

Le mode principal est à 2 joueurs. 

Le joueur noir commence en premier systématiquement. 

Le joueur dont c'est le tour est cerclé de rouge et signalé en bas du plateau.

L'aide cercle les cases qui sont jouables par le joueur courant d'une couleur bleue.

Le coup précédent est affiché en haut à droite.

Pour jouer il suffit de cliquer tour à tour sur les cases jouables jusqu'à atteindre une condition de victoire ou d'égalité.

Vous pouvez quitter ou créer une nouvelle partie dans le menu "Fichier", activer/désactiver l'aide de jeu dans le menu "Affichage" ou lire les règles dans le menu "Aide".

## Implémentation

Le jeu est développé en Python 3.5.1 et utilise la librairie graphique TKinter.

Le projet est décomposée en plusieurs fonctions permettant d'exécuter l'intégralité du fonctionnement du jeu dans la game loop.

## Les améliorations envisageables

Plusieurs améliorations du projet ont été envisagées :

* condition de victoire en ligne ;
* condition de victoire en boucle ;
* IA naïve ;
* IA progressive ;
* enregistrer et charger une partie ;
* timer par joueur.

## Exécuter

S'assurer d'avoir Python 3.5.x installé sur son système d'exploitation et exécuter le fichier kamon_v1.py.

## Démonstration vidéo

Dans le dossier "Démonstration" se trouve la vidéo en question
