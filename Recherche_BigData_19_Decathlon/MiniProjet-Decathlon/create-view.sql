-- 

DROP VIEW SPView;
DROP VIEW VPView;
DROP VIEW SMView;
DROP VIEW VMView;
DROP VIEW VCDView;
DROP VIEW SCDView;
DROP VIEW VLView;
DROP VIEW SLView;

CREATE OR REPLACE VIEW SPView
AS	SELECT s.idProduit, s.idLieu, s.idDate, s.idMagasin, p.description, p.prixHT, p.prixAchatFournisseur, p.tauxTaxe, p.prixTTC, p.marge, p.categorie, p.sousCategorie, p.typeConditionnement, p.tailleConditionnement, p.poidsNet, p.hauteur, p.largeur, p.profondeur, s.quantiteEnRayon, s.quantiteEntrepot, s.quantiteTotale
	FROM Stock s, Produit p 
	WHERE s.idProduit = p.idProduit;

CREATE OR REPLACE VIEW VPView
AS	SELECT v.idProduit, v.idClient, v.idTemps, v.idLieu, v.idDate, v.idMagasin, p.description, p.prixHT, p.prixAchatFournisseur, p.tauxTaxe, p.prixTTC, p.marge, p.categorie, p.sousCategorie, p.typeConditionnement, p.tailleConditionnement, p.poidsNet, p.hauteur, p.largeur, p.profondeur, v.numeroTransaction, v.quantiteVendue, v.prixVente
	FROM Vente v, Produit p 
	WHERE v.idProduit = p.idProduit;

CREATE OR REPLACE VIEW SMView
AS 	SELECT s.idMagasin, s.idProduit, s.idLieu, s.idDate, m.nomMagasin, m.numeroMagasin, m.nbEmployees, m.gerantMagasin, m.SurfaceDuMagasin, m.SurfaceDeStockage, m.SurfaceDeVente, m.datePremiereOuverture, m.dateFermetureMagasin, s.quantiteEnRayon, s.quantiteEntrepot, s.quantiteTotale
	FROM Stock s, Magasin m
	WHERE s.idMagasin = m.idMagasin;

CREATE OR REPLACE VIEW VMView
AS	SELECT v.idMagasin, v.idClient, v.idProduit, v.idTemps, v.idLieu, v.idDate, m.nomMagasin, m.numeroMagasin, m.nbEmployees, m.gerantMagasin, m.SurfaceDuMagasin, m.SurfaceDeStockage, m.SurfaceDeVente, m.datePremiereOuverture, m.dateFermetureMagasin, v.numeroTransaction, v.quantiteVendue, v.prixVente
	FROM Vente v, Magasin m 
	WHERE v.idMagasin = m.idMagasin;

CREATE OR REPLACE VIEW VCDView
AS    SELECT v.idDate, v.idClient, v.idProduit, v.idTemps, v.idLieu, v.idMagasin, cd.jour, cd.jourSemaine, cd.jourAnnee, cd.semaine, cd.semaineDuMois, cd.mois, cd.trimestre, cd.annee, cd.saison, cd.vacances, v.numeroTransaction, v.quantiteVendue, v.prixVente
    FROM Vente v, CurrentDate cd 
    WHERE v.idDate = cd.idDate;

CREATE OR REPLACE VIEW SCDView
AS    SELECT s.idDate, s.idProduit, s.idLieu, s.idMagasin, cd.jour, cd.jourSemaine, cd.jourAnnee, cd.semaine, cd.semaineDuMois, cd.mois, cd.trimestre, cd.annee, cd.saison, cd.vacances, s.quantiteEnRayon, s.quantiteEntrepot, s.quantiteTotale
    FROM Stock s, CurrentDate cd 
    WHERE s.idDate = cd.idDate;

CREATE OR REPLACE VIEW VLView
AS    SELECT v.idLieu, v.idClient, v.idProduit, v.idTemps, v.idDate, v.idMagasin, l.pays, l.departement, l.region, l.ville, l.quartier, l.rue, l.codeCommune, l.zipCode, l.cedex, v.numeroTransaction, v.quantiteVendue, v.prixVente
    FROM Vente v, Lieu l 
    WHERE v.idLieu = l.idLieu;

CREATE OR REPLACE VIEW SLView
AS    SELECT s.idLieu, s.idProduit, s.idDate, s.idMagasin, l.pays, l.departement, l.region, l.ville, l.quartier, l.rue, l.codeCommune, l.zipCode, l.cedex, s.quantiteEnRayon, s.quantiteEntrepot, s.quantiteTotale
    FROM Stock s, Lieu l 
    WHERE s.idLieu = l.idLieu;