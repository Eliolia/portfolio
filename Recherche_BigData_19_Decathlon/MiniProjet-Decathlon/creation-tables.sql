DROP TABLE Vente;
DROP TABLE Stock;
DROP TABLE Client;
DROP TABLE Produit;
DROP TABLE Temps;
DROP TABLE Lieu;
DROP TABLE CurrentDate;
DROP TABLE Magasin;

CREATE TABLE Magasin(
	idMagasin 				NUMERIC(8) NOT NULL,
	nomMagasin 				VARCHAR2(64) NOT NULL,
	numeroMagasin 			NUMERIC(6) NOT NULL,
	nbEmployees				NUMERIC(6),
	gerantMagasin			VARCHAR2(64),
	SurfaceDuMagasin		NUMERIC(6),
	SurfaceDeStockage		NUMERIC(6),
	SurfaceDeVente			NUMERIC(6),
	datePremiereOuverture	DATE NOT NULL,
	dateFermetureMagasin	DATE
);

CREATE TABLE CurrentDate(
	idDate					NUMERIC(8) NOT NULL,
	jour 					NUMERIC(2) NOT NULL,
	jourSemaine				NUMERIC(1) NOT NULL,
	jourAnnee				NUMERIC(3) NOT NULL,
	semaine 				NUMERIC(2) NOT NULL,
	semaineDuMois 			NUMERIC(1) NOT NULL,
	mois 					NUMERIC(2) NOT NULL,
	trimestre 				NUMERIC(1) NOT NULL,
	annee 					NUMERIC(4) NOT NULL,
	saison 					VARCHAR2(12) NOT NULL,
	vacances				NUMERIC(1)
);

CREATE TABLE Lieu(
	idLieu 					NUMERIC(8) NOT NULL,
	pays					VARCHAR2(64) NOT NULL,
	departement 			VARCHAR2(64) NOT NULL,
	region 					VARCHAR2(64) NOT NULL,
	ville 					VARCHAR2(128) NOT NULL,
	quartier 				VARCHAR2(64),
	rue						VARCHAR2(128),
	codeCommune				NUMERIC(5),
	zipCode					NUMERIC(5) NOT NULL,
	cedex					NUMERIC(2)
);

CREATE TABLE Temps(
	idTemps					NUMERIC(8) NOT NULL,
	heure					NUMERIC(2) NOT NULL,
	minute					NUMERIC(2) NOT NULL,
	seconde					NUMERIC(2) NOT NULL,
	fuseauHoraire 			VARCHAR2(3) NOT NULL,
	indicateurAMPM			VARCHAR2(2) NOT NULL,
	momentJournee			VARCHAR2(32)
);

CREATE TABLE Produit(
	idProduit				NUMERIC(8) NOT NULL,
	description				VARCHAR2(128) NOT NULL,
	prixHT 					NUMERIC(8,2) NOT NULL,
	prixAchatFournisseur	NUMERIC(8,2) NOT NULL,
	tauxTaxe				NUMERIC(4,2) NOT NULL,
	prixTTC 				NUMERIC(8,2) NOT NULL,
	marge 					NUMERIC(8,2) NOT NULL,
	marque 					VARCHAR2(64) NOT NULL,
	categorie 				VARCHAR2(128) NOT NULL,
	sousCategorie 			VARCHAR2(128) NOT NULL,
	typeConditionnement 	VARCHAR2(32),
	tailleConditionnement 	NUMERIC(3),
	poidsNet 				NUMERIC(5,2),
	hauteur 				NUMERIC(4),
	largeur 				NUMERIC(4),
	profondeur				NUMERIC(4)
);

CREATE TABLE Client(
	idClient 				NUMERIC(8) NOT NULL,
	prenom 					VARCHAR2(64),
	nom 					VARCHAR2(64),
	adresse 				VARCHAR2(64),
	ville 					VARCHAR2(128),
	mail 					VARCHAR2(128),
	dateProgrammeFidelite	DATE
);

CREATE TABLE Stock(
	idProduit 				NUMERIC(8) NOT NULL,
	idLieu 					NUMERIC(8) NOT NULL,	
	idDate 					NUMERIC(8) NOT NULL,
	idMagasin 				NUMERIC(8) NOT NULL,
	quantiteEnRayon 		NUMERIC(8) NOT NULL,
	quantiteEntrepot 		NUMERIC(8) NOT NULL,
	quantiteTotale			NUMERIC(8) NOT NULL	
);

CREATE TABLE Vente(
	idClient 				NUMERIC(8) NOT NULL,
	idProduit				NUMERIC(8) NOT NULL,
	idTemps 				NUMERIC(8) NOT NULL,
	idLieu 					NUMERIC(8) NOT NULL,
	idDate 					NUMERIC(8) NOT NULL,
	idMagasin 				NUMERIC(8) NOT NULL,
	numeroTransaction 		NUMERIC(8) NOT NULL, 
	quantiteVendue 			NUMERIC(8) NOT NULL,
	prixVente 				NUMERIC(8) NOT NULL
);

ALTER TABLE CurrentDate ADD CONSTRAINT CHECK_jour CHECK (jour BETWEEN 0 AND 31);

ALTER TABLE CurrentDate ADD CONSTRAINT CHECK_jourSemaine CHECK (jourSemaine BETWEEN 1 AND 7);

ALTER TABLE CurrentDate ADD CONSTRAINT CHECK_jourAnnee CHECK (jourAnnee BETWEEN 1 AND 366);

ALTER TABLE CurrentDate ADD CONSTRAINT CHECK_semaine CHECK (semaine BETWEEN 1 AND 53);

ALTER TABLE CurrentDate ADD CONSTRAINT CHECK_semaineDuMois CHECK (semaineDuMois BETWEEN 1 AND 5);

ALTER TABLE CurrentDate ADD CONSTRAINT CHECK_mois CHECK (mois BETWEEN 1 AND 12);

ALTER TABLE CurrentDate ADD CONSTRAINT CHECK_trimestre CHECK (trimestre BETWEEN 1 AND 4);

ALTER TABLE CurrentDate ADD CONSTRAINT CHECK_annee CHECK (annee >= 1976);

ALTER TABLE CurrentDate ADD CONSTRAINT CHECK_vacances CHECK (vacances BETWEEN 0 AND 1);

ALTER TABLE Temps ADD CONSTRAINT CHECK_heure CHECK (heure BETWEEN 0 AND 12);

ALTER TABLE Temps ADD CONSTRAINT CHECK_minute CHECK (minute BETWEEN 0 AND 60);

ALTER TABLE Temps ADD CONSTRAINT CHECK_seconde CHECK (seconde BETWEEN 0 AND 60);

ALTER TABLE Temps ADD CONSTRAINT CHECK_indicateurAMPM CHECK (indicateurAMPM IN ('AM', 'PM'));

ALTER TABLE Temps ADD CONSTRAINT CHECK_momentJourneee CHECK (momentJournee IN ('Matin', 'Midi', 'Apres-Midi', 'Soir'));


ALTER TABLE Produit ADD CONSTRAINT CHECK_prixHT CHECK (prixHT >= 0.0);

ALTER TABLE Produit ADD CONSTRAINT CHECK_prixAchatFournisseur CHECK (prixAchatFournisseur >= 0.0);

ALTER TABLE Produit ADD CONSTRAINT CHECK_tauxTaxe CHECK (tauxTaxe BETWEEN 0 AND 99);

ALTER TABLE Produit ADD CONSTRAINT CHECK_prixTTC CHECK (prixTTC >= 0.0);

ALTER TABLE Produit ADD CONSTRAINT CHECK_marge CHECK (marge >= 0.0);

ALTER TABLE Stock ADD CONSTRAINT CHECK_quantiteEnRayon CHECK (quantiteEnRayon >= 0);

ALTER TABLE Stock ADD CONSTRAINT CHECK_quantiteEntrepot CHECK (quantiteEntrepot >= 0);

ALTER TABLE Stock ADD CONSTRAINT CHECK_quantiteTotale CHECK (quantiteTotale >= 0);

ALTER TABLE Vente ADD CONSTRAINT CHECK_prixVente CHECK (prixVente >= 0.0);