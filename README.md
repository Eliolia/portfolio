# Portfolio

Ce dépôt git contient l'ensemble des projets personnels et universitaires d'Elodie Tribout.

Chaque dossier comporte : 

* Un readme.md expliquant brièvement le projet et comment l'exécuter;

* Une vidéo démo, quand cela est possible.