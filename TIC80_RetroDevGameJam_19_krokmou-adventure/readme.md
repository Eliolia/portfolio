# Krokmou Adventure

## Résumé

Il s'agit d'un simple platformer réalisé rapidement sur TIC80, dans le cadre d'une UE de jeux sérieux.

## Comment jouer ?

Pour se déplacer il suffit d'utiliser les flèches directionnelles et la touche "x" pour valider.

## Implémentation

TIC80 est un ensemble d'outils allant de la création d'assets (graphiques, sonores) et à l'élaboration de scripts.

Le scripting se fait avec un langage embarqué proche du LUA.

## Les améliorations envisageables

Plusieurs améliorations du projet ont été envisagées :

* écran d'accueil ;
* écran de fin avec retour à l'accueil ;
* niveaux supplémentaires.

## Exécuter

Ouvrir le fichier krokmou_adventure.tic.html dans votre navigateur préféré.

## Démonstration vidéo

Dans le dossier "Démonstration" se trouve la vidéo en question
