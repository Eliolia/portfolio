# Equilibrium

## Résumé

Le jeu a été créé lors de la Climate Game Jam 2018. Une compétition que j'ai moi-même co-organisée avec les associations Hakatah et Hello World, en coopération avec de multiples acteurs (Ministère de la transition écologique et des solidarités, CNRS, Push Start, etc).

Equilibrium est un jeu sérieux se concentrant sur les principes de la résilience écologique et communautaire au travers de l'adaptation de micro-organismes dans des environnements variés. Il s'agit de souligner l'importance de la bio-diversité et d'illustrer les récentes théories néodarwinistes de l'évolution, notamment, _"la théorie virale"_.

Il s'agit d'un prototype ayant été imaginé en jeu de plateau dont les règles sont présentes dans le dossier "Documents".

## Comment jouer ?

Vous devez, en incarnant un micro-organisme aléatoire, vous adapter à votre environnement en un nombre limité de coups.

Pour se faire, vous devez absorber/assimiler d'autres micro-organismes. Cela impactera votre capacité de résistance à votre environnement, illustré par les jauges en bas de l'écran.

Pour vous déplacer, il vous faut utiliser les flèches directionnelles du clavier.

Pour absorber/assimiler d'autres micro-organismes, il vous suffit d'entrer en collision avec eux.

## Implémentation

Le projet a été développé avec le logiciel propriétaire "Construct 2", autorisant plus de souplesse niveau implémentation pour se concentrer sur les éléments de Game  Design.

En résulte une exportation HTML/CSS/JS pouvant être exécuté, quelque soit le support de jeu, sur un navigateur.

## Les améliorations envisageables

* implémenter avec un moteur différent (Python/Pygame, C#/Unity, ...) ;
* développer le contenu ;

## Exécuter

La dernière version du projet se trouve dans le dossier "Build". Pour l'exécuter en local, il faut déposer le dossier sur un webserveur (par exemple [Servez](https://greggman.github.io/servez/)) puis lancer "[127.0.0.1:8080/index.html](127.0.0.1:8080/index.html)" sur un navigateur.

## Bug report

* Vous devez obligatoirement redémarrer votre navigateur pour lancer une nouvelle partie.

* Parfois l'organisme que vous contrôlez n'apparait pas à l'écran, il faut redémarrer votre navigateur.

## Démonstration vidéo

Dans le dossier "Démonstration" se trouve la vidéo en question
