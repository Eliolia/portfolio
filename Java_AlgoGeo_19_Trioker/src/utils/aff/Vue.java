package utils.aff;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import utils.go.Etoile;
import utils.go.PointVisible;
import utils.go.Transformation;
import utils.go.Triangle;
import utils.go.Vecteur;
import utils.io.ReadWritePoint;


@SuppressWarnings("serial")
public class Vue extends JPanel implements KeyListener, MouseListener, MouseMotionListener{

	Color bgColor;
	Color fgColor; 
	int width, height;
	public static int WIDTH = 800;
	double angle;
	private int iTriangle = -1;
	int oldX, oldY;		
	int curentFile; 			// Compteur pour naviguer de puzzle en puzzle
	
	private ArrayList<String> filesToRead;											// Liste des puzzles
	private ArrayList<PointVisible> points = new ArrayList<PointVisible>();			// Points du Puzzle
	private ArrayList<PointVisible> points_droites = new ArrayList<PointVisible>();	// Points des triangles
	private ArrayList<Vecteur> aretes = new ArrayList<Vecteur>();					// Aretes du Puzzle
	private ArrayList<Vecteur> aretes_droites = new ArrayList<Vecteur>();			// Aretes des triangles
	private ArrayList<Triangle> triangles = new ArrayList<Triangle>(); 				// Ensemble des triangles jouables
	
	Rectangle rectangleElastique;
	
	/*
	 * @brief
	 * @param
	 * @return
	 */
	public Vue(int width, int height, String fileName, String fileName2, boolean t) {
		super();
		Couleur.forPrinter(true);
		this.bgColor = Couleur.bg;
		this.fgColor = Couleur.fg;
		this.width = 2*width;
		this.height = height;
		this.setBackground(Couleur.bg);
		this.setPreferredSize(new Dimension(2*width, height));
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		filesToRead = new ArrayList<String>();
		filesToRead.add(fileName2);
		initFromLog(fileName, fileName2, t);
		// Initialisation du premier puzzle
		export("trio-hypo-2.csv");
		DecimalFormat df = new DecimalFormat("#.#");
		double test = 0.523;
		System.err.println(df.format(test));
		setupFiles();
	}
	
	/*
	 * @brief
	 * @param
	 * @return
	 */
	private void setupFiles(){
		curentFile = 1;
		if(!filesToRead.contains("data/Trioker/trio-footballer.csv"))
			filesToRead.add("data/Trioker/trio-footballer.csv");
		if(!filesToRead.contains("data/Trioker/trio-aigle.csv"))
			filesToRead.add("data/Trioker/trio-aigle.csv");
		if(!filesToRead.contains("data/Trioker/trio-buste.csv"))
			filesToRead.add("data/Trioker/trio-buste.csv");
		if(!filesToRead.contains("data/Trioker/trio-hommeAssis.csv"))
			filesToRead.add("data/Trioker/trio-hommeAssis.csv");
		if(!filesToRead.contains("data/Trioker/trio-hypo.csv"))
			filesToRead.add("data/Trioker/trio-hypo.csv");
		if(!filesToRead.contains("data/Trioker/trio-hypo-2.csv"))
			filesToRead.add("data/Trioker/trio-hypo-2.csv");
		if(!filesToRead.contains("data/Trioker/trio-lampe.csv"))
			filesToRead.add("data/Trioker/trio-lampe.csv");
		if(!filesToRead.contains("data/Trioker/trio-roquet.csv"))
			filesToRead.add("data/Trioker/trio-roquet.csv");
	}


	/*
	 * @brief
	 * @param
	 * @return
	 */
	private void initFromLog(String fileName, String fileName2, boolean triangle) {
		ReadWritePoint rw = new ReadWritePoint(fileName2);
		points = rw.read(!triangle);
		ReadWritePoint rw2 = new ReadWritePoint(fileName);
		points_droites = rw2.read(triangle);
		aretes = new ArrayList<Vecteur>();
		aretes_droites = new ArrayList<Vecteur>();
		int n = points.size();
		int m = points_droites.size();
		
		// Récupération des informations concernant les triangles et les aretes associées
		for (int i = 0 ; i < m; i+=3) {
			aretes_droites.add(new Vecteur(points_droites.get(i), points_droites.get((i+1)%m)));
			aretes_droites.add(new Vecteur(points_droites.get(i+1), points_droites.get((i+2)%m)));
			aretes_droites.add(new Vecteur(points_droites.get(i+2), points_droites.get((i)%m)));
			triangles.add(new Triangle(points_droites.get(i), points_droites.get(i+1), points_droites.get(i+2),i/3));
		}
		//Puzzle
		for(int i=0; i<n;i++){
			aretes.add(new Vecteur(points.get(i), points.get((i+1)%n)));
		}
	}

	/*
	 * @brief
	 * @param
	 * @return
	 */
	public void export(String logFile) {
		ReadWritePoint rw = new ReadWritePoint(logFile);
		for (PointVisible p: points){
			rw.add((int)p.getMC().x+";"+(int)p.getMC().y+";"+p.toString());
		}
		rw.write();
	}

	/*
	 * @brief
	 * @param
	 * @return
	 */
	private void aimantation(Triangle t){
		PointVisible[] sT1 = t.getSommets();
		for(Triangle t2 : triangles){
			if(!t.equals(t2) && t.isClose(t2) && !t.isAttachedTo(t2)){
				PointVisible[] sT2 = t2.getSommets();
				PointVisible pMem = new PointVisible(0,0);
				PointVisible pMem2 = new PointVisible(0,0);
				int nbProche = 0;
				int nbNonProche = 0;
				for(PointVisible p : sT1){
					for(PointVisible p2 : sT2){
						if(p.poids == p2.poids && p.distance(p2) < 15){
							nbProche++;
							pMem = p;
							pMem2 = p2;
						}else if(p.poids != p2.poids && p.distance(p2) < 15) {
							nbNonProche++;
						}
						if (nbProche > 1 && nbNonProche > 0) {
							nbProche = 0;
						}
					}
				}
				if(nbProche > 1){
					Transformation translation = new Transformation();
					int difX =(int)(pMem2.getX() - pMem.getX());
					int difY = (int)(pMem2.getY() - pMem.getY());
					t.setP1(translation.translation(t.getP1(), difX, difY));
					t.setP2(translation.translation(t.getP2(), difX, difY));
					t.setP3(translation.translation(t.getP3(), difX, difY));
				}
			}
		}		
	}
	
	/*
	 * @brief
	 * @param
	 * @return
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setPaintMode(); 
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,	RenderingHints.VALUE_ANTIALIAS_ON);	
		g2d.setColor(fgColor);
		if (rectangleElastique != null) g2d.draw(rectangleElastique);
		aretes = new ArrayList<Vecteur>();
		aretes_droites = new ArrayList<Vecteur>();
		int n = points.size();
		int m = points_droites.size();
		PointVisible triangle_centre;
		g2d.fillRect(10,10,80,80);
		g2d.setColor(Color.WHITE);
		g2d.setFont(new Font("default", Font.BOLD, 16));
		g2d.drawString("Puzzles", 18, 55);
		g2d.setFont(new Font("default", Font.PLAIN, 12));
		g2d.setColor(Color.BLACK);
		requestFocus();
		for (int i = 0 ; i < m; i+=3) {
			aretes_droites.add(new Vecteur(points_droites.get(i), points_droites.get((i+1)%m)));
			aretes_droites.add(new Vecteur(points_droites.get(i+1), points_droites.get((i+2)%m)));
			aretes_droites.add(new Vecteur(points_droites.get(i+2), points_droites.get((i)%m)));
		}
		
		for(int i=0;i<n;i++){
			aretes.add(new Vecteur(points.get(i), points.get((i+1)%n)));
		}
		
		for (Vecteur v: aretes) {
			v.dessine(g2d, false);
		}
		for (Vecteur v: aretes_droites) {
			v.dessine(g2d, true);
		}

		for(Triangle t_tmp : triangles) {
            triangle_centre = t_tmp.getCentre();
            g2d.drawString(t_tmp.getLabel_centre(), triangle_centre.x-11, triangle_centre.y);
            for(Etoile e: t_tmp.getEtoiles()) {
                e.Draw(g2d);
            }
        }
		
		
	}
	
	
	/*
	 * @brief
	 * @param
	 * @return
	 */
	private void draw_puzzle(String fileName2) {
		ReadWritePoint rw = new ReadWritePoint(fileName2);
		points = rw.read(false);
		aretes = new ArrayList<Vecteur>();
		int n = points.size();
		//Puzzle
		for(int i=0; i<n;i++){
			aretes.add(new Vecteur(points.get(i), points.get((i+1)%n)));
		}
	}
	
	/*
	 * @brief
	 * @param
	 * @return
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		for(int iT=0; iT<triangles.size();iT++){
			Triangle t = triangles.get(iT);
			if(t.isInside(e.getX(), e.getY()) && e.getButton() == MouseEvent.BUTTON3){
				Transformation rot = new Transformation();
				ArrayList<PointVisible> trianglePoint = rot.rotation(t, 60.0);
				for(int i=0;i<points_droites.size();i++){
					if(points_droites.get(i).equals(t.getP1())){
						points_droites.remove(i);
						points_droites.add(i, trianglePoint.get(0));
					}
					else if(points_droites.get(i).equals(t.getP2())){
						points_droites.remove(i);
						points_droites.add(i, trianglePoint.get(1));
					}
					else if(points_droites.get(i).equals(t.getP3())){
						points_droites.remove(i);
						points_droites.add(i, trianglePoint.get(2));
					}
				}
				Triangle t2 = new Triangle(trianglePoint.get(0),trianglePoint.get(1),trianglePoint.get(2),t.getID());
				triangles.remove(iT);
				triangles.add(iT,t2);
				aimantation(t2);
			}
		}
		repaint();
	}

	/*
	 * @brief
	 * @param
	 * @return
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		if(SwingUtilities.isLeftMouseButton(e)) {
			for(int iT=0; iT<triangles.size();iT++){
				Triangle t = triangles.get(iT);
				if(t.isInside(e.getX(), e.getY())){
					iTriangle = iT;
					oldX = e.getX();
					oldY = e.getY();
				}
			}
		}
		// Si le joueur clique sur le carré supérieur gauche
		// Changement de puzzle
		Point position = e.getPoint();
		if(position.x >= 10 && position.x <= 90 && position.y >= 10 && position.y <= 90){
			draw_puzzle(filesToRead.get(curentFile));
			triangles.clear();
			initFromLog("data/Trioker/triangle.csv", filesToRead.get(curentFile), true);
			curentFile++;
			curentFile = curentFile%filesToRead.size();
		}
	}
	
	/*
	 * @brief
	 * @param
	 * @return
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		
		// Réinitialisation de la liste des triangles aimantés
		for(Triangle t : triangles) {
			t.initAimante();
		}
		for(Triangle t : triangles) {
			t.reInitAimante(triangles);
		}

		for(Triangle t: triangles){
			if(t.getAimante().size() == 0){
				//System.out.println("Vide");
				t.getP1().setColor_id(Color.BLACK);
				t.getP2().setColor_id(Color.BLACK);
				t.getP3().setColor_id(Color.BLACK);
			}else{
				t.getP1().setColor_id(Color.GREEN);
				t.getP2().setColor_id(Color.GREEN);
				t.getP3().setColor_id(Color.GREEN);
			}
		}
		iTriangle = -1;
		repaint();
	}
	
	/*
	 * @brief
	 * @param
	 * @return
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
		if(iTriangle != -1){
			Triangle t  = triangles.get(iTriangle);
			Transformation tr = new Transformation();
			ArrayList<PointVisible> trianglePoint = new ArrayList<PointVisible>();
			trianglePoint.add(new PointVisible(t.getP1().x,t.getP1().y,t.getP1().poids,t.getP1().idTri));
			trianglePoint.add(new PointVisible(t.getP2().x,t.getP2().y,t.getP2().poids,t.getP2().idTri));
			trianglePoint.add(new PointVisible(t.getP3().x,t.getP3().y,t.getP3().poids,t.getP3().idTri));
			for(int i=0;i<3;i++) {
				PointVisible p = trianglePoint.get(i);
				p = tr.translation(p, e.getX()-oldX,e.getY()-oldY);
				trianglePoint.remove(i);
				trianglePoint.add(i,p);
			}
			Triangle t2 = new Triangle(trianglePoint.get(0),trianglePoint.get(1),trianglePoint.get(2),t.getID());
			double x = t2.getCentre().getX(), y = t2.getCentre().getY();
			if(x >=0 && x<=width && y>= 0 && y <= height){
				for(int i=0;i<points_droites.size();i++){
					if(points_droites.get(i).equals(t.getP1())){
						points_droites.remove(i);
						points_droites.add(i, trianglePoint.get(0));
					}
					else if(points_droites.get(i).equals(t.getP2())){
						points_droites.remove(i);
						points_droites.add(i, trianglePoint.get(1));
					}
					else if(points_droites.get(i).equals(t.getP3())){
						points_droites.remove(i);
						points_droites.add(i, trianglePoint.get(2));
					}
				}
				triangles.remove(iTriangle);
				triangles.add(iTriangle,t2);
				aimantation(t2);
			}
			oldX = e.getX();
			oldY = e.getY();
		}
		repaint();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}


}


