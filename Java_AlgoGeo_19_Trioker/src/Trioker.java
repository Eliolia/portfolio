import javax.swing.BorderFactory;
import javax.swing.JFrame;

import utils.aff.Couleur;
import utils.aff.Vue;

public class Trioker {

	public static void main(String s[]) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				int x = 10, y = 10, w = 800, h = 800;
				String fileName = "data/Trioker/triangle.csv";
				String fileName2 = "data/Trioker/trio-hypo.csv";
				JFrame frame = new JFrame("Plateau de Trioker");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				Vue v1;
				v1 = new Vue(w, h, fileName, fileName2, true);
				v1.setBorder(BorderFactory.createLineBorder(Couleur.fg) );
				frame.add(v1);
				frame.pack();
				frame.setLocation(x, y);
				frame.setResizable(false);
				frame.setVisible(true);
			}
		});
	}

}
