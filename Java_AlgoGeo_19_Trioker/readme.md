# Trioker

## Règles

Le Trioker est un jeu de société qui ne dispose que d'une seule règle : 

* deux pièces ne peuvent être réunies par une arête que si, et seulement si, les sommets juxtaposés portent le même nombre, couleur ou symbole.

Chaque pièce est un triangle équilatéral dont les valeurs aux sommets sont uniques. 

Il y a en tout et pour tout 24 pièces.

## Comment jouer ?

On dispose de différents modèles, correspondant à un puzzle, que l'on doit remplir à l'aide des pièces triangulaires, tout en respectant la règle énoncée ci-dessus.

Le déplacement des pièces se fait par "drag and drop" avec le clic gauche et la rotation avec le clic droit.

Pour passer au puzzle suivant, cliquez sur le bouton "Puzzles".

## Implémentation

L'application est implémentée en Java et utilise la librairie graphique Swing sous l'IDE Eclipse.

À l'aide de fonctions de lecture/écriture, on récupère la position des triangles ainsi que la valeur aux sommets dans des fichiers "csv", de même que pour les puzzles.

La valeur aux sommets est symbolisée par des petites étoiles dans chaque coin du triangle ainsi que par le nombre au centre. 

Chaque triangle peut être récupéré et déposé sur le modèle par translation. Il peut également être pivoté sur lui-même et une "aimantation" permet une validation des triangles compatibles qui subiront alors un changement de couleur verte.

## Les améliorations envisageables

Plusieurs améliorations du projet ont été envisagées :

* aimantation avec le puzzle ;
* redimensionnement des pièces par rapport au puzzle et à la taille de la fenêtre ;
* redimensionnement des modèles par rapport au puzzle et à la taille de la fenêtre ;
* Condition de victoire par validation générale pour passer au puzzle suivant ;
* Mise en place d'un score en fonction du temps et du nombre d'essais ;
* Recalage automatique des pièces sur le puzzle.

## Exécuter

Ouvrir le projet avec l'IDE Eclipse et lancer un "run".

## Démonstration vidéo

Dans le dossier "Démonstration" se trouve la vidéo en question
