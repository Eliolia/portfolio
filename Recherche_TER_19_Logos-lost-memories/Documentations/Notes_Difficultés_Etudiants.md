Les étudiants avaient du mal de passer des phrases en langage naturel à des variables, à la résoluton du système.

Passer d'un fait à une variable => Capacité d'abstraction

Les joueurs ont une structure de logique (ils sont universitaires).

Difficultés : Langage naturel -> Langage Formalisé

Les élèves qui sortent du secondaire connaissent bien comment trouver une contradiction d'une implication, mais pas la négation d'une implication

Les faits sont là
La logique travaille avec les énoncés et pas les faits
Les faits peuvent produire des énoncés
Les énoncés peuvent décrire des faits

Parfois il existe des énoncés qui sont contingents
* Trop contingent car pas assez d'information
* Contingent

Utiliser des mécaniques à logique implicite car nécessaire pour le joueur, mais besoin de les expliciter.

Lecture du document:

Problème dans la transition Lycée -> Université, echec scolaire

Plusieurs difficultées rencontré par les enseignants mathématiciens et logiciens :
* Programmes du secondaire repoussent de nombreuses notion vers le premier cycle universitaire
* Les étudiants ne sont pas préparé au formalisme logique, de fait, ils ont des difficultés à passer d'énoncés du langage naturel vers des énoncés en langage formalisé utilisant les variables, fonctions ou bien des quantificateurs logiques. En sommes, c'est leur capacité d'abstraction mathématique qui pèche.

D'un autre côté, les enseignants utilisent la quantification implicite, telle que quelque soit une proposition donnée, le quantificateur universelle est sous-entendu. Or, cette quantification implicite n'est pas partagée par tous les étudiants ce qui implique que certains se retrouvent donc démunis car ne disposant pas de tous les outils suffisants et nécessaires pour comprendre les énoncés complexes. 

Bien que les élèves qui sortent du secondaire sachent reconnaître une implication quant il y en a une explicitement, ils n'arrivent pas à la concevoir dès lors qu'elle est implicite et donc non exprimée par l'énoncée.

Parfois, certains énoncés peuvent être vrais dans certains cas, et faux dans d'autres; ces énoncés sont donc contingents. Une étude datant de juin 2005, menée par Vivianne Durand-Guerrier, met en lumière une autre pratique de l'enseignement classique qui repose sur l'abscence de conclusion dans les démonstrations proposées aux élèves avant leur premier cycle universitaire scientifique où l'on démontre seulement par des preuves utilisant des éléments génériques sans pour autant établir de théorèmes généraux, ce qui tend à "obscurcir la notion de vérité dans une théorie donnée".

