
# Prise de notes sur les différents documents

## Découverte des "Serious Games" par Damien Djaouti

"Tout jeu dont la finalité première est autre que le simple divertissement" [Sande Chen & David Michael, 2005]

**Fonctions utilitaires d'un Serious Game:**
* Diffusion de messages ("Les Eonautes", Almédia 2012; "Smart eBall", Smart 2011)
* Dispenser entraînements (physique/cognitif) ("Hammer and Planks" de Natural Pad, 2012)
* Favoriser les échanges ("Foldit" UoWashington, 2008; "Google Image Labeler" Google 2006)

Applicable dans de nombreux domaines.

***

Modèle Entertainement != Modèle Serious Game

Pour créer un Serious Game, on détermine d'abord quelles sont les éléments que l'on souhaite transmettre au public-joueurs, ce qui va définir notre scénario utilitaire, puis on articule un scéanrio vidéoludique autours du premier, au travers de ses mécaniques de jeu (intrasèque | constructivisme) ou durant des phases d'apprentissages séparés du gameplay (extrinsèque | béhaviorisme).


**Qu'est ce que la Serious Games Initiative ?** (Chercher si nécessaire)

Serious Game != "loisir pour enfants"

"Serious Game Summit Europe"

Serious Game Classification

Le Serious Game est en train de subir des transformation et se s'implenter dans tous les domaines.

**Avantages des SG en classe**
* Motivation des apprenants
* Apprentissage par essais-erreurs (notion de feedback)
* Adaptation au rythme des apprenants
* Entretenir la communication entre les apprenants à des fins pédagogiques

L'utilisation d'un SG va dépendre de plusieurs paramètres, tels que le public visé, l'utilisation qui en est faite par les enseignants et sa manière de travailler ainsi que celle de ses étudiants. Un jeu parfaitement adapté pour une classe donnée ne le sera pas forcément pour une autre.

Importance d'un enseignant: le SG ne le remplace pas, le feedback à travers un debriefing est important, il doit s'intégrer au sein d'un cours et non le remplacer.

L'utilisation d'un SG en classe a un coût non négligeable qui doit être pris en considération à la fois par l'utilisateur mais également par le concepteur.

L'enseignant a le choix entre utiliser des jeux déjà existant, il va devoir réfléchir à la pertinence de son jeu vis à vis de son cours, soit créer lui même un jeu spécialement pour ça, par lui-mêm ou par ses élèves/étudiants, qui peut prendre plus de temps. 

Le SG peut être utilisé en Introduction ou en application d'un cours, mais son utilisation va permettre d'attiser la curiosité des apprenants, et donc in fine leur motivation à apprendre.

Les jeux vont dépendre de la vision des concepteurs sur un sujet donnés, il y aura toujours une part de subjectivité qui en émane d'eux, mais également de l'interprétation que peut en faire le joueur. Ainsi ils ne sont jamais "neutres".

Serious Gaming := Détournement de jeux de divertissement à des fins utilitaires (ex: angry birds pour les projections de force)

Pas besoin d'être concepteur/programmeur pour faire des SG car il existe des outils logiciels adaptés pour les non informaticiens. L'imagination est le meilleur moteur de la création de jeux.

## Apprendre le Game Design : Aspects théoriques et pratiques par Damien Djaouti

**Trois étapes principales:**
* Imaginer un concept de jeu
* Créer un jeu vidéo fonctionnel
* Evaluer la pertinence du jeu

**Théorie du "flow"**: expérience holistique que ressentent des individus lorsqu'ils agissent en s'impliquant totalement (__Mihaly Csikszentmihalyi__). L'immersion totale dans une activité et dans son accomplissement. Elle n'est pas spécifique au jeu vidéo car elle peut s'appliquer à d'autres formes d'activités.
		Lien : [Théorie du flow](http://edutechwiki.unige.ch/fr/Th%C3%A9orie_du_flow)
		* Très forte concentration et focalisation sur l'activité
		* Perception du temps altérée
		* Existence d'objectifs clair et précis
		* Valorisation de l'effort intrinsèque à l'activité (points d'xp, objets etc)
		* Equilibre entre capacité et objectifs

**Théorie du "fun"** : Forme d'application de la théorie du "flow" au jeu. L'apprentissage est au coeur du jeu car il permet de maintenir l'excitation du joueur en créant des feedbacks variables (ex: Tic-tac-toe aux stratégies prévisibles et au feedback certains, Jeu d'échecs aux stratégies originales et aux Feedbacks variables). La nécéssité de rechercher et d'apprendre des stratégies gagnantes, de nouveaux patterns.
		Lien : [Théorie du fun de Raph Koster - El Gamificator](https://www.elgamificator.com/creer-une-experience-fun-la-theorie-de-raph-koster)
		*Le fun est le plaisir éprouvé suite à la résolution mentale d'un défi* [Raph Koster]

Il faut donc construire des patterns que le joueur devra **apprendre** et maîtriser pour gagner. La construction du "fun" va donc dépendre de deux manières de contruire le jeu : 
		* Game Design (_Inventer les mécaniques de jeu_)
		* Level Design (_Construction d'Univers dont les éléments sont soumis aux mécaniques du jeu_)
La difficulté va être de gérer la difficulté d'apprentissage des patterns en alternant des "tension" affin de garder le joueur en alerte et des "détentes" durant lequel le joueur pourra appliquer tranquillement ce qu'il vient d'apprendre.

**Boucles de Gameplay** : 
		Lien: [Définir les boucles de gameplay pour orienter les techniques de rétroactions en classe de l’accompagnateur/ice de Esteban Giner](https://halshs.archives-ouvertes.fr/halshs-01739912/document)

**Typologie de joueurs** : [Typologie de Richard Bartle](https://punchygame.wordpress.com/2014/10/26/les-typologies-de-joueurs-le-bartle-test/)

Le Game Design va permettre de se projeter dans la conception d'un jeu vis à vis d'une deadline (ex: Game Jam)

***

Il existe **3 principales catégories de "théories"** ~ méthodes :
* Elaboration des mécaniques de gameplay à travers une vision analytique
	* Boucles de Gameplay
	* Briques de Gameplay
	* Trois types de règles de jeu
* Attiser la curiosité et l'excitation du joueur
	* [Théorie du flow](http://edutechwiki.unige.ch/fr/Th%C3%A9orie_du_flow)
	* [Théorie du fun de Raph Koster](https://www.elgamificator.com/creer-une-experience-fun-la-theorie-de-raph-koster)
	* [Théorie des 3C (Control, Camera, Character)](https://www.supinfo.com/articles/single/4614-game-design-3c-control-camera-character)
* Comprendre le ressenti du joueur
	* [Typologie de Richard Bartle]( types différents )
	* Expérience utilisateur

***

Le modèle M.D.A est un framework permettant d'analyser les jeux selon trois composantes : Mechanics Dynamics Aesthetics
* **Mechanics** sont les composants de base du jeu, à savoir ses règles, les actions basiques pouvant être effectuées par le joueur, les algorithmes utilisés, les structures de données dans le moteur, etc.
* **Dynamics** correspond au comportement à l'exécution des mécaniques liés aux interactions du joueur et/ou les interactions avec les autres mécaniques
* **Aesthetics** sont les réponses émotionnelles suscitées par le joueur. Elles ont été statué en 8 types différents par [Hunicke, LeBlanc and Zubek](https://www.researchgate.net/publication/228884866_MDA_A_Formal_Approach_to_Game_Design_and_Game_Research "MDA: A Formal Approach to Game Design and Game Research") :
	* Sensation (__Game as sense-pleasure__)
	* Fantaisie (__Game as make-believe__)
	* Narrative (__Game as drama__)
	* Challenge (__Game as obstacle course__)
	* Camaraderie (__Game as social framework__)
	* Découverte (__Game as uncharted territory__)
	* Expression (__Game as self-discovery__)
	* Soumission (__Game as pastime__)

Il est important de se poser la question quant au comportement éventuel du joueur, car chaque personne aura son propre ressenti et donc sa propre expérience du jeu. L'écrivain britannique Richard Bartle classifie les joueurs selon 4 stéréotypes segmentés en deux axes (action/interaction et joueur/monde) en fonction de leurs réponses à 30 questions :

* **Killer** : Joueur ayant besoin de se mesurer aux autres de façon compétitive et de manière assez compulsive. Il a soif de reconnaissance et d'être au dessus du lot.
* **Socializer** : Joueur ayant besoin de sociabiliser et de coopérer avec les autres. Le jeu n'est qu'un support pour lui, ainsi la connaissance pleine et entière de ses mécaniques n'est pas un fin en soit sauf si elle devient nécessaire pour son intégration dans un groupe.
* **Explorer** : Il va acquérir avec le temps une connaissance considérable du jeu. Il va s'intéresser plus particulièrement au Lore (Univers du jeu) et chercher à maîtriser les mécaniques de jeu par soif de connaissance. 
* **Achiever** : C'est un collectionneur dont le but est de finir de fond en comble le jeu, quelque soit la difficulté.

Ce qui est à retenir, c'est que le test de Bartle permet de mieux prendre en considération la personnalité des joueurs afin de proposer plusieurs expériences de jeu à un public souvent hétérogène mais qui se complètent entre eux. Cependant elle ne s'applique pas à toutes les formes de jeu. 

La neuro-scientifique Amy Jo Kim va apporter quelques modification et compléter le test de Bartle afin de mieux identifier les joueurs au travers de leurs actions.

* **Killer** : 
	* gagner
	* se montrer
	* battre des records ou ses adversaires
	* se comparer aux autres
	* railler ses adversaires 
	* défier 
	* se vanter
* **Socializer** :
	* commenter 
	* partager 
	* «aimer»
	* aider 
	* donner 
	* complimenter 
	* échanger 
	* rejoindre un groupe 
	* faire du commerce
* **Explorer** :
	* entendre, interagir ou vivre une histoire 
	* découvrir un univers 
	* tester de nouveaux produits 
	* résoudre des problèmes de manière originale 
	* la curiosité
	* chercher
* **Achiever** : 
	* collectionner 
	* achever tous les buts quel que soit leur intérêt 
	* être perfectionniste 
	* finir quelque chose 
	* faire toutes les quêtes 
	* posséder tous les objets virtuels 
	* acheter des produits dérivés

***

Plusieurs modèles existent afin d'aider les concepteurs de jeu à créer leur jeu, mais il n'existe pas de solution miracle. Chaque créatif se doit de choisir l'outil qui lui correspond le mieux ou bien de faire sans.
* La Théorie des Tokens (Rollings et Morris 2003)
* Game Alchemy (Cook 2007)
* Game Layers (Tajè 2007)


## Recherche des jeux de "logique"

### Still Life
* Jeu d'enquête Point and click, Polar
* Avril 2005 Xbox et PC

### Shenmue
* Decmbre 2000 sur Dreamcast
* Histoire de vengeance avec enquête, collecte d'indices

### Under a Killing Moon
* Détective qui doit mettre au jour ce que fait une secte

### Life is Strange 
* Parler à des gens pour débloquer des scènes, d'autres interactions avec des objets ou des personnages

### The vanishing of Ethan Carter
* Septembre 2014 PC
* Reconstituer les événements passés de manière atypique pour résoudre le mystère

### Detroit : Become Humans

### Heavy Rain
* Expérience parallèle entre 4 personnages, polar numérique

### The Last Express
* Point'n click, 1997 sur PC, enquête policière
* 

### Licence des Zeldas

### Les chevaliers de Baphomet

### Lara Croft
* Regarder les enigmes comment elles sont amenés, est-ce issu de la "logique"?

### Portal 1 et 2
* 2007 sur PC, Xbox 360 et PS3
* Parcours dans un labyrinthe de salles

### World of Warcraft
* Certains mini-jeu sous forme de résolution de systèmes

### Soldat Inconnu : Mémoire de la grande guerre

### Le testament de Sherlock Holmes
* Septembre 2012
* 60aines d'enigmes variées avec une enquête

### Phoenix Wrigth

### Her Story
* Aventure sous forme d'un jeu de piste en utilisant un moteur de recherche
* associer des idées en fonction de mot-clefs et de dates

### L.A. Noire
* Histoire de détective
* Analyse des experssions faciales

### Myst
* 1993,
* aventure, point'n click en vue subjective 

### Uncharted ?

### Batman : Arkham City et Asylium

### TellTale Games

### Professor Layton
* Akira Tago ? -> Livre de casse-tête

### The Talos Principle
* PC en 2014
* Suite de puzzles dans un monde semi-ouvert

### Trine
* Action, puzzle et aventure
* Coopération entre 3 personnages avec des pouvoirs différents

### Monument Valley
* Jouer sur la perspective, isométrique, guider un personnage, illusions optique
* Jeu de réflexion

### Antichamber
* Constructions impossibles

### Tetris

### Braid
* Aventure, plateforme, réflexion
* Remonter le temps lui permettant de trouver les solutions pour résoudre des mystères

### Lemmings
* 1991 par DMA Design
* Guider à travers des niveaux des 10aines de lemmings s'orientant aveuglément sans en perdre trop

### Little Nightmares

### Keep talking and nobody Explodes
* Pression du temps pour résoudre plusieurs tâches

### Flow free

### Fez
* Basé sur la perspective

### The Witness
 
### Brothers : A tale of two sons

### The Last Guardian

### The last day of June

### Rime

### Hellblade : Senua's Sacrifice

Logique propositionnelle => Règles du jeu !
(Si je saute et qu'il y a un trou -> Je meurs) équivalent à (Je ne saute pas ou il n'y a pas de trou ou je meurs)