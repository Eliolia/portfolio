Webographie : 

Méthodes d'apprentissage et Psychopédagogie :
    https://fr.wikipedia.org/wiki/Psychop%C3%A9dagogie
    https://fr.wikipedia.org/wiki/M%C3%A9thode_d%27apprentissage_cognitif
    https://hal.sorbonne-universite.fr/hal-01134066v6/document
    https://www.persee.fr/doc/rfp_0556-7807_1995_num_112_1_1226
    Cours de Madame Violaine Prince
    https://www.mpg.de/research/video-games-brain
    https://www.meirieu.com/OUTILSDEFORMATION/vellas_comparerlespedagogies.pdf
    
Serious Games :
    http://thesesups.ups-tlse.fr/1458/1/2011TOU30229.pdf
    http://eduscol.education.fr/numerique/dossier/apprendre/jeuxserieux/bibliographie/theses-etudes
    http://seriousgameresearchnetwork.univ-jfc.fr/fr/publications
    http://www.ludoscience.com/FR/diffusion/index.html
    http://www.ludovia.com/2014/07/creation-conception-dun-serious-game-le-cas-termitia/
    http://serious.gameclassification.com/FR/index.html
    Cours de Monsieur Damien Djaouti et A. Gouaïch
    
Game Design :
    https://www.supinfo.com/articles/single/4614-game-design-3c-control-camera-character
    https://halshs.archives-ouvertes.fr/halshs-01739912/document
    http://edutechwiki.unige.ch/fr/Th%C3%A9orie_du_flow
    https://www.elgamificator.com/creer-une-experience-fun-la-theorie-de-raph-koster
    https://punchygame.wordpress.com/2014/10/26/les-typologies-de-joueurs-le-bartle-test/
    Cours de Monsieur Damien Djaouti
    https://en.wikipedia.org/wiki/MDA_framework
    https://www.researchgate.net/publication/228884866_MDA_A_Formal_Approach_to_Game_Design_and_Game_Research
    https://www.elgamificator.com/amy-jo-kim-gamification
    
Programmation :
    https://www.developpez.net/forums/d880196/applications/developpement-2d-3d-jeux/contribuez/coordonnees-map-2d-isometriques/
    https://docs.unity3d.com/Manual/Tilemap-Isometric-Palette.html
    https://www.emse.fr/~picard/cours/2A/DesignPatterns.pdf
    https://umlgameprog.phtools.fr/charger-un-niveau-avec-le-patron-visiteur/
    https://openclassrooms.com/forum/sujet/l-architecture-d-un-jeu-33928
    https://linuxfr.org/news/je-cree-mon-jeu-video-e01-les-systemes-a-entites


Bibliographie :
    Apprendre avec les Serious Games ? Julian Alvarez, Damien Djaouti, Olivier Rampnoux
    Scrum Une méthode agile pour vos projets, Aurélien Vannieuwenhuyze
    Design Patterns en Java. Les 23 modèles de conception : descriptions et solutions illustrées en UML2 et Java, Laurent Debrauwer
    Apprenez à développer en C#, Nicolas Hilaire
    IHM et recherche d'information, Céline Paganelli

