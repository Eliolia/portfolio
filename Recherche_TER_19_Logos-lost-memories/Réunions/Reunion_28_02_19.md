# Réunion du 28 Février 2019
* Début : 8h40
* Fin : 9h35
* Présents : Cédric, Elodie, M. Gouaïch


Rentrer dans les scènes de dialogue pour voir les skills du personnage sur son raisonnement;
Extraire soit-même les éléments d'une phrases pour faire ses propres formules logiques
Quand les habitants donnent des indices, est-ce qu'il ne peut pas construire soit même ses faits
	T'as la phrase, t'as des connecteurs connus, et tu dois en faire une formule (ET, OU, NON)


Stabiliser la mécanique, montrer dans la scène de dialogue comment le joueur va devoir raisonner, qu'est ce que ça veut dire pour lui en terme d'action (Que peut il sélectionner, faire, cliquer, as-t-il un inventaire, est-ce prouver un faire, contredire?)
=> Donner un objectif particulier à un raisonnement (ex: Echanges socratiques, contredire quelqu'un)
=> Livre sur la théorie de la contreverse : https://www.amazon.fr/Art-Controversy-Arthur-Schopenhauer/dp/1533215782