# Réunion avec Madame Durand-Guerrier
## Début 14 h 00

Difficultés : Langage naturel -> Langage Formalisé
Mémoire de Logique en 1989

Différentes situations:
* Math discrète : polyminaux que permet de travailler la logique sous forme de jeux, jeux d'enigmes policière

Alice au pays de merveilles, Logique sans peine de Lewis Caroll en gamifiant certaines situations.

Journée Mondiale de la Logique Janvier 2019 : Tarski, mort de Gödel, Théorie élémentaire des modèles
 * Faire des mini-modèles théoriques pour répondre à certaines questions
 * Le Kangourou des mathématiques : On peut pas répondre car contingent
 * Meutre Champagne de Agatha Christie: le héro dit qu'il a une idée géniale pour résoudre toutes les enquêtes, il mets en place un modèle de la situation.

Labyrinthe : Il y a des phrases et il faut dire si elles sont vraies ou fausses => Logique des prédicats

Jeu du portrait robot : On dit qu'une partie des gens ont menti, on sait rien des autres, il faut faire un portrait robot => Pas de solution

Nicolas Pelet => Jeu en Mathématiques (PlaisirMath) Développe des jeux de plateau, thèse en jeu
Contrat Didactique = Ce qui est responsabilité du prof et de l'élève
Théorie des situations didactiques ~~ Théorie des jeux de Von-Neuman

Colas Duflot, philosophe ayant développé le contrat didadactique = Que veut dire jouer ?

Les élèves qui sortent du secondaire connaissent bien comment trouver une contradiction d'une implication, mais pas la négation d'une implication

Distinction entre les faits et les énoncés sur les faits => Enquête policière

Les faits sont là
La logique travaille avec les énoncés et pas les faits
Les faits peuvent produire des énoncés
Les énoncés peuvent décrire des faits

Slots = On mets des énoncés sur des faits

Parfois il existe des énoncés qui sont contingents
* Trop contingent car pas assez d'information
* Contingent

Logique propositionnelle = Logique Binaire != Logique prédicative
Utiliser des mécaniques à logique implicite car nécessaire pour le joueur, mais besoin de les expliciter.

Garde ren tête que les mécaniques puissent avoir un sens pas trop artificiel pour donner envie au joueur de rester. S'il y a juste un habillage, l'interêt disparait. L'idée de naviguer entre le narratif et les mécaniques.

Bande dessiné LogiComix (https://fr.wikipedia.org/wiki/Logicomix)

