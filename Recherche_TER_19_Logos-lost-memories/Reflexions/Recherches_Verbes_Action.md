# Recherche de verbes d'actions

Le but est de chercher puis classifier les verbes d'actions afin de mieux définir les actions qui pourront être effectués par les joueurs de notre projet.
Les verbes sont catégorisés et nous avons sélectionné ceux qui nous ont semblé les plus pertinents.

## Décider
* Choisir
* Conclure
* Déterminer
* Juger
* Résoudre
* Trancher

## Gérer
* Consolider (son raisonnement)
* Exploiter

## Diriger
* Définir

## Administrer
* Classer
* Etablir
* Gérer

## Produire
* Appliquer
* Exécuter
* Crafter

## Organiser
* Structurer
* Manipuler

## Communiquer
* Discuter
* Ecouter
* Interviewer

## Developper
* Etendre (une formule)
* Déclencher

## Chercher
* Analyser
* Enquêter
* Observer
* Rechercher

## Former
* Développer
* Transformer

## Controler
* Enquêter
* Evaluer
* Prouver
* Valider

## Créer
* Construire
* Découvrir

## Négocier
* Argumenter
* Conclure
* Démontrer
* Discuter
* Sélectionner

## Conseiller
* Clarifier
* Comprendre 


Réflexions sur les actions du joueurs
Objectif : Trouver 3 à 5 verbes d'actions qui vont définir les actions de base

Exemple : 
Ecrire, Envoyer, Recevoir => Client de Messagerie
Ecrire, Editer, Sauvegarder => Éditeur de texte

1) Recherche de verbes d'actions

Nous avons sélectionnés les verbes d'actions qui semblaient les plus pertinents pour un jeu de type Enquête - Réflexion Logique - Aventure

Décider : Choisir, Conclure, Déterminer Juger Résoudre Trancher
Gérer : Consolider (son raisonnement)Exploiter
Diriger :  Définir
Administrer : Classer Établir Gérer
Produire : Appliquer Exécuter Crafter
Organiser : Structurer Manipuler
Communiquer :  Discuter Ecouter Interviewer
Développer : Étendre (une formule) Déclencher
Chercher : Analyser Enquêter Observer Rechercher
Former : Développer Transformer
Contrôler : Enquêter Evaluer Prouver Valider
Créer : Construire Découvrir
Négocier : Argumenter Conclure Démontrer Discuter Sélectionner
Conseiller : Clarifier Comprendre 


Propositions :
    1) Interviewer, assembler, résoudre                                       COMMUNIQUER, ORGANISER, DÉCIDER
    2) Enquêter, sélectionner, conclure                                         CHERCHER, NÉGOCIER, DÉCIDER
    3) Enquêter, analyser, structurer, transformer, conclure      CONTRÔLER, CHERCHER, ORGANISER, FORMER, DÉCIDER
    4) Observer, Analyser, Évaluer, Produire, Conclure              CHERCHER, CONTRÔLER, PRODUIRE, DÉCIDER

 3 et 4 ont l'air assez bien
    
Contours du jeu :

Lorsque le joueur lance le jeu, il accède au menu. Il a le choix entre 3 chapitres. Ces chapitres ont leurs mécaniques adaptées à la logique ciblée. Chaque chapitre est découpé en plusieurs sous-chapitres.

Idée : Le joueur peut incarner deux points de vue différent pour chaque partie de jeu.

Chaque sous-chapitre peut être divisé en deux phases: Une phase d'enquête, et une phase de décision.
Le joueur peut naviguer entre plusieurs lieux:
Débloqué en fonction de son avancement ? -> Rend nécessaire la résolution d'énigme ou de conclure sur les éléments qu'on a
Tous accessibles en même temps mais à environnement dynamique ?
À chaque lieu, le joueur pourra les examiner, parler à des PNJ, présenter des faits ou des objets à ceux-ci.
Les faits énoncés par les PNJ peuvent être valides ou contradictoires en fonction de ce que l'on sait déjà
Liste des indices possibles :
Jeux de mots
Photographie
Détail d'un lieu
Evenements
...
La présentation d'indices aux PNJ peut apporter des informations supplémentaires comme inutiles
Certains éléments peuvent servir à résoudre des énigmes permettant de débloquer d'autres lieux, ou d'obtenir des objets/faits plus complexes par composition (obtention d'une "sous-formule" logique)
Le joueur pourra rapidement observer ce avec quoi il peut interagir (couleur, forme, surbrillance, animation)
Le joueur pourra faire face à certaines mécanismes/machine/puzzle qu'il va devoir comprendre afin d'obtenir les informations nécessaires.

chapitre 1 : booleen
chapitre 2 : propositionnelle
chapitre 3 : prédicative

Boolean => Afin de se familiariser avec les premières mécaniques, le joueur devra résoudre des petits puzzles initiant aux principes inhérents à l'algèbre de Boole (ET, OU, XOR, XAND, ...), progressivement. EXEMPLE: PORTAL

Il faut trouver un "modèle" permettant de rendre satisfiable l'ensemble des faits amenant à une conséquence logique que l'on cherche à prouver.