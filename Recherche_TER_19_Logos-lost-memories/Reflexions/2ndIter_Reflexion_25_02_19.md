## Scène 1 : Introduction

### Personnage(s) :
	* Logan : Incarné par le joueur
	* Logos : Non joueur accompagnant le joueur
### Description du Lieu : Maison/studio
	* Salle de bain : baignoire, toilette et lavabo
	* Cuisine : table, chaises, vaissellier
	* Séjour/chambre : Lit, commode, table de chevet
### Objet(s) : 
	* Clefs de la porte d'entrée

### Déroulement :

Logan se réveille dans un lit, dans une maison. Logos se rapproche de lui, l'interloquant: "Tu comptais dormir encore longtemps?"

	Le joueur peut interagir avec l'ensemble des objets dans la maison/pièce en les observant. 
	L'interaction avec les objets de la maison améneront à des réactions de Logos : "Tu comptes aussi contempler ta maison toute la journée?" 

=> On peut interagir avec des éléments de l'environnement.
=> On peut explorer son environnement

	Si le joueur essaye de passer par une porte fermée sans interagir, il se heurte à celle-ci. => Nécessiter d'interagir

Lorsque Logan tente de sortir de chez lui, la porte est fermée.
-> Logan n'a pas la clef sur lui
	-> Logos : "Tu n'as pas la clef ET la porte est fermée. DONC, tu ne peux pas sortir, CQFD. Elles ne doivent pas être bien loin."
	-> Logan doit chercher ses clefs dans la maison.
	-> Logan tente d'ouvrir la porte sans la clef, rien ne se produit.
-> Logan a la clef sur lui
	-> Logan insère la clef dans la porte, et une petite "trappe" s'ouvre avec un mécanisme à l'intérieur.

	Le joueur va résoudre le mécanime qui symbolise un circuit logique avec un connecteur ET. 
	À une des extremités, deux boutons sont éteints, et à l'autre extrémité se trouve une "LED" également éteinte.(OFF ET OFF : OFF) 
	Il va devoir allumer les deux boutons (ON ET ON : ON)

Feedback de Logos : ""
Un bruit indique que la porte s'est ouverte, Logan peut quitter sa maison.

***

## Scène 2 : Village

### Personnage(s)
	* Logan : Incarné par le joueur
	* Logos : PNJ accompagnant le joueur (Ambiguïté: Qui peut le voir? On peut jouer dessus.)
	* Michel le Collectionneur : PNJ
	* Habitants : Bûcheron, commerçant, autres 
	* Deux gardes : PNJ
### Description du Lieu
	* La maison de Logan
	* La maison de Michel
	* Taverne
	* Pont-levis
	* Place centrale avec une rose des vents
	* Plusieurs bâtiments divers
	* Bibliothèque
	* Porte d'entrée du village
	* Mur entourant le village
	* Deux petits ports, un à chaque bord de la rivière
	* Commerces

	Orientation de la ville Nord <--> Sud
	Orientation de la map décalé de 45° sens anti-horaire

### Objet(s)
	* Levier
	* Manivelle
	* Rouage
	* Bûche -> Pied de table
### Déroulement

Logan et Logos se retrouve à l'extérieur. On y découvre un petit village simple, quelques `Maisons`, une `Place Centrale`, des `habitants` érants, et une `Taverne`.
Logos : "Toute bonne journée commence à la taverne. Peut être y trouvera tu quelque chose à faire? "
La Taverne se trouve de l'autre côté d'une petite rivière traversant le village de bout en bout. Un petit `pont-levis` permet de relier les deux bords.
La sortie du village est fermée, des gardes avertissent Logan que les environs ne sont pas sûr, ils attendent le retour des éclaireurs avant d'autoriser de nouveau les flux.

	Le joueur a la possibilité de parcourir le village. 
		* Il ne peut pas entrer dans les maisons ni de nouveau chez lui, on en ressort.
		* Il peut discuter avec les villageois qu'il croisera mais qui n'apporte rien hormis un peu leur histoire et celle du village. Certains peuvent lui indiquer où se trouve la `Taverne` en discutant avec eux.
		* [Autres interactions?]

En arrivant du côté du `pont`, on constate que celui-ci est levé, impossible de traverser en l'état. Un tableau de bord se situe de chaque côté du pont.
-> Il faut combiner le `rouage` et la `manivelle` puis insérer le composite dans un des emplacements.
-> Le `levier` bloque ou débloque le système -> ON/OFF
ET
-> La `manivelle` permet de lever le pont -> ON/OFF
=>> Le pont se ferme. Le passage s'ouvre.

	Le joueur peut s'approcher du tableau de bord. 
		* S'il choisit de l'observer, il pourra constater que celui-ci nécessite l'actionnement d'un `levier` ET d'une `manivelle` combiné à un `rouage`, mais que ceux-ci sont manquant. Il pourra également trouver une note accroché au tableau. => Combinaison d'objet qui ménera plus tard à une combinaison de faits.
		* S'il choisit de l'utiliser, Logos lui indiquera "J'ai l'impression qu'il manque un truc. Regarde de plus près."

Sur la note récupérée par Logan est inscrit: "C'est juste un emprunt ! M.". 
Logos : "Un emprunt qui habituellement s'appelle 'Revient'. Allons le retrouver."

	Ici, le joueur peut interroger Logos :
		* Qui as bien pu "emprunter" ces objets? => "C'est pas moi! J'ai rien volé cette fois! Attends, non rien."
		* Pense tu qu'on puisse traverser à la nage? => "Essaye, on verra bien."
		* Que peut-il bien faire d'un levier, d'une manivelle et d'un rouage ? => "C'est à lui qu'il faut demander!"
	Il peut également parcourir le village afin d'interroger les habitants.
		-> Utiliser la note sur les habitants -> Qui est M?
		-> Demander qui est M?
		-> Demander ce qu'on peut bien faire d'un levier, d'une manivelle et surtout d'un rouage?

S'il demande à l'un des habitants qui est "M", ceux-ci lui révéleront qu'il s'agit de Michel, un habitant du village appellé "Le Collectionneur".

	Le joueur peut demander aux habitants où habite Michel le Collectionneur. Ceux-ci lui donneront des indications qu'il va devoir recouper pour déduire l'emplacement de la maison.
	Pour savoir si un bâtiment est la maison de Michel, il va devoir se placer devant la porte et crier "MICHEL?!". S'il n'a pas demandé qui était "M", il ne peut pas réaliser cette action.
	Liste des indications : (Conjonction de clauses)
		Habitant 1 -> La lumière du jour frappe directement sa porte le matin (Est)
		Habitant 2 -> Proche du mur entourant la ville et de la forêt. (Mur ET Forêt)
		Habitant 3 -> Il a un jardin ... particulier. (Jardin)
		Bûcheron/Menuisier  -> Lui ayant livré une stère de bois récemment, il lui semble qu'il a une cheminée ou un atelier, mais pas les deux. (Cheminée OUex Atelier)
			Logos : "Donc il a soit une cheminée et pas d'atelier, ou un atelier et pas de cheminée."
		Commerçant -> Il lui a acheté une charette à bras dernièrement (Charette)
	Formule : Est ET (Mur ET Forêt) ET Jardin ET ((Cheminée ET ~Atelier) OU (~Cheminée ET Atelier)) ET Charette
	Les indices seront accessibles et mis en forme sous la forme de "proposition".
	La rose des vents située sur la place centrale peut aider le joueur à s'orienter.

Logan et Logos sillonnent le village pour rechercher Michel, interrogeant les passant pouvant/voulant bien répondre à leurs questions.
En trouvant la maison de Michel, ils seront accueillit par celui-ci. [Développer la rencontre]
A l'extérieur du jardin : Une bûche peut être trouvée
L'intérieur de la maison est remplie de bric-à-bracs divers et variés, une mine d'or pour quiconque à besoin de n'importe quoi.

	Le joueur peut alors parler avec Michel :
	 -> Demander ce qu'il fait avec un levier, d'une manivelle et encore plus d'un rouage? 
	 -> Demander s'il veut essayer de traverser la rivière à la nage
	 -> Demander pourquoi il cumule autant d'affaire
	Si le joueur lui a demandé ce qu'il fait d'un levier et d'une manivelle, Michel lui indique qu'il se sert du levier comme pied de table et de la manivelle pour son invention : la machine à musique. Et le rouage qui fait un magnifique dessous-de-table.
	Il peut également récupérer une bûche posée près de la porte d'entrée.

Logan ne peut pas retirer le levier comme ça, au risque de faire tomber ce qui est posé sur la table. Michel lui indique qu'il lui faudrait quelque chose à interchanger. Logos : "Un bout de bois devrait faire l'affaire non?".
Il peut récupérer le "dessous-de-table" ainsi que la manivelle. Il peut actionner la manivelle pour tester la machine à musique.

	Le joueur va devoir récupérer la bûche, retourner voir le bûcheron/menuisier pour en faire un nouveau pied de table. Il pourra alors récupérer le levier.
	Il pourra de nouveau se diriger vers le pont.

Rappel : 
-> Il faut combiner le rouage et la manivelle puis insérer le composite dans un des emplacements.
-> Le levier bloque ou débloque le système -> ON/OFF
ET
-> La manivelle permet de lever le pont -> ON/OFF
=>> Le pont se ferme. Le passage s'ouvre.

	Le joueur peut traverser le pont et ainsi se diriger vers la Taverne.


***

## [WIP] Scène ? : Village le retour

### Personnage(s)
	* Logan : Incarné par le joueur
	* Logos : PNJ accompagnant le joueur
	* Michel le villageois : PNJ 
	* Jeanine la villageoise : PNJ
	* Deux gardes : PNJ
### Description du Lieu
	* La maison de Logan
	* L'entrée du village
	* 
### Objet(s)
	* Stèle étrange
### Déroulement

Un villageois s'approche de Logan et l'interpelle :
Michel: "Logan! Logan! Viens voir c'est incroyable!"
Logos : "Tout peut être incroyable avec lui, voyons voir ce que c'est cette fois."

Le joueur est amené par Michel le Villageois vers le lieu où se dresse un objet des plus étranges : une grande stèle de couleur obscidienne parsemée de gravures brillante. 

Logos : "Incroyable! Un cailloux géant tout noir. Et après?"

	Le joueur a la possibilité de s'approcher de la stèle et de l'observer.









